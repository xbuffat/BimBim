import numpy as np
import matplotlib.pyplot as plt

from BimBim.Basis import Basis
from BimBim.Beams import Beams

def parseWakeTable(filename):
    wakefile = open(filename,'r')
    lines = wakefile.readlines()
    row = len(lines)
    col = len(lines[0].split())
    wakeTable = [[0.0 for i in range(col)] for j in range(row)]
    for i in range(row):
        thisline = lines[i].split()
        wakeTable[i][0] = float(thisline[0])
        for j in range(1,col):
            wakeTable[i][j]=float(thisline[j])
    wakefile.close()
    return wakeTable


if __name__=='__main__':
    nSlice = 20
    nRing = 10
    nDim = 2
    energy = 4.0E3
    intensity = 1.5E11
    emittance = 2E-6
    sigs = 0.0755
    sigp = 0.00011
    qs = 1.9E-3
    fill1 = '1 1 1 0'
    fill2 = '1 1 1 0'
    sfact = 2.99792458e8/1.0e9
    beams = Beams(fill1,fill2,energy=energy,intensity = intensity,emittance = emittance,sigs=sigs,sigp=sigp)
    basis = Basis(beams.getNBunchB1(),beams.getNBunchB2(),nSlice,nRing,nDim,equiDistantRing=False)

    print('radius',basis._ringDistribution[0])
    print('weight',basis._ringDistribution[1])
    print('boundaries',basis._ringDistribution[2])

    x = []
    y = []
    for i in range(nSlice):
        for j in range(nRing):
            tmpx = basis.getSPosition(i,j,sigs)/sfact
            tmpy = basis.getDPoverP(i,j,sigp,1)
            x.append(tmpx)
            y.append(tmpy)
    plt.figure(0)
    R = 3.0
    color = 'k'
    #plt.plot([0.0,R*sigs/sfact],[0.0,0.0],color=color,alpha=0.1)
    for i in range(1,nSlice):
        alpha1 = basis.getSliceAngle(i-1)
        alpha0 = basis.getSliceAngle(i)
        alpha = (alpha0+alpha1)/2.0
        plt.plot([0.0,R*sigs*np.cos(alpha)/sfact],[0.0,R*sigp*np.sin(alpha)],color=color,alpha=0.1)
    
    alphas = np.arange(0.0,2.0*np.pi,1E-3)
    #for j in range(0,nRing):
    #    plt.plot(basis.getRingRadius(j)*sigs*np.cos(alphas)/sfact,basis.getRingRadius(j)*sigp*np.sin(alphas),color=color,alpha=0.1)

    for i in range(len(basis._ringDistribution[2])):
        plt.plot(basis._ringDistribution[2][i]*sigs*np.cos(alphas)/sfact,basis._ringDistribution[2][i]*sigp*np.sin(alphas),'-',color=color,alpha=0.1)

    plt.scatter(x,y)
    plt.xlabel('s')
    plt.ylabel(r'$\Delta p / p$')

    plt.figure(2)
    drs = np.zeros(nRing)
    for i in range(nRing):
        drs[i] = basis._ringDistribution[2][i+1]-basis._ringDistribution[2][i]
        #drs[i] = 0.5*(basis._ringDistribution[2][i+1]+basis._ringDistribution[2][i])*(basis._ringDistribution[2][i+1]-basis._ringDistribution[2][i])
    plt.plot(basis._ringDistribution[0],basis._ringDistribution[1]/drs,'xr')

    x = np.arange(0.0,3.0,0.01)
    plt.plot(x,np.exp(-x**2/2))

    if True:
        basis = Basis(beams.getNBunchB1(),beams.getNBunchB2(),nSlice,nRing,nDim,equiDistantRing=True)
        plt.figure(2)
        drs = np.zeros(nRing)
        for i in range(nRing):
            drs[i] = basis._ringDistribution[2][i+1]-basis._ringDistribution[2][i]
            #drs[i] = 0.5*(basis._ringDistribution[2][i+1]+basis._ringDistribution[2][i])*(basis._ringDistribution[2][i+1]-basis._ringDistribution[2][i])
        #drs[i] = basis._ringDistribution[0][i]*(basis._ringDistribution[2][i+1]-basis._ringDistribution[2][i])
        plt.plot(basis._ringDistribution[0],basis._ringDistribution[1]/drs,'xb')


    if False:
        plt.twinx()
        
        wakeFileName = 'wake_lhc.in'
        newWakeFileName = 'smoothWake_lhc.in'
        wake = parseWakeTable(wakeFileName)
        clight = 2.99792458e8
        energy = 4000.0
        mass=0.93827231
        gamma = 1.0 + energy/mass
        beta = np.sqrt(1.0 - 1.0/gamma**2)
        sfact = clight/(beta*1.0e9)
        sigs = 0.094
        
        s = [wake[k][0] for k in range(len(wake))]
        wakeData = [wake[k][1] for k in range(len(wake))]
        plt.plot(s,wakeData,'-x')
        plt.xlim([0,1.5])
    
    
    
    
    plt.show()
