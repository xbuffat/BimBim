import numpy as np
from matplotlib import pyplot as plt
from scipy import constants as cst

from BimBim.Beams import Beams
from BimBim.Basis import Basis
from BimBim.System import System

from BimBim.Action.Transport import Transport
from BimBim.Action.BeamBeam import BeamBeam

if __name__ == '__main__':
    n_slice = 5
    n_ring = 1

    energy = 6.8E3
    norm_emit = 2E-6
    gamma = energy*1E3/cst.value('proton mass energy equivalent in MeV')
    phys_emit = norm_emit / gamma
    qs = 1.9E-3
    sigma_z = 0.09
    sigma_dpp = 1.1E-4
    plane = 'x'
    betaStar = 0.5
    r0 = cst.e**2/(4*np.pi*cst.epsilon_0*cst.m_p*cst.c**2)

    intensities = np.arange(2E9,2E11,4E9)
    xis = intensities*r0/(4*np.pi*norm_emit)
    coherentTunes = np.zeros((len(intensities),2*n_slice*n_ring),dtype=complex)
    beams = Beams(stringRep=['1 1 1 0','1 1 1 0'],energy=energy,phys_emit_x = phys_emit,intensity = 0.0,sigma_z=sigma_z,sigma_dpp=sigma_dpp)
    basis = Basis([beams.getNBunch(1),beams.getNBunch(2)], n_slice, n_ring,plane=plane,equiDistantRing = False)
    actionSequence = beams.getEmptyActionSequence()
    actionSequence[0] = BeamBeam(betaStar_x = betaStar,hourglass=True,virtualDrift=True)
    actionSequence[1] = Transport(phase_x=0.31,beta_x=betaStar,phase_z=qs)
    system = System(beams,actionSequence,basis)
    for iintensity,intensity in enumerate(intensities):
        beams.getBunch(0,beam=1).intensity = intensity
        beams.getBunch(0,beam=2).intensity = intensity
        oneTurn = system.buildOneTurnMap()
        eigvals,eigvecs = np.linalg.eig(oneTurn)
        coherentTunes0 = np.log(eigvals)*1j/(2*np.pi)
        mask = np.real(coherentTunes0)>0
        coherentTunes0 = coherentTunes0[mask]
        coherentTunes[iintensity,:] = coherentTunes0[np.argsort(np.imag(coherentTunes0))]

    plt.figure(0)
    for i in range(np.shape(coherentTunes)[1]):
        plt.plot(xis,np.real(coherentTunes[:,i])-0.31,'ob');
    plt.show()

