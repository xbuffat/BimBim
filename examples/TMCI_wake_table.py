import numpy as np
from matplotlib import pyplot as plt

from BimBim.Beams import Beams
from BimBim.Basis import Basis
from BimBim.System import System

from BimBim.Action.Transport import Transport
from BimBim.Action.Impedance import Impedance
from BimBim.Action.WakeFromTable import WakeFromTable

if __name__ == '__main__':
    n_slice = 5
    n_ring = 1

    energy = 6.8E3
    qs = 1.9E-3
    sigma_z = 0.09
    sigma_dpp = 1.1E-4
    plane = 'x'

    wakeDefinition = WakeFromTable('./wakes/wakeforhdtl_LHC_flattop_2022.dat')

    intensities = np.arange(2E10,1.5E12,2E10)
    coherentTunes = np.zeros((len(intensities),n_slice*n_ring),dtype=complex)
    beams = Beams(energy=energy,intensity = 0.0,sigma_z=sigma_z,sigma_dpp=sigma_dpp)
    basis = Basis(beams.getNBunch(1),n_slice,n_ring,plane=plane,equiDistantRing = False)
    actionSequence = beams.getEmptyActionSequence()
    actionSequence[0] = Transport(phase_x=0.31,beta_x=1.0,phase_z=qs)
    actionSequence[1] = Impedance(wakeDefinition,beta = [68.09,70.34])
    system = System(beams,actionSequence,basis)
    for iintensity,intensity in enumerate(intensities):
        beams.getBunch(0).intensity = intensity
        oneTurn = system.buildOneTurnMap()
        eigvals,eigvecs = np.linalg.eig(oneTurn)
        coherentTunes0 = np.log(eigvals)*1j/(2*np.pi)
        mask = np.real(coherentTunes0)>0
        coherentTunes0 = coherentTunes0[mask]
        coherentTunes[iintensity,:] = coherentTunes0[np.argsort(np.imag(coherentTunes0))]

    plt.subplots(num=0,nrows = 2, ncols = 1)
    vmax = np.max(np.imag(coherentTunes))
    vmin = -vmax
    for i in range(np.shape(coherentTunes)[1]):
        plt.subplot(211)
        plt.scatter(intensities,np.real(coherentTunes[:,i]),c=np.imag(coherentTunes[:,i]),vmin=vmin,vmax=vmax,marker='o', cmap=plt.get_cmap('YlOrRd'), edgecolor='none',s=6);
        plt.subplot(212)
        plt.scatter(intensities,2.0*np.pi*np.imag(coherentTunes[:,i]),c=np.imag(coherentTunes[:,i]),vmin=vmin,vmax=vmax,marker='o', cmap=plt.get_cmap('YlOrRd'), edgecolor='none',s=6);

    plt.show()

