import numpy as np
from matplotlib import pyplot as plt

from BimBim.Beams import Beams
from BimBim.Basis import Basis
from BimBim.System import System

from BimBim.Action.Transport import Transport
from BimBim.Action.Impedance import Impedance
from BimBim.Action.ResistiveWallWake import ResistiveWallWake
from BimBim.Action.WakeFromTable import WakeFromTable

if __name__ == '__main__':
    n_slice = 10
    n_ring = 1

    energy = 6.8E3
    intensity = 1.4E11
    qs = 1.9E-3
    sigma_z = 0.09
    sigma_dpp = 1.1E-4
    plane = 'x'

    #wakeDefinition = ResistiveWallWake(resistivity=1E10)
    wakeDefinition = WakeFromTable('./wakes/wakeforhdtl_LHC_flattop_2022.dat',skip_header=1)

    nSlot = 3564
    modes = np.hstack([np.arange(-100,100+0.1,1),np.arange(-nSlot/2,nSlot/2+0.1,100)])
    modes = np.sort(np.unique(modes))
    coherentTunes = np.zeros((len(modes),n_slice*n_ring),dtype=complex)
    beams = Beams(energy=energy,intensity = intensity,sigma_z=sigma_z,sigma_dpp=sigma_dpp)
    basis = Basis(beams.getNBunch(1),n_slice,n_ring,plane=plane,equiDistantRing = False)
    actionSequence = beams.getEmptyActionSequence()
    actionSequence[0] = Transport(phase_x=0.31,beta_x=1.0,phase_z=qs,chroma_x = 0)
    actionSequence[1] = Impedance(wakeDefinition,beta = [68.09,70.34],quadWake=False,
                    mimicbunch_n_bunch=nSlot,mimicbunch_distance=25.0,
                    mimicbunch_phase_x=0.0,mimicbunch_phase_y=0.0,mimicbunch_beta_x=1.0,mimicbunch_beta_y=0.0)
    system = System(beams,actionSequence,basis)
    for imode,mode in enumerate(modes):
        actionSequence[1].mimicbunch_phase_x=2.0*np.pi*mode/nSlot
        oneTurn = system.buildOneTurnMap()
        eigvals,eigvecs = np.linalg.eig(oneTurn)
        coherentTunes0 = np.log(eigvals)*1j/(2*np.pi)
        mask = np.real(coherentTunes0)>0
        coherentTunes0 = coherentTunes0[mask]
        coherentTunes[imode,:] = coherentTunes0[np.argsort(np.imag(coherentTunes0))]
        print('mode',mode,np.max(np.imag(coherentTunes[imode,:])))

    for imode,mode in enumerate(modes):
        plt.plot(np.zeros_like(coherentTunes[imode,:],dtype=float) + mode,np.imag(coherentTunes[imode,:]),'xb')

    plt.show()

