from BimBim.Beams import Beams
from BimBim.Basis import Basis
from BimBim.System import System

from BimBim.Action.Transport import Transport
from BimBim.Action.BeamBeam import BeamBeam
  

if __name__ == '__main__':
    nSlice = 1
    nRing = 1
    sep = 12
    nDim = 4
    energy = 7E3
    gamma = 7E3/0.938
    intensity = 1E11
    emittance = 2.7E-6
    phys_emit = emittance/gamma
    
    sigma_z = 0.0755
    sigma_dpp = 0.0001129
    qs = 1.9E-3
    bunchSpacing = 25.0

    nSlot = 3564
    
    actionSequence = []
    for i in range(2*nSlot):
        actionSequence.append(None)

    phaseHIP1B1 = 0
    phaseHIP2B1 = 8.527900629
    phaseHIP5B1 = 31.97655342
    phaseHIP8B1 = 56.15073773
    QHB1 = 64.31

    phaseVIP1B1 = 0
    phaseVIP2B1 = 7.522944842
    phaseVIP5B1 = 29.64850625
    phaseVIP8B1 = 51.17444701
    QVB1 = 59.32

    phaseHIP1B2 = 0
    phaseHIP2B2 = 56.22563835
    phaseHIP5B2 = 32.32502585
    phaseHIP8B2 = 8.64237048
    QHB2 = 64.31

    phaseVIP1B2 = 0
    phaseVIP2B2 = 51.224297
    phaseVIP5B2 = 29.55810731
    phaseVIP8B2 = 7.677585591
    QVB2 = 59.32

    ip2Shift = 891
    ip5Shift = nSlot
    ip8Shift = -894

#####################################   IP1   ####################################################
    if True:
        actionSequence[-16] = BeamBeam(betaStar_x=1.0,sepStar=[0.0,sep],hourglass=False,virtualDrift=False)
        actionSequence[-15] = BeamBeam(betaStar_x=1.0,sepStar=[0.0,sep],hourglass=False,virtualDrift=False)
        actionSequence[-14] = BeamBeam(betaStar_x=1.0,sepStar=[0.0,sep],hourglass=False,virtualDrift=False)
        actionSequence[-13] = BeamBeam(betaStar_x=1.0,sepStar=[0.0,sep],hourglass=False,virtualDrift=False)
        actionSequence[-12] = BeamBeam(betaStar_x=1.0,sepStar=[0.0,sep],hourglass=False,virtualDrift=False)
        actionSequence[-11] = BeamBeam(betaStar_x=1.0,sepStar=[0.0,sep],hourglass=False,virtualDrift=False)
        actionSequence[-10] = BeamBeam(betaStar_x=1.0,sepStar=[0.0,sep],hourglass=False,virtualDrift=False)
        actionSequence[-9] = BeamBeam(betaStar_x=1.0,sepStar=[0.0,sep],hourglass=False,virtualDrift=False)
        actionSequence[-8] = BeamBeam(betaStar_x=1.0,sepStar=[0.0,sep],hourglass=False,virtualDrift=False)
        actionSequence[-7] = BeamBeam(betaStar_x=1.0,sepStar=[0.0,sep],hourglass=False,virtualDrift=False)
        actionSequence[-6] = BeamBeam(betaStar_x=1.0,sepStar=[0.0,sep],hourglass=False,virtualDrift=False)
        actionSequence[-5] = BeamBeam(betaStar_x=1.0,sepStar=[0.0,sep],hourglass=False,virtualDrift=False)
        actionSequence[-4] = BeamBeam(betaStar_x=1.0,sepStar=[0.0,sep],hourglass=False,virtualDrift=False)
        actionSequence[-3] = BeamBeam(betaStar_x=1.0,sepStar=[0.0,sep],hourglass=False,virtualDrift=False)
        actionSequence[-2] = BeamBeam(betaStar_x=1.0,sepStar=[0.0,sep],hourglass=False,virtualDrift=False)
        actionSequence[-1] = BeamBeam(betaStar_x=1.0,sepStar=[0.0,sep],hourglass=False,virtualDrift=False)
    if True:
        actionSequence[0] = [Transport(phase_x=0.25,beta_x=1.0,phase_y=0.25,beta_y=1.0,phase_z=0),BeamBeam(betaStar_x=1.0,hourglass=False,virtualDrift=False),Transport(phase_x=0.25,beta_x=1.0,phase_y=0.25,beta_y=1.0,phase_z=0)]
    else :
        actionSequence[0] = Transport(phase_x=0.5,beta_x=1.0,phase_y=0.5,beta_y=1.0,phase_z=0)
    if True:
        actionSequence[1] = BeamBeam(betaStar_x=1.0,sepStar=[0.0,-sep],hourglass=False,virtualDrift=False)
        actionSequence[2] = BeamBeam(betaStar_x=1.0,sepStar=[0.0,-sep],hourglass=False,virtualDrift=False)
        actionSequence[3] = BeamBeam(betaStar_x=1.0,sepStar=[0.0,-sep],hourglass=False,virtualDrift=False)
        actionSequence[4] = BeamBeam(betaStar_x=1.0,sepStar=[0.0,-sep],hourglass=False,virtualDrift=False)
        actionSequence[5] = BeamBeam(betaStar_x=1.0,sepStar=[0.0,-sep],hourglass=False,virtualDrift=False)
        actionSequence[6] = BeamBeam(betaStar_x=1.0,sepStar=[0.0,-sep],hourglass=False,virtualDrift=False)
        actionSequence[7] = BeamBeam(betaStar_x=1.0,sepStar=[0.0,-sep],hourglass=False,virtualDrift=False)
        actionSequence[8] = BeamBeam(betaStar_x=1.0,sepStar=[0.0,-sep],hourglass=False,virtualDrift=False)
        actionSequence[9] = BeamBeam(betaStar_x=1.0,sepStar=[0.0,-sep],hourglass=False,virtualDrift=False)
        actionSequence[10] = BeamBeam(betaStar_x=1.0,sepStar=[0.0,-sep],hourglass=False,virtualDrift=False)
        actionSequence[11] = BeamBeam(betaStar_x=1.0,sepStar=[0.0,-sep],hourglass=False,virtualDrift=False)
        actionSequence[12] = BeamBeam(betaStar_x=1.0,sepStar=[0.0,-sep],hourglass=False,virtualDrift=False)
        actionSequence[13] = BeamBeam(betaStar_x=1.0,sepStar=[0.0,-sep],hourglass=False,virtualDrift=False)
        actionSequence[14] = BeamBeam(betaStar_x=1.0,sepStar=[0.0,-sep],hourglass=False,virtualDrift=False)
        actionSequence[15] = BeamBeam(betaStar_x=1.0,sepStar=[0.0,-sep],hourglass=False,virtualDrift=False)
        actionSequence[16] = BeamBeam(betaStar_x=1.0,sepStar=[0.0,-sep],hourglass=False,virtualDrift=False)
########################################################################################
    actionSequence[17] = Transport(phase_x=[phaseHIP2B1-phaseHIP1B1-0.5,phaseHIP2B2-phaseHIP1B2-0.5],beta_x=1.0,phase_y=[phaseVIP2B1-phaseVIP1B1-0.5,phaseVIP2B2-phaseVIP1B2-0.5],beta_y=1.0,phase_z=0)
###################################   IP2   ######################################################
    if True:
        actionSequence[ip2Shift-16] = BeamBeam(betaStar_x=1.0,sepStar=[0.0,sep],hourglass=False,virtualDrift=False)
        actionSequence[ip2Shift-15] = BeamBeam(betaStar_x=1.0,sepStar=[0.0,sep],hourglass=False,virtualDrift=False)
        actionSequence[ip2Shift-14] = BeamBeam(betaStar_x=1.0,sepStar=[0.0,sep],hourglass=False,virtualDrift=False)
        actionSequence[ip2Shift-13] = BeamBeam(betaStar_x=1.0,sepStar=[0.0,sep],hourglass=False,virtualDrift=False)
        actionSequence[ip2Shift-12] = BeamBeam(betaStar_x=1.0,sepStar=[0.0,sep],hourglass=False,virtualDrift=False)
        actionSequence[ip2Shift-11] = BeamBeam(betaStar_x=1.0,sepStar=[0.0,sep],hourglass=False,virtualDrift=False)
        actionSequence[ip2Shift-10] = BeamBeam(betaStar_x=1.0,sepStar=[0.0,sep],hourglass=False,virtualDrift=False)
        actionSequence[ip2Shift-9] = BeamBeam(betaStar_x=1.0,sepStar=[0.0,sep],hourglass=False,virtualDrift=False)
        actionSequence[ip2Shift-8] = BeamBeam(betaStar_x=1.0,sepStar=[0.0,sep],hourglass=False,virtualDrift=False)
        actionSequence[ip2Shift-7] = BeamBeam(betaStar_x=1.0,sepStar=[0.0,sep],hourglass=False,virtualDrift=False)
        actionSequence[ip2Shift-6] = BeamBeam(betaStar_x=1.0,sepStar=[0.0,sep],hourglass=False,virtualDrift=False)
        actionSequence[ip2Shift-5] = BeamBeam(betaStar_x=1.0,sepStar=[0.0,sep],hourglass=False,virtualDrift=False)
        actionSequence[ip2Shift-4] = BeamBeam(betaStar_x=1.0,sepStar=[0.0,sep],hourglass=False,virtualDrift=False)
        actionSequence[ip2Shift-3] = BeamBeam(betaStar_x=1.0,sepStar=[0.0,sep],hourglass=False,virtualDrift=False)
        actionSequence[ip2Shift-2] = BeamBeam(betaStar_x=1.0,sepStar=[0.0,sep],hourglass=False,virtualDrift=False)
        actionSequence[ip2Shift-1] = BeamBeam(betaStar_x=1.0,sepStar=[0.0,sep],hourglass=False,virtualDrift=False)
    if True:
        actionSequence[ip2Shift] = [Transport(phase_x=0.25,beta_x=1.0,phase_y=0.25,beta_y=1.0,phase_z=0),BeamBeam(betaStar_x=1.0,hourglass=False,virtualDrift=False),Transport(phase_x=0.25,beta_x=1.0,phase_y=0.25,beta_y=1.0,phase_z=0)]
    else :
        actionSequence[ip2Shift] = Transport(phase_x=0.5,beta_x=1.0,phase_y=0.5,beta_y=1.0,phase_z=0)
    if True:
        actionSequence[ip2Shift+1] = BeamBeam(betaStar_x=1.0,sepStar=[0.0,-sep],hourglass=False,virtualDrift=False)
        actionSequence[ip2Shift+2] = BeamBeam(betaStar_x=1.0,sepStar=[0.0,-sep],hourglass=False,virtualDrift=False)
        actionSequence[ip2Shift+3] = BeamBeam(betaStar_x=1.0,sepStar=[0.0,-sep],hourglass=False,virtualDrift=False)
        actionSequence[ip2Shift+4] = BeamBeam(betaStar_x=1.0,sepStar=[0.0,-sep],hourglass=False,virtualDrift=False)
        actionSequence[ip2Shift+5] = BeamBeam(betaStar_x=1.0,sepStar=[0.0,-sep],hourglass=False,virtualDrift=False)
        actionSequence[ip2Shift+6] = BeamBeam(betaStar_x=1.0,sepStar=[0.0,-sep],hourglass=False,virtualDrift=False)
        actionSequence[ip2Shift+7] = BeamBeam(betaStar_x=1.0,sepStar=[0.0,-sep],hourglass=False,virtualDrift=False)
        actionSequence[ip2Shift+8] = BeamBeam(betaStar_x=1.0,sepStar=[0.0,-sep],hourglass=False,virtualDrift=False)
        actionSequence[ip2Shift+9] = BeamBeam(betaStar_x=1.0,sepStar=[0.0,-sep],hourglass=False,virtualDrift=False)
        actionSequence[ip2Shift+10] = BeamBeam(betaStar_x=1.0,sepStar=[0.0,-sep],hourglass=False,virtualDrift=False)
        actionSequence[ip2Shift+11] = BeamBeam(betaStar_x=1.0,sepStar=[0.0,-sep],hourglass=False,virtualDrift=False)
        actionSequence[ip2Shift+12] = BeamBeam(betaStar_x=1.0,sepStar=[0.0,-sep],hourglass=False,virtualDrift=False)
        actionSequence[ip2Shift+13] = BeamBeam(betaStar_x=1.0,sepStar=[0.0,-sep],hourglass=False,virtualDrift=False)
        actionSequence[ip2Shift+14] = BeamBeam(betaStar_x=1.0,sepStar=[0.0,-sep],hourglass=False,virtualDrift=False)
        actionSequence[ip2Shift+15] = BeamBeam(betaStar_x=1.0,sepStar=[0.0,-sep],hourglass=False,virtualDrift=False)
        actionSequence[ip2Shift+16] = BeamBeam(betaStar_x=1.0,sepStar=[0.0,-sep],hourglass=False,virtualDrift=False)
####################################################################################
    actionSequence[ip2Shift+17] = Transport(phase_x=[phaseHIP5B1-phaseHIP2B1-0.5,phaseHIP5B2-phaseHIP2B2-0.5],beta_x=1.0,phase_y=[phaseVIP5B1-phaseVIP2B1-0.5,phaseVIP5B2-phaseVIP2B2-0.5],beta_y=1.0,phase_z=qs/2)
###################################   IP5   #################################################
    if True:
        actionSequence[ip5Shift-16] = BeamBeam(betaStar_x=1.0,sepStar=[sep,0.0],hourglass=False,virtualDrift=False)
        actionSequence[ip5Shift-15] = BeamBeam(betaStar_x=1.0,sepStar=[sep,0.0],hourglass=False,virtualDrift=False)
        actionSequence[ip5Shift-14] = BeamBeam(betaStar_x=1.0,sepStar=[sep,0.0],hourglass=False,virtualDrift=False)
        actionSequence[ip5Shift-13] = BeamBeam(betaStar_x=1.0,sepStar=[sep,0.0],hourglass=False,virtualDrift=False)
        actionSequence[ip5Shift-12] = BeamBeam(betaStar_x=1.0,sepStar=[sep,0.0],hourglass=False,virtualDrift=False)
        actionSequence[ip5Shift-11] = BeamBeam(betaStar_x=1.0,sepStar=[sep,0.0],hourglass=False,virtualDrift=False)
        actionSequence[ip5Shift-10] = BeamBeam(betaStar_x=1.0,sepStar=[sep,0.0],hourglass=False,virtualDrift=False)
        actionSequence[ip5Shift-9] = BeamBeam(betaStar_x=1.0,sepStar=[sep,0.0],hourglass=False,virtualDrift=False)
        actionSequence[ip5Shift-8] = BeamBeam(betaStar_x=1.0,sepStar=[sep,0.0],hourglass=False,virtualDrift=False)
        actionSequence[ip5Shift-7] = BeamBeam(betaStar_x=1.0,sepStar=[sep,0.0],hourglass=False,virtualDrift=False)
        actionSequence[ip5Shift-6] = BeamBeam(betaStar_x=1.0,sepStar=[sep,0.0],hourglass=False,virtualDrift=False)
        actionSequence[ip5Shift-5] = BeamBeam(betaStar_x=1.0,sepStar=[sep,0.0],hourglass=False,virtualDrift=False)
        actionSequence[ip5Shift-4] = BeamBeam(betaStar_x=1.0,sepStar=[sep,0.0],hourglass=False,virtualDrift=False)
        actionSequence[ip5Shift-3] = BeamBeam(betaStar_x=1.0,sepStar=[sep,0.0],hourglass=False,virtualDrift=False)
        actionSequence[ip5Shift-2] = BeamBeam(betaStar_x=1.0,sepStar=[sep,0.0],hourglass=False,virtualDrift=False)
        actionSequence[ip5Shift-1] = BeamBeam(betaStar_x=1.0,sepStar=[sep,0.0],hourglass=False,virtualDrift=False)
    if True:
        actionSequence[ip5Shift] = [Transport(phase_x=0.25,beta_x=1.0,phase_y=0.25,beta_y=1.0,phase_z=0),BeamBeam(betaStar_x=1.0,hourglass=False,virtualDrift=False),Transport(phase_x=0.25,beta_x=1.0,phase_y=0.25,beta_y=1.0,phase_z=0)]
    else :
        actionSequence[ip5Shift] = Transport(phase_x=0.5,beta_x=1.0,phase_y=0.5,beta_y=1.0,phase_z=0)
    if True:
        actionSequence[ip5Shift+1] = BeamBeam(betaStar_x=1.0,sepStar=[-sep,0.0],hourglass=False,virtualDrift=False)
        actionSequence[ip5Shift+2] = BeamBeam(betaStar_x=1.0,sepStar=[-sep,0.0],hourglass=False,virtualDrift=False)
        actionSequence[ip5Shift+3] = BeamBeam(betaStar_x=1.0,sepStar=[-sep,0.0],hourglass=False,virtualDrift=False)
        actionSequence[ip5Shift+4] = BeamBeam(betaStar_x=1.0,sepStar=[-sep,0.0],hourglass=False,virtualDrift=False)
        actionSequence[ip5Shift+5] = BeamBeam(betaStar_x=1.0,sepStar=[-sep,0.0],hourglass=False,virtualDrift=False)
        actionSequence[ip5Shift+6] = BeamBeam(betaStar_x=1.0,sepStar=[-sep,0.0],hourglass=False,virtualDrift=False)
        actionSequence[ip5Shift+7] = BeamBeam(betaStar_x=1.0,sepStar=[-sep,0.0],hourglass=False,virtualDrift=False)
        actionSequence[ip5Shift+8] = BeamBeam(betaStar_x=1.0,sepStar=[-sep,0.0],hourglass=False,virtualDrift=False)
        actionSequence[ip5Shift+9] = BeamBeam(betaStar_x=1.0,sepStar=[-sep,0.0],hourglass=False,virtualDrift=False)
        actionSequence[ip5Shift+10] = BeamBeam(betaStar_x=1.0,sepStar=[-sep,0.0],hourglass=False,virtualDrift=False)
        actionSequence[ip5Shift+11] = BeamBeam(betaStar_x=1.0,sepStar=[-sep,0.0],hourglass=False,virtualDrift=False)
        actionSequence[ip5Shift+12] = BeamBeam(betaStar_x=1.0,sepStar=[-sep,0.0],hourglass=False,virtualDrift=False)
        actionSequence[ip5Shift+13] = BeamBeam(betaStar_x=1.0,sepStar=[-sep,0.0],hourglass=False,virtualDrift=False)
        actionSequence[ip5Shift+14] = BeamBeam(betaStar_x=1.0,sepStar=[-sep,0.0],hourglass=False,virtualDrift=False)
        actionSequence[ip5Shift+15] = BeamBeam(betaStar_x=1.0,sepStar=[-sep,0.0],hourglass=False,virtualDrift=False)
        actionSequence[ip5Shift+16] = BeamBeam(betaStar_x=1.0,sepStar=[-sep,0.0],hourglass=False,virtualDrift=False)
#######################################################################################
    actionSequence[ip5Shift+17] = Transport(phase_x=[phaseHIP8B1-phaseHIP5B1-0.5,phaseHIP8B2-phaseHIP5B2-0.5],beta_x=1.0,phase_y=[phaseVIP8B1-phaseVIP5B1-0.5,phaseVIP8B2-phaseVIP5B2-0.5],beta_y=1.0,phase_z=qs/2)
####################################   IP8   ######################################################
    if True:
        actionSequence[ip8Shift-16] = BeamBeam(betaStar_x=1.0,sepStar=[-sep,0.0],hourglass=False,virtualDrift=False)
        actionSequence[ip8Shift-15] = BeamBeam(betaStar_x=1.0,sepStar=[-sep,0.0],hourglass=False,virtualDrift=False)
        actionSequence[ip8Shift-14] = BeamBeam(betaStar_x=1.0,sepStar=[-sep,0.0],hourglass=False,virtualDrift=False)
        actionSequence[ip8Shift-13] = BeamBeam(betaStar_x=1.0,sepStar=[-sep,0.0],hourglass=False,virtualDrift=False)
        actionSequence[ip8Shift-12] = BeamBeam(betaStar_x=1.0,sepStar=[-sep,0.0],hourglass=False,virtualDrift=False)
        actionSequence[ip8Shift-11] = BeamBeam(betaStar_x=1.0,sepStar=[-sep,0.0],hourglass=False,virtualDrift=False)
        actionSequence[ip8Shift-10] = BeamBeam(betaStar_x=1.0,sepStar=[-sep,0.0],hourglass=False,virtualDrift=False)
        actionSequence[ip8Shift-9] = BeamBeam(betaStar_x=1.0,sepStar=[-sep,0.0],hourglass=False,virtualDrift=False)
        actionSequence[ip8Shift-8] = BeamBeam(betaStar_x=1.0,sepStar=[-sep,0.0],hourglass=False,virtualDrift=False)
        actionSequence[ip8Shift-7] = BeamBeam(betaStar_x=1.0,sepStar=[-sep,0.0],hourglass=False,virtualDrift=False)
        actionSequence[ip8Shift-6] = BeamBeam(betaStar_x=1.0,sepStar=[-sep,0.0],hourglass=False,virtualDrift=False)
        actionSequence[ip8Shift-5] = BeamBeam(betaStar_x=1.0,sepStar=[-sep,0.0],hourglass=False,virtualDrift=False)
        actionSequence[ip8Shift-4] = BeamBeam(betaStar_x=1.0,sepStar=[-sep,0.0],hourglass=False,virtualDrift=False)
        actionSequence[ip8Shift-3] = BeamBeam(betaStar_x=1.0,sepStar=[-sep,0.0],hourglass=False,virtualDrift=False)
        actionSequence[ip8Shift-2] = BeamBeam(betaStar_x=1.0,sepStar=[-sep,0.0],hourglass=False,virtualDrift=False)
        actionSequence[ip8Shift-1] = BeamBeam(betaStar_x=1.0,sepStar=[-sep,0.0],hourglass=False,virtualDrift=False)
    if True:
        actionSequence[ip8Shift] = [Transport(phase_x=0.25,beta_x=1.0,phase_y=0.25,beta_y=1.0,phase_z=0),BeamBeam(betaStar_x=1.0,hourglass=False,virtualDrift=False),Transport(phase_x=0.25,beta_x=1.0,phase_y=0.25,beta_y=1.0,phase_z=0)]
    else :
        actionSequence[ip8Shift] = Transport(phase_x=0.5,beta_x=1.0,phase_y=0.5,beta_y=1.0,phase_z=0)
    if True:
        actionSequence[ip8Shift+1] = BeamBeam(betaStar_x=1.0,sepStar=[sep,0.0],hourglass=False,virtualDrift=False)
        actionSequence[ip8Shift+2] = BeamBeam(betaStar_x=1.0,sepStar=[sep,0.0],hourglass=False,virtualDrift=False)
        actionSequence[ip8Shift+3] = BeamBeam(betaStar_x=1.0,sepStar=[sep,0.0],hourglass=False,virtualDrift=False)
        actionSequence[ip8Shift+4] = BeamBeam(betaStar_x=1.0,sepStar=[sep,0.0],hourglass=False,virtualDrift=False)
        actionSequence[ip8Shift+5] = BeamBeam(betaStar_x=1.0,sepStar=[sep,0.0],hourglass=False,virtualDrift=False)
        actionSequence[ip8Shift+6] = BeamBeam(betaStar_x=1.0,sepStar=[sep,0.0],hourglass=False,virtualDrift=False)
        actionSequence[ip8Shift+7] = BeamBeam(betaStar_x=1.0,sepStar=[sep,0.0],hourglass=False,virtualDrift=False)
        actionSequence[ip8Shift+8] = BeamBeam(betaStar_x=1.0,sepStar=[sep,0.0],hourglass=False,virtualDrift=False)
        actionSequence[ip8Shift+9] = BeamBeam(betaStar_x=1.0,sepStar=[sep,0.0],hourglass=False,virtualDrift=False)
        actionSequence[ip8Shift+10] = BeamBeam(betaStar_x=1.0,sepStar=[sep,0.0],hourglass=False,virtualDrift=False)
        actionSequence[ip8Shift+11] = BeamBeam(betaStar_x=1.0,sepStar=[sep,0.0],hourglass=False,virtualDrift=False)
        actionSequence[ip8Shift+12] = BeamBeam(betaStar_x=1.0,sepStar=[sep,0.0],hourglass=False,virtualDrift=False)
        actionSequence[ip8Shift+13] = BeamBeam(betaStar_x=1.0,sepStar=[sep,0.0],hourglass=False,virtualDrift=False)
        actionSequence[ip8Shift+14] = BeamBeam(betaStar_x=1.0,sepStar=[sep,0.0],hourglass=False,virtualDrift=False)
        actionSequence[ip8Shift+15] = BeamBeam(betaStar_x=1.0,sepStar=[sep,0.0],hourglass=False,virtualDrift=False)
        actionSequence[ip8Shift+16] = BeamBeam(betaStar_x=1.0,sepStar=[sep,0.0],hourglass=False,virtualDrift=False)
#################################################################################
    actionSequence[ip8Shift+17] = Transport(phase_x=[QHB1-phaseHIP8B1-0.5,QHB2-phaseHIP8B2-0.5],beta_x=1.0,phase_y=[QVB1-phaseVIP8B1-0.5,QVB2-phaseVIP8B2-0.5],beta_y=1.0,phase_z=0)

    psBatch = '72 1 8 0' # 80 slots, 72 bunches, 8 empty
    spsTrain_1 = 1*(psBatch+' ') + '30 0' # 110 slots, 72 bunches, 38 empty
    spsTrain_2 = 2*(psBatch+' ') + '30 0' # 190 slots, 144 bunches, 46 empty
    spsTrain_3 = 3*(psBatch+' ') + '30 0' # 270 slots, 216 bunches, 54 empty
    spsTrain_4 = 4*(psBatch+' ') + '30 0' # 350 slots, 288 bunches, 62 empty
    
    abortGap = nSlot-190-7*270-4*350-3
    mainTrain = (spsTrain_2+' ')+(spsTrain_3+' ')+(spsTrain_4+' ')+3*('1 0 '+(spsTrain_3+' ')+(spsTrain_3+' ')+(spsTrain_4+' '))+str(abortGap)+' 0'
    
    fill1 = mainTrain
    fill2 = mainTrain
    ###############
    beams = Beams(stringRep=[fill1,fill2],energy=energy,intensity = intensity,phys_emit_x = phys_emit,sigma_z=sigma_z,sigma_dpp=sigma_dpp,bunchSpacing=bunchSpacing)
    
    basis = Basis([beams.getNBunch(1),beams.getNBunch(2)],nSlice,nRing,['x','y'])
    
    print('Number of bunches',beams.getNBunch(1),beams.getNBunch(2))
    print('Action sequence length',len(actionSequence))
    print(actionSequence)

    system = System(beams,actionSequence,basis)
    print('building matrix')
    oneTurn = system.buildOneTurnMap()
    print('computing eigen values')
    eigvals,eigvecs = np.linalg.eig(oneTurn)
