import numpy as np
from matplotlib import pyplot as plt

from BimBim.Basis import Basis
from BimBim.Beams import Beams

if __name__ == '__main__':
    nSlice = 40
    nRing = 1
    nDim = 2
    energy = 4.0E3
    intensity = 1.5E11
    emittance = 2E-6
    sigs = 0.0755
    sigp = 0.00011
    qs = 1.9E-3
    fill1 = '1 1 1 0'
    fill2 = '1 1 1 0'
    sfact = 2.99792458e8/1.0e9
    beams = Beams(fill1,fill2,energy=energy,intensity = intensity,emittance = emittance,sigs=sigs,sigp=sigp)
    basis = Basis(beams.getNBunchB1(),beams.getNBunchB2(),nSlice,nRing,nDim,equiDistantRing=True)
    longitudinalPositions = basis.getLongitudinalProfileDiscretePoints()
    expectedProfile = np.exp(-longitudinalPositions**2/2)/np.sqrt(2.0*np.pi)

    if True:
        plt.figure(1000)
        plt.plot(longitudinalPositions,expectedProfile,'-k')

        longitudinalProfile = basis.getLongitudinalProfile()
        plt.plot(longitudinalPositions,longitudinalProfile,'b')
        longitudinalProfile = basis.getLongitudinalProfileFromWeight()
        plt.plot(longitudinalPositions,longitudinalProfile,'--b')

        beams = Beams(fill1,fill2,energy=energy,intensity = intensity,emittance = emittance,sigs=sigs,sigp=sigp)
        basis = Basis(beams.getNBunchB1(),beams.getNBunchB2(),nSlice,nRing,nDim,equiDistantRing=False)
        longitudinalProfile = basis.getLongitudinalProfile()
        plt.plot(longitudinalPositions,longitudinalProfile,'or')
        longitudinalProfile = basis.getLongitudinalProfileFromWeight()
        plt.plot(longitudinalPositions,longitudinalProfile,'xr')

    if False:
        fullProfile = np.zeros_like(longitudinalPositions)
        for iSlice in range(nSlice):
            for iRing in range(nRing):
                plt.figure(1001)
                longitudinalProfile = basis.getElementLongitudinalProfile(iSlice,iRing)
                plt.plot(longitudinalPositions,longitudinalProfile)
                fullProfile += longitudinalProfile
        plt.plot(longitudinalPositions,fullProfile,'-k')

    if False:
        alphas = np.arange(0.0,2.0*np.pi,1E-3)
        R = 3.0
        for iSlice in range(nSlice):
            for iRing in range(nRing):
                plt.figure(iSlice*nRing+iRing)
                color = 'k'
                plt.plot([0.0,R],[0.0,0.0],color=color,alpha=0.1)
                for k in range(1,nSlice):
                    alpha1 = basis.getSliceAngle(k-1)
                    alpha0 = basis.getSliceAngle(k)
                    alpha = (alpha0+alpha1)/2.0
                    plt.plot([0.0,R*np.cos(alpha)],[0.0,R*np.sin(alpha)],color=color,alpha=0.1)
                for k in range(len(basis._ringDistribution[2])):
                    plt.plot(basis._ringDistribution[2][k]*np.cos(alphas),basis._ringDistribution[2][k]*np.sin(alphas),'-',color=color,alpha=0.1)
                plt.plot(basis.getSPosition(iSlice,iRing,1),basis.getDPoverP(iSlice,iRing,1,1),'ob')
                plt.twinx()
                longitudinalProfile = basis.getElementLongitudinalProfile(iSlice,iRing)
                print(np.sum(longitudinalProfile))
                plt.plot(longitudinalPositions,longitudinalProfile)

    plt.show()
