import numpy as np
from matplotlib import pyplot as plt
from scipy import constants as cst

from BimBim.Beams import Beams
from BimBim.Basis import Basis
from BimBim.System import System

from BimBim.Action.Transport import Transport
from BimBim.Action.BeamBeam import BeamBeam

if __name__ == '__main__':
    n_slice = 5
    n_ring = 1

    energy = 45.6
    electron_mass = cst.value('electron mass energy equivalent in MeV')*1E-3
    phys_emit_x = 0.27E-9
    phys_emit_y = 1.0E-12
    qx = 0.57/2
    qy = 0.61/2
    qs = 18E-3
    sigma_z = 12.1E-3
    sigma_dpp = 1.32E-3

    betaStarX=0.15
    betaStarY=0.8E-3
    Xing = 0.0

    sigma_x = np.sqrt(betaStarX*phys_emit_x)
    sigma_y = np.sqrt(betaStarY*phys_emit_y)
    gamma = energy/electron_mass
    r0 = cst.e**2/(4*np.pi*cst.epsilon_0*cst.m_e*cst.c**2)
    piwinski = sigma_z*np.tan(Xing/2)/sigma_x

    plane = 'x'
    intensities = np.arange(2E8,6E9,1E8)
    xis_x = intensities*r0*betaStarX/(2*np.pi*gamma*sigma_x*np.sqrt(1+piwinski**2)*(sigma_x*np.sqrt(1+piwinski**2)+sigma_y))
    coherentTunes = np.zeros((len(intensities),2*n_slice*n_ring),dtype=complex)
    beams = Beams(stringRep=['1 1 1 0','1 1 1 0'],energy=energy,phys_emit_x = phys_emit_x,phys_emit_y = phys_emit_y,intensity = 0.0,sigma_z=sigma_z,sigma_dpp=sigma_dpp,particleMass=electron_mass,particleCharge=[1.0,-1.0])
    basis = Basis([beams.getNBunch(1),beams.getNBunch(2)], n_slice, n_ring,plane=plane,equiDistantRing = False)
    actionSequence = beams.getEmptyActionSequence()
    actionSequence[0] = BeamBeam(betaStar_x = betaStarX, betaStar_y = betaStarY,hourglass=False,virtualDrift=False)
    actionSequence[1] = Transport(phase_x = qx,phase_y = qy,beta_x=betaStarX,beta_y=betaStarY,phase_z=qs)
    system = System(beams,actionSequence,basis)
    for iintensity,intensity in enumerate(intensities):
        beams.getBunch(0,beam=1).intensity = intensity
        beams.getBunch(0,beam=2).intensity = intensity
        oneTurn = system.buildOneTurnMap()
        eigvals,eigvecs = np.linalg.eig(oneTurn)
        coherentTunes0 = np.log(eigvals)*1j/(2*np.pi)
        mask = np.real(coherentTunes0)>0
        coherentTunes0 = coherentTunes0[mask]
        coherentTunes[iintensity,:] = coherentTunes0[np.argsort(np.imag(coherentTunes0))]

    plt.figure(0)
    for i in range(np.shape(coherentTunes)[1]):
        plt.plot(xis_x,np.real(coherentTunes[:,i])-qx,'ob');

    plane = 'y'
    intensities = np.arange(2E8,5E9,1E8)
    xis_y = intensities*r0*betaStarY/(2*np.pi*gamma*(sigma_x*np.sqrt(1+piwinski**2)+sigma_y)*sigma_y)
    coherentTunes = np.zeros((len(intensities),2*n_slice*n_ring),dtype=complex)
    beams = Beams(stringRep=['1 1 1 0','1 1 1 0'],energy=energy,phys_emit_x = phys_emit_x,phys_emit_y = phys_emit_y,intensity = 0.0,sigma_z=sigma_z,sigma_dpp=sigma_dpp,particleMass=electron_mass,particleCharge=[1.0,-1.0])
    basis = Basis([beams.getNBunch(1),beams.getNBunch(2)], n_slice, n_ring,plane=plane,equiDistantRing = False)
    actionSequence = beams.getEmptyActionSequence()
    actionSequence[0] = BeamBeam(betaStar_x = betaStarX, betaStar_y = betaStarY,hourglass=False,virtualDrift=False,XYCoupling=False)
    actionSequence[1] = Transport(phase_x = qx,phase_y = qy,beta_x=betaStarX,beta_y=betaStarY,phase_z=qs)
    system = System(beams,actionSequence,basis)
    for iintensity,intensity in enumerate(intensities):
        beams.getBunch(0,beam=1).intensity = intensity
        beams.getBunch(0,beam=2).intensity = intensity
        oneTurn = system.buildOneTurnMap()
        eigvals,eigvecs = np.linalg.eig(oneTurn)
        coherentTunes0 = np.log(eigvals)*1j/(2*np.pi)
        mask = np.real(coherentTunes0)>0
        coherentTunes0 = coherentTunes0[mask]
        coherentTunes[iintensity,:] = coherentTunes0[np.argsort(np.imag(coherentTunes0))]

    plt.figure(1)
    for i in range(np.shape(coherentTunes)[1]):
        plt.plot(xis_y,np.real(coherentTunes[:,i])-qy,'ob');

    plane = ['x','y']
    intensities = np.arange(2E8,5E9,1E8)
    xis_y = intensities*r0*betaStarY/(2*np.pi*gamma*(sigma_x*np.sqrt(1+piwinski**2)+sigma_y)*sigma_y)
    coherentTunes = np.zeros((len(intensities),4*n_slice*n_ring),dtype=complex)
    beams = Beams(stringRep=['1 1 1 0','1 1 1 0'],energy=energy,phys_emit_x = phys_emit_x,phys_emit_y = phys_emit_y,intensity = 0.0,sigma_z=sigma_z,sigma_dpp=sigma_dpp,particleMass=electron_mass,particleCharge=[1.0,-1.0])
    basis = Basis([beams.getNBunch(1),beams.getNBunch(2)], n_slice, n_ring,plane=plane,equiDistantRing = False)
    actionSequence = beams.getEmptyActionSequence()
    actionSequence[0] = BeamBeam(betaStar_x = betaStarX, betaStar_y = betaStarY,hourglass=False,virtualDrift=False,XYCoupling=False)
    actionSequence[1] = Transport(phase_x = qx,phase_y = qy,beta_x=betaStarX,beta_y=betaStarY,phase_z=qs)
    system = System(beams,actionSequence,basis)
    for iintensity,intensity in enumerate(intensities):
        beams.getBunch(0,beam=1).intensity = intensity
        beams.getBunch(0,beam=2).intensity = intensity
        oneTurn = system.buildOneTurnMap()
        eigvals,eigvecs = np.linalg.eig(oneTurn)
        coherentTunes0 = np.log(eigvals)*1j/(2*np.pi)
        mask = np.real(coherentTunes0)>0
        coherentTunes0 = coherentTunes0[mask]
        coherentTunes[iintensity,:] = coherentTunes0[np.argsort(np.imag(coherentTunes0))]

    plt.figure(2)
    for i in range(np.shape(coherentTunes)[1]):
        plt.plot(xis_y,np.real(coherentTunes[:,i]),'ob');
    plt.axhline(qx,color='g',ls='--')
    plt.axhline(qy,color='r',ls='--')

    plt.show()

