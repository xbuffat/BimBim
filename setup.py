# copyright ############################### #
# This file is part of the Xtrack Package.  #
# Copyright (c) CERN, 2021.                 #
# ######################################### #

from setuptools import setup, find_packages
from pathlib import Path


version_file = Path(__file__).parent / 'BimBim/_version.py'
dd = {}
with open(version_file.absolute(), 'r') as fp:
    exec(fp.read(), dd)
__version__ = dd['__version__']

setup(
    name='BimBim',
    version=__version__,
    description='Implementation of the circulant matrix model for collective beam dynamics',
    long_description='Implementation of the circulant matrix model for collective beam dynamics',
    author='X. Buffat et al.',
    project_urls={
            "Source Code": "https://gitlab.cern.ch/xbuffat/BimBim",
        },
    packages=find_packages(),
    include_package_data=True,
    install_requires=[
        'numpy>=1.0',
        'scipy',
        ],
    )
