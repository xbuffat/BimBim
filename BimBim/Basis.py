import time
import scipy.sparse as spm
import numpy as np
from BimBim.Error import BimBimError
from BimBim.Matrix import printMatrix, sparse_insert
from BimBim.Distribution import Gaussian
from scipy.interpolate import RectBivariateSpline

# Description : beam X bunch X ring X slice X transverse plane
# nBunch and plane can be lists to specify the two beams
class Basis:
    def __init__(self, nBunch=1, nSlice=0, nRing=0, plane = 'x', equiDistantRing=False, distribution=None,
                 angularShift=0.0):
        self.plane = np.atleast_1d(plane)
        if len(plane) == 1:
            if plane[0] != 'x' and plane[0] != 'y':
                raise BimBimError(f"Basis: {plane[0]} is not a valid plane, pick 'x' or 'y'")
        elif len(plane) > 1:
            if plane[0] != 'x' or plane[1] != 'y':
                raise BimBimError("Basis: The only 2D option is plane['x','y']")
        nBunch_tmp = np.atleast_1d(nBunch)
        self.nBunchB1 = nBunch_tmp[0]
        self.nBeam = 1
        if len(nBunch_tmp) > 1:
            self.nBunchB2 = nBunch_tmp[1]
            self.nBeam = 2
        else:
            self.nBunchB2 = 0
        self.equiDistantRing = False
        self.ringDistribution = None
        self.minAmpl = 0.0
        self.maxAmpl = 4.0  # sigma
        self.bunchBlockSize = 0
        self.beamBlockSizeB1 = 0
        self.beamBlockSizeB2 = 0
        self.nSlice = nSlice
        self.nRing = nRing
        self.nDim = 2*len(self.plane)
        self.size = (self.nBunchB1+self.nBunchB2)*self.nSlice*self.nRing*self.nDim
        self.bunchBlockSize = self.nSlice*self.nRing*self.nDim
        self.beamBlockSizeB1 = self.nBunchB1*self.bunchBlockSize
        self.beamBlockSizeB2 = self.nBunchB2*self.bunchBlockSize
        self.equiDistantRing = equiDistantRing
        self.angularShift = angularShift
        if distribution==None:
            self.distribution = Gaussian()
        else:
            self.distribution = distribution
        self.ringDistribution = self.distribution.generateRingDistribution(self.nRing,self.minAmpl,self.maxAmpl,self.equiDistantRing)

    def getNBunch(self, beam=1):
        if beam == 1:
            return self.nBunchB1
        else:
            return self.nBunchB2

    def getIndexForBeam(self, beam):
        if beam == 1:
            return 0
        else:
            return self.nBunchB1*self.bunchBlockSize
    
    def getIndexForBunch(self, bunch, beam=1):
        if beam == 1:
            return bunch*self.bunchBlockSize
        else:
            return (self.nBunchB1 + bunch)*self.bunchBlockSize
    
    def getIndexForSlice(self, iSlice, iRing, beam=1, bunch=0):
        retVal = bunch*self.bunchBlockSize + self.nDim*(iRing*self.nSlice + iSlice)
        if beam == 1:
            return retVal
        else:
            retVal += self.nBunchB1*self.bunchBlockSize
            return retVal

    def getDistributionFunction(self,r):
        return self.distribution(r)
        
    def getBeamBlockSize(self, beam):
        if beam == 1:
            return self.beamBlockSizeB1
        else:
            return self.beamBlockSizeB2

    def getBunchProjection(self, beam, bunch, inputMatrix):
        if spm.issparse(inputMatrix):
            if inputMatrix.get_shape() != (self.bunchBlockSize,self.bunchBlockSize):
                raise BimBimError('Matrix could not be projected : wrong shape '+str(inputMatrix.get_shape())+' instead of'+str((self.bunchBlockSize,self.bunchBlockSize)))
            matrix = inputMatrix.tolil()
        else:
            if np.shape(inputMatrix) != (self.bunchBlockSize,self.bunchBlockSize):
                raise BimBimError('Matrix could not be projected : wrong shape '+str(inputMatrix.get_shape())+' instead of'+str((self.bunchBlockSize,self.bunchBlockSize)))
            matrix = inputMatrix

        if self.nBeam == 1 and self.nBunchB1 == 1:
            return spm.lil_matrix(inputMatrix)
        else:
            proj = spm.identity(self.size,format='lil',dtype=inputMatrix.dtype)
            index = self.getIndexForBunch(bunch,beam)
            sparse_insert(proj,inputMatrix,index)
            #myFile = open('debug_proj.mat','w')
            #printMatrix(myFile,proj)
            #myFile.close()
            return proj
        
    #Project the matrix for a single bunch into the basis (all line i.e. all bunch depdencies)
    def getFullBunchProjection(self,beam,bunch,inputMatrix):
        if spm.issparse(inputMatrix):
            if inputMatrix.get_shape() != (self.bunchBlockSize,self.getBeamBlockSize(beam)):
                raise BimBimError('Matrix could not be projected : wrong shape '+str(inputMatrix.get_shape())+' instead of'+str((self.bunchBlockSize,self.getBeamBlockSize(beam))))
            matrix = inputMatrix.tolil()
        else:
            if np.shape(inputMatrix) != (self.bunchBlockSize,self.getBeamBlockSize(beam)):
                raise BimBimError('Matrix could not be projected : wrong shape '+str(inputMatrix.get_shape())+' instead of'+str((self.bunchBlockSize,self.getBeamBlockSize(beam))))
            matrix = inputMatrix
        if self.nBeam == 1 and self.nBunchB1 == 1:
            return spm.lil_matrix(inputMatrix)
        else:
            proj = spm.identity(self.size,format='lil',dtype=inputMatrix.dtype)
            sparse_insert(proj,matrix,self.getIndexForBunch(bunch,beam),self.getIndexForBeam(beam))
            #myFile = open('debug_proj.mat','w')
            #printMatrix(myFile,proj)
            #myFile.close()
            return proj
        
    #project the matrix of two bunches into the basis
    def getTwoBunchProjection(self,bunchB1,bunchB2,inputMatrix):
        if spm.issparse(inputMatrix):
            if np.shape(inputMatrix) != (2*self.bunchBlockSize,2*self.bunchBlockSize):
                raise BimBimError('Matrix could not be projected : wrong shape '+str(np.shape(inputMatrix))+' instead of'+str((2*self.bunchBlockSize,2*self.bunchBlockSize)))
        else:
            if np.shape(inputMatrix) != (2*self.bunchBlockSize,2*self.bunchBlockSize):
                raise BimBimError('Matrix could not be projected : wrong shape '+str(np.shape(inputMatrix))+' instead of'+str((2*self.bunchBlockSize,2*self.bunchBlockSize)))

        if self.nBunchB1 == 1 and self.nBunchB2 == 1:
            return spm.lil_matrix(inputMatrix)
        else:
            retVal = spm.identity(self.size,format='lil',dtype=inputMatrix.dtype)
            index1 = self.getIndexForBunch(bunchB1,1)
            index2 = self.getIndexForBunch(bunchB2,2)
            for i in range(self.bunchBlockSize):
                for j in range(self.bunchBlockSize):
                    retVal[i+index1,j+index1] = inputMatrix[i,j] #copy diagonal term of B1 bunch
                    retVal[i+index2,j+index2] = inputMatrix[i+self.bunchBlockSize,j+self.bunchBlockSize] #copy diagonal term of B2 bunch
                    retVal[i+index1,j+index2] = inputMatrix[i,j+self.bunchBlockSize] # copy upper left block
                    retVal[i+index2,j+index1] = inputMatrix[i+self.bunchBlockSize,j] # copy lower right block
            return retVal
        
    # project the matrix of interaction of two element into the basis
    def getTwoElementProjection(self,bunchB1,iSliceB1,iRingB1,bunchB2,iSliceB2,iRingB2,inputMatrix,inputIsDense=False):
        if inputIsDense:
            if np.shape(inputMatrix) != (2*self.nDim,2*self.nDim):
                raise BimBimError('Matrix could not be projected : wrong shape '+str(np.shape(inputMatrix))+' instead of'+str((2*self.bunchBlockSize,2*self.bunchBlockSize)))
        else:
            if np.shape(inputMatrix) != (2*self.nDim,2*self.nDim):
                raise BimBimError('Matrix could not be projected : wrong shape '+str(np.shape(inputMatrix))+' instead of'+str((2*self.bunchBlockSize,2*self.bunchBlockSize)))
        retVal = spm.identity(self.size,format='lil',dtype=inputMatrix.dtype)
        index1 = self.getIndexForSlice(iSliceB1,iRingB1,1,bunchB1)
        index2 = self.getIndexForSlice(iSliceB2,iRingB2,2,bunchB2)
        for i in range(self.nDim):
            for j in range(self.nDim):
                retVal[i+index1,j+index1] = inputMatrix[i,j]
                retVal[i+index2,j+index2] = inputMatrix[i+self.nDim,j+self.nDim]
                retVal[i+index1,j+index2] = inputMatrix[i,j+self.nDim]
                retVal[i+index2,j+index1] = inputMatrix[i+self.nDim,j]
        return retVal

    # dot product optimised for the interaction of two elements (only the required lines are computed, fullMat is updated without copy)
    #   binaryInteractionMat : nDim x nDim matrix representing the interaction of element (iSlice1,iRing1,beam1,bunch1) and (iSlice2,iRing2,beam2,bunch2)
    #   fullMat : Basis Size x Basis Size matrix
    def binaryInteractionOptimisedDotProduct(self,binaryInteractionMat,fullMat,iSlice1,iRing1,beam1,bunch1,iSlice2,iRing2,beam2,bunch2):
        index1 = self.getIndexForSlice(iSlice1,iRing1,beam1,bunch1)
        index2 = self.getIndexForSlice(iSlice2,iRing2,beam2,bunch2)
        indices = np.append(np.arange(self.nDim,dtype=int)+index1,np.arange(self.nDim,dtype=int)+index2)
        fullMat[indices,:] = np.dot(binaryInteractionMat,fullMat[indices,:])

    def getRingRadius(self, iRing):
        return self.ringDistribution[0][iRing]

    def getRingsRadii(self):
        return self.ringDistribution[0]

    def getRingWeight(self, iRing):
        return self.ringDistribution[1][iRing]

    def getRingBoundaries(self, iRing):
        return self.ringDistribution[2][iRing], self.ringDistribution[2][iRing+1]

    def getWeight(self, iSlice, iRing):
        return self.getRingWeight(iRing)/self.nSlice

    def getWeightMatrix(self):
        retVal = np.zeros(shape=(self.nSlice*self.nRing,self.nSlice*self.nRing))
        for line in range(self.nSlice*self.nRing):
            for iSlice in range(self.nSlice):
                for iRing in range(self.nRing):
                    retVal[line,iSlice + iRing*self.nSlice] = self.getWeight(iSlice, iRing)
        return retVal

    def getSliceAngle(self, iSlice):
        return (iSlice*2+1)*np.pi/float(self.nSlice)+self.angularShift

    def getSlicesAngles(self):
        basisAngles = np.arange(0, 2.0*np.pi, 2.0*np.pi/self.nSlice)+np.pi/self.nSlice+self.angularShift
        return basisAngles[:self.nSlice]

    def getSPosition(self, iSlice, iRing, sigma_z):
        return self.getRingRadius(iRing)*sigma_z*np.cos(self.getSliceAngle(iSlice))

    def getDPoverP(self, iSlice, iRing, sigma_dpp, transition):
        return transition*self.getRingRadius(iRing)*sigma_dpp*np.sin(self.getSliceAngle(iSlice))

    def getSDiff(self, iSliceTest, iRingTest, iSliceSource, iRingSource, sigma_z, threshold=1E-12):
        retVal = self.getSPosition(iSliceSource, iRingSource, sigma_z)-self.getSPosition(iSliceTest, iRingTest, sigma_z)
        #if iSliceSource == 2 and iSliceTest == 2 and iRingSource == 1 and iRingTest == 0:
        #    print(self.getSPosition(iSliceSource,iRingSource,1.0),self.getSPosition(iSliceTest,iRingTest,1.0),retVal)
        if abs(retVal) > threshold:
            return retVal
        else:
            return 0.0

    def getDiscretisationIndicesFromFullVectorIndex(self, index):
        intraBunchIndex = index%self.bunchBlockSize
        bunch = int((index-intraBunchIndex)/self.bunchBlockSize)
        if bunch < self.nBunchB1:
            beam = 1
        else:
            beam = 2
            bunch -= self.nBunchB1
        dim = intraBunchIndex%self.nDim
        intraBunchIndex = int((intraBunchIndex-dim)/self.nDim)
        iRing = intraBunchIndex%self.nRing
        iSlice = int((intraBunchIndex-iRing)/self.nRing)
        return iSlice, iRing, dim, beam, bunch

    def getLongitudinalPhaseSpacePositionFromFullVectorIndex(self, index, sigma_z, sigma_dpp, transition=-1):
        iSlice,iRing,dim,beam,bunch = self.getDiscretisationIndicesFromFullVectorIndex(index)
        return self.getSPosition(iSlice, iRing, sigma_z),self.getDPoverP(iSlice, iRing, sigma_dpp, transition)

    #dof = 0 for horizontal position, 1 for horizontal divergence, 2 for vertical position, 2 for vertical divergence
    def getModeFirstOrderMoment(self,eigvec,dof=0):
        eigvec = np.ravel(eigvec)
        if len(eigvec)!=self.size:
            raise BimBimError('Eigenvector does not have the right dimension: '+str(len(eigvec))+' but should be '+str(self.size))
        if dof > self.nDim:
            raise BimBimError('Cannot compute first order moment for request degree of freedom '+str(dof)+' > '+str(self.nDim)+'D')

        retValB1 = []
        retValB2 = []
        for bunch in range(self.nBunchB1):
            retValB1.append(0.0)
            norm = 0.0
            for iSlice in range(self.nSlice):
                for iRing in range(self.nRing):
                    norm += np.abs(eigvec[self.getIndexForSlice(iSlice,iRing,1,bunch)+dof])**2 # Not clear why this norm is not always 0.5
                    retValB1[bunch] += eigvec[self.getIndexForSlice(iSlice,iRing,1,bunch)+dof]*self.getWeight(iSlice,iRing)
            retValB1[bunch] /= np.sqrt(norm/self.nSlice/self.nRing)
        for bunch in range(self.nBunchB2):
            retValB2.append(0.0)
            tmp1 = 0.0
            tmp2 = 0.0
            for iSlice in range(self.nSlice):
                for iRing in range(self.nRing):
                    norm += np.abs(eigvec[self.getIndexForSlice(iSlice,iRing,2,bunch)+dof])**2
                    retValB2[bunch] += eigvec[self.getIndexForSlice(iSlice,iRing,2,bunch)+dof]*self.getWeight(iSlice,iRing)
            retValB2[bunch] /= np.sqrt(norm/self.nSlice/self.nRing)
        return retValB1,retValB2

    #dof = 0 for horizontal position, 1 for horizontal divergence, 2 for vertical position, 2 for vertical divergence
    def getModeCrossProduct(self,eigvec1,eigvec2,dof=0,beam=1,bunch=0):
        eigvec1 = np.ravel(eigvec1)
        eigvec2 = np.ravel(eigvec2)
        if len(eigvec1)!=self.size:
            raise BimBimError('Eigenvector 1 does not have the right dimension: '+str(len(eigvec1))+' but should be '+str(self.size))
        if len(eigvec2)!=self.size:
            raise BimBimError('Eigenvector 2 does not have the right dimension: '+str(len(eigvec2))+' but should be '+str(self.size))
        if dof > self.nDim:
            raise BimBimError('Cannot compute first order moment for request degree of freedom '+str(dof)+' > '+str(self.nDim)+'D')
        retVal = 0.0
        norm1 = 0.0
        norm2 = 0.0
        for iSlice in range(self.nSlice):
            for iRing in range(self.nRing):
                norm1 += np.abs(eigvec1[self.getIndexForSlice(iSlice,iRing,1,bunch)+dof])**2 # Not clear why this norm is not always 0.5
                norm2 += np.abs(eigvec2[self.getIndexForSlice(iSlice,iRing,1,bunch)+dof])**2 # Not clear why this norm is not always 0.5
                retVal += eigvec1[self.getIndexForSlice(iSlice,iRing,1,bunch)+dof]*np.conj(eigvec2[self.getIndexForSlice(iSlice,iRing,1,bunch)+dof])*self.getWeight(iSlice,iRing)
        retVal /= np.sqrt(norm1*norm2)/self.nSlice/self.nRing
        return retVal

    #dof = 0 for horizontal position, 1 for horizontal divergence, 2 for vertical position, 2 for vertical divergence
    def getModeCrabMoment(self,eigvec,dof=0,sigma_z=1):
        eigvec = np.ravel(eigvec)
        if len(eigvec)!=self.size:
            raise BimBimError('Eigenvector does not have the right dimension: '+str(len(eigvec))+' but should be '+str(self.size))
        if dof > self.nDim:
            raise BimBimError('Cannot compute first order moment for request degree of freedom '+str(dof)+' > '+str(self.nDim)+'D')

        retValB1 = []
        retValB2 = []
        for bunch in range(self.nBunchB1):
            retValB1.append(0.0)
            norm = 0.0
            for iSlice in range(self.nSlice):
                for iRing in range(self.nRing):
                    norm += np.abs(eigvec[self.getIndexForSlice(iSlice,iRing,1,bunch)+dof])**2 # Not clear why this norm is not always 0.5
                    retValB1[bunch] += self.getSPosition(iSlice,iRing,sigma_z)*eigvec[self.getIndexForSlice(iSlice,iRing,1,bunch)+dof]*self.getWeight(iSlice,iRing)
            retValB1[bunch] /= np.sqrt(norm/self.nSlice/self.nRing)
        for bunch in range(self.nBunchB2):
            retValB2.append(0.0)
            tmp1 = 0.0
            tmp2 = 0.0
            for iSlice in range(self.nSlice):
                for iRing in range(self.nRing):
                    norm += np.abs(eigvec[self.getIndexForSlice(iSlice,iRing,2,bunch)+dof])**2
                    retValB2[bunch] += self.getSPosition(iSlice,iRing,sigma_z)*eigvec[self.getIndexForSlice(iSlice,iRing,2,bunch)+dof]*self.getWeight(iSlice,iRing)
            retValB2[bunch] /= np.sqrt(norm/self.nSlice/self.nRing)
        return retValB1,retValB2

    def getRadialComponent(self,eigvec,weighted=True):
        __dummy,radialComponent = self.getAzimuthalModeNumber(eigvec,returnRadialComponent=True,weighted=weighted)
        return radialComponent
        

    # TODO RecBivariateSpline does not work for too few rings
    def getAzimuthalModeNumber(self,eigvec,returnRadialComponent=False,azimuthalOversamplingFactor=10,weighted=True):
        eigvec = np.ravel(eigvec)
        if len(eigvec)!=self.size:
            raise BimBimError('Eigenvector does not have the right dimension: '+str(len(eigvec))+' but should be '+str(self.size))
        interpolationAngles = np.arange(0, 2.0*np.pi, 2.0*np.pi/(azimuthalOversamplingFactor*self.nSlice))
        interpolationRadii_mesh,interpolationAngles_mesh = np.meshgrid(self.ringDistribution[0],interpolationAngles)

        matRep = self.getSingleBunchModeComplexMatrixRepresentation(eigvec)
        interpolatorReal = RectBivariateSpline(self.getSlicesAngles(),self.ringDistribution[0],np.real(matRep))
        interpolatedMatRepReal = interpolatorReal(interpolationAngles,self.ringDistribution[0])

        myMax = 0.0
        azimuthalMode = -1
        maxRadialComponent = None
        for testAzimuthalMode in range(int(np.floor(self.nSlice/2+0.5))):
            reducedMatRep = interpolatedMatRepReal*np.exp(1j*interpolationAngles_mesh*testAzimuthalMode)
            testRadialComponent = np.sum(reducedMatRep,axis=0)
            if np.max(np.abs(testRadialComponent)) > myMax:
                myMax = np.max(np.abs(testRadialComponent))
                azimuthalMode = testAzimuthalMode
                radialComponent = testRadialComponent

        if returnRadialComponent:
            index = np.argmax(np.abs(radialComponent))
            angle = np.angle(radialComponent[index])
            radialComponent *= np.exp(-1j*angle)
            if weighted:
                radialComponent*=self.ringDistribution[1]
            return azimuthalMode,np.real(radialComponent)
        else:
            return azimuthalMode

    def getSingleBunchModeComplexMatrixRepresentation(self,eigvec,dof=0,beam=1,bunch=0):
        eigvec = np.ravel(eigvec)
        if len(eigvec)!=self.size:
            raise BimBimError('Eigenvector does not have the right dimension: '+str(len(eigvec))+' but should be '+str(self.size))
        if dof > self.nDim:
            raise BimBimError('Cannot compute first order moment for request degree of freedom '+str(dof)+' > '+str(self.nDim)+'D')
        retMat = np.zeros((self.nSlice,self.nRing),dtype=complex)
        for iSlice in range(self.nSlice):
            for iRing in range(self.nRing):
                retMat[iSlice,iRing] = eigvec[self.getIndexForSlice(iSlice,iRing,beam,bunch)+dof]
        return retMat

    def getSingleBunchModeMatrixRepresentation(self,eigvec,dof=0,beam=1,bunch=0):
        return np.real(self.getSingleBunchModeComplexMatrixRepresentation(eigvec,dof=dof,beam=beam,bunch=bunch))

    #TODO handle separatly nSlice < 2 or can we still use the interpolator?
    def getInterpolatedSingleBunchModeMatrixRepresentation(self,eigvec,basis,dof=0,beam=1,bunch=0):
        matRep = basis.getSingleBunchModeMatrixRepresentation(eigvec,dof=dof,beam=beam,bunch=bunch)
        interpolator = RectBivariateSpline(basis.getSlicesAngles(),basis.getRingsRadii(),matRep)
        return interpolator(self.getSlicesAngles(),self.getRingsRadii())

    def getLongitudinalProfileDiscretePoints(self,nZ=100):
        return np.arange(-self.maxAmpl,self.maxAmpl+self.maxAmpl/nZ,2.0*self.maxAmpl/nZ)

    def getElementLongitudinalProfileFromWeight(self,iSlice,iRing=0,nTheta=1000,nR=100,nZ=100):
        longitudinalProfile = np.zeros_like(self.getLongitudinalProfileDiscretePoints(nZ))
        dTheta = 2.0*np.pi/self.nSlice/nTheta
        for theta in np.arange(self.getSliceAngle(iSlice)-np.pi/self.nSlice,self.getSliceAngle(iSlice)+np.pi/self.nSlice,dTheta):
            dR = (self.ringDistribution[2][iRing+1]-self.ringDistribution[2][iRing])/nR
            for R in np.arange(self.ringDistribution[2][iRing],self.ringDistribution[2][iRing+1],dR):
                zIndex = int(np.floor(nZ*R*np.cos(theta)/(2.0*self.maxAmpl)+nZ/2+0.5))
                longitudinalProfile[zIndex] += self.getWeight(iSlice,iRing)/nTheta/nR/(2.0*np.pi) # Why 2pi?
        return longitudinalProfile*nZ

    def getLongitudinalProfileFromWeight(self,nTheta=100,nR=100,nZ=100):
        longitudinalProfile = np.zeros_like(self.getLongitudinalProfileDiscretePoints(nZ))
        for iSlice in range(self.nSlice):
            for iRing in range(self.nRing):
                longitudinalProfile += self.getElementLongitudinalProfileFromWeight(iSlice,iRing,nTheta,nR,nZ)

        return longitudinalProfile

    def getElementLongitudinalProfile(self,iSlice,iRing=0,nTheta=1000,nR=100,nZ=100):
        longitudinalProfile = np.zeros_like(self.getLongitudinalProfileDiscretePoints(nZ))
        dTheta = 2.0*np.pi/self.nSlice/nTheta
        for theta in np.arange(self.getSliceAngle(iSlice)-np.pi/self.nSlice,self.getSliceAngle(iSlice)+np.pi/self.nSlice,dTheta):
            dR = (self.ringDistribution[2][iRing+1]-self.ringDistribution[2][iRing])/nR
            for R in np.arange(self.ringDistribution[2][iRing],self.ringDistribution[2][iRing+1],dR):
                zIndex = int(np.floor(nZ*R*np.cos(theta)/(2.0*self.maxAmpl)+nZ/2+0.5))
                longitudinalProfile[zIndex] += R*np.exp(-R**2/2)*dR*dTheta/(2.0*np.pi)**2  # Why 2pi?
        return longitudinalProfile*nZ

    def getLongitudinalProfile(self,nTheta=100,nR=100,nZ=100):
        longitudinalProfile = np.zeros_like(self.getLongitudinalProfileDiscretePoints(nZ))
        for iSlice in range(self.nSlice):
            for iRing in range(self.nRing):
                longitudinalProfile += self.getElementLongitudinalProfile(iSlice,iRing,nTheta,nR,nZ)

        return longitudinalProfile

    def getTransverseSignal(self,eigvec,beam=1,bunch=0,dof=0,nTheta=100,nR=100,nZ=100):
        transverseSignal = np.zeros_like(self.getLongitudinalProfileDiscretePoints(nZ))
        for iSlice in range(self.nSlice):
            for iRing in range(self.nRing):
                transverseSignal += np.real(eigvec[self.getIndexForSlice(iSlice,iRing,beam,bunch)+dof])*self.getElementLongitudinalProfile(iSlice,iRing,nTheta,nR,nZ)
                #transverseSignal += np.real(eigvec[self.getIndexForSlice(iSlice,iRing,beam,bunch)+dof])*self.getElementLongitudinalProfileFromWeight(iSlice,iRing,nTheta,nR,nZ)
        return transverseSignal

    def getAverageAbsolutPosition(self, eigvec, beam=1, bunch=0, dof=0):
        averageAbsPos = 0
        for iSlice in range(self.nSlice):
            for iRing in range(self.nRing):
                averageAbsPos += abs(eigvec[self.getIndexForSlice(iSlice,iRing,beam,bunch)+dof])*self.getWeight(iSlice, iRing)
        return averageAbsPos

    def getPolarMesh(self):
        #return np.meshgrid(self.ringDistribution[0], np.linspace(0, 2.0*np.pi, self.nSlice))
        return np.meshgrid(self.ringDistribution[0], np.linspace(0, 2.0*np.pi, self.nSlice)+np.pi/self.nSlice)

    def cellsOverlapLongitudinaly(self,iSlice,iRing,jSlice,jRing):
        iLowerR,iUpperR = self.getRingBoundaries(iRing)
        iLowerAngle = self.getSliceAngle(iSlice)-np.pi/self.nSlice
        iUpperAngle = self.getSliceAngle(iSlice)+np.pi/self.nSlice
        iEdges = np.array([iLowerR*np.cos(iLowerAngle),iLowerR*np.cos(iUpperAngle),iUpperR*np.cos(iLowerAngle),iUpperR*np.cos(iUpperAngle)])
        jLowerR,jUpperR = self.getRingBoundaries(jRing)
        jLowerAngle = self.getSliceAngle(jSlice)-np.pi/self.nSlice
        jUpperAngle = self.getSliceAngle(jSlice)+np.pi/self.nSlice
        jEdges = np.array([jLowerR*np.cos(jLowerAngle),jLowerR*np.cos(jUpperAngle),jUpperR*np.cos(jLowerAngle),jUpperR*np.cos(jUpperAngle)]) 
        return np.min(iEdges) < np.max(jEdges)  and np.min(jEdges) < np.max(iEdges)


