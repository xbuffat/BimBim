import numpy as np
from scipy.special import erf,gamma
from BimBim.Error import BimBimError

class Gaussian:
    def getDescription(self):
        return 'Gaussian'

    # 2D Gaussian
    def __call__(self,r):
        return r/(2*np.pi)*np.exp(-r*r/2)

    def generateRingDistribution(self,nRing,minAmpl,maxAmpl,equiDistantRing):
        if nRing > 1:
            if equiDistantRing:
                dx = (maxAmpl - minAmpl)/nRing
                boundaries = dx*np.arange(nRing+1)
                #for i in range(nRing+1):
                #    boundaries.append(i*dx)
            else:
                boundaries = np.zeros(nRing+1,dtype=float)
                for i in range(1,nRing):
                    lbd = boundaries[i-1]
                    boundaries[i]= np.sqrt(-2.0*(np.log(np.exp(-lbd**2/2)-1.0/nRing)))
                boundaries[nRing] = maxAmpl
        else:
            boundaries = np.array([0.0,maxAmpl])
        
        radius = np.zeros(nRing)
        weight = np.zeros(nRing)
        for i in range(nRing):
            lbd = boundaries[i]
            ubd = boundaries[i+1]
            weight[i] = np.exp(-lbd**2/2)-np.exp(-ubd**2/2)
            radius[i] = (np.sqrt(np.pi/2)*(erf(ubd/np.sqrt(2))-erf(lbd/np.sqrt(2)))-(ubd*np.exp(-ubd**2/2)-lbd*np.exp(-lbd**2/2)))/weight[i]

        return [radius,weight,boundaries]

class AirBag:
    def __init__(self,width=0.01,radius=None):
        self._width = width
        if radius == None:
            self._radius = np.sqrt(2) # by default this matches the airbag radius to get an rms length of 1
        else:
            self._radius = radius

    def getDescription(self):
        return 'AirBag_W{:04f}_R{:04f}'.format(self._width,self._radius)

    def __call__(self,r):
        if np.isscalar(r):
            if (r < (self._radius+self._width/2)) and (r > (self._radius-self._width/2)):
                return 1/(2*np.pi*self._radius*self._width)
            else:
                return 0.0
        else:
            psi = np.zeros_like(r)
            mask = (r < (self._radius+self._width/2)) & (r > (self._radius-self._width/2))
            psi[mask] = 1/(2*np.pi*self._radius*self._width)
            return psi

    def generateRingDistribution(self,nRing=None,minAmpl=None,maxAmpl=None,equiDistantRing=None):
        boundaries = np.array([self._radius - self._width/2,self._radius + self._width/2])
        weight = np.array([1.0])
        radius = np.array([self._radius])

        return [radius,weight,boundaries]


#Note: if the equipopulated option is chosen, the position of the slices is determined based on the Gaussian without hole
class GaussianHoledGaussian:

    def getDescription(self):
        return 'GaussianHoledGaussian_r{:03f}-sig{:03f}-depth{:03f}'.format(self._r0,self._sigma,self._depth)

    def _getGaussianHole(self,r):
        return 1.0-self._depth*np.exp(-(r-self._r0)**2/(2*self._sigma**2))

    def _getGaussianRadialDistribution(self,r):
        return r*np.exp(-r**2/2)/(2.0*np.pi)

    def __init__(self,r0=0.0,sigma=1.0,depth=1.0,dr=0.00001):
        self._r0 = r0
        self._sigma = sigma
        self._depth = depth
        self._dr = dr
        self._rs = np.arange(0.0,6.0,self._dr)
        radialGauss = self._getGaussianRadialDistribution(self._rs)
        hole = self._getGaussianHole(self._rs)
        self._holedDistr = radialGauss*hole
        norm = (np.sum(self._holedDistr[1:-1])+0.5*(self._holedDistr[0]+self._holedDistr[1]))*dr
        self._holedDistr /= norm*2.0*np.pi

    def __call__(self,r):
        return np.interp(r,self._rs,self._holedDistr)

    def generateRingDistribution(self,nRing,minAmpl,maxAmpl,equiDistantRing):
        boundaries = np.zeros(nRing+1)
        if nRing > 1:
            if equiDistantRing:
                dx = (maxAmpl - minAmpl)/nRing
                boundaries = dx*np.arange(nRing+1)
                #for i in range(nRing+1):
                #    boundaries.append(i*dx)
            else:
                boundaries = np.zeros(nRing+1,dtype=float)
                for i in range(1,nRing):
                    lbd = boundaries[i-1]
                    boundaries[i]= np.sqrt(-2.0*(np.log(np.exp(-lbd**2/2)-1.0/nRing)))
                boundaries[nRing] = maxAmpl
        else:
            boundaries = np.array([0.0,maxAmpl])

        radius = np.zeros(nRing)
        weight = np.zeros(nRing)
        for i in range(nRing):
            mask = np.logical_and(self._rs>=boundaries[i],self._rs<boundaries[i+1])
            weight[i] = np.sum(self._holedDistr[mask])*self._dr*2.0*np.pi
            radius[i] = np.sum(self._rs[mask]*self._holedDistr[mask])/np.sum(self._holedDistr[mask])

        return [radius,weight,boundaries]
        
#Note: if the equipopulated option is chosen, the position of the slices is determined based on the Gaussian without hole
class SineHoledGaussian:

    def getDescription(self):
        return 'SineHoledGaussian_r{:03f}-sig{:03f}-depth{:03f}'.format(self._r0,self._sigma,self._depth)

    def __getSineHole(self,r):
        retVal = np.ones_like(r)
        mask = np.logical_and(r>=self._r0-self._sigma,r<self._r0+self._sigma)
        derivative = (1-self._r0**2)*np.exp(-self._r0**2/2)
        retVal[mask] += self._depth*np.sin(np.pi*(r[mask]-self._r0)/self._sigma)*(1-2*derivative*(r[mask]-self._r0))
        return retVal

    def _getSineHole(self,r):
        retVal = np.ones_like(r)
        mask = np.logical_and(r>=self._r0-self._sigma,r<self._r0+self._sigma)
        derivative = (1-self._r0**2)*np.exp(-self._r0**2/2)
        mask2 = np.logical_and(r>=self._r0,r<self._r0+self._sigma)
        scaling = np.ones_like(r)
        scaling[mask2] *= 1.0-2.0*self._sigma*derivative
        retVal[mask] += self._depth*np.sin(np.pi*(r[mask]-self._r0)/self._sigma)*scaling[mask]
        return retVal

    def _getGaussianRadialDistribution(self,r):
        return r*np.exp(-r**2/2)/(2.0*np.pi)

    def __init__(self,r0=0.0,sigma=1.0,depth=1.0,dr=0.00001):
        self._r0 = r0
        self._sigma = sigma
        self._depth = depth
        self._dr = dr
        self._rs = np.arange(0.0,6.0,self._dr)
        radialGauss = self._getGaussianRadialDistribution(self._rs)
        hole = self._getSineHole(self._rs)
        self._holedDistr = radialGauss*hole
        norm = (np.sum(self._holedDistr[1:-1])+0.5*(self._holedDistr[0]+self._holedDistr[1]))*dr
        self._holedDistr /= norm*2.0*np.pi

    def __call__(self,r):
        return np.interp(r,self._rs,self._holedDistr)

    def generateRingDistribution(self,nRing,minAmpl,maxAmpl,equiDistantRing):
        boundaries = np.zeros(nRing+1)
        if nRing > 1:
            if equiDistantRing:
                dx = (maxAmpl - minAmpl)/nRing
                boundaries = dx*np.arange(nRing+1)
                #for i in range(nRing+1):
                #    boundaries.append(i*dx)
            else:
                boundaries = np.zeros(nRing+1,dtype=float)
                for i in range(1,nRing):
                    lbd = boundaries[i-1]
                    boundaries[i]= np.sqrt(-2.0*(np.log(np.exp(-lbd**2/2)-1.0/nRing)))
                boundaries[nRing] = maxAmpl
        else:
            boundaries = np.array([0.0,maxAmpl])

        radius = np.zeros(nRing)
        weight = np.zeros(nRing)
        for i in range(nRing):
            mask = np.logical_and(self._rs>=boundaries[i],self._rs<boundaries[i+1])
            weight[i] = np.sum(self._holedDistr[mask])*self._dr*2.0*np.pi
            radius[i] = np.sum(self._rs[mask]*self._holedDistr[mask])/np.sum(self._holedDistr[mask])

        return [radius,weight,boundaries]

#Note: if the equipopulated option is chosen, the position of the slices is determined based on the Gaussian without hole
class QGaussian:
    def __init__(self,q,beta=1.0):
        if q >= 1.5:
            raise BimBimError('Error in Distribution.py: QGaussian is only defined for q smaller than 1.5 ('+str(q)+')')
        if beta != 1.0:
            print('WARNING: QGaussian with beta different from one effectively changes the rms bunch length ('+str(beta)+')')
        self._beta = beta
        self._q = q

    def getDescription(self):
        return 'QGaussian_beta{:.03f}-q{:-03f}'.format(self._beta,self._q)

    def __call__(self,inputR):
        r = np.atleast_1d(inputR)
        rMask = np.ones_like(r,dtype=bool)
        retVal = np.zeros_like(r)
        if np.abs(self._q-1.0)<1E-6:
            myQ = self._q+1E-6
            Kq2 = 2.0*np.pi
        elif self._q<1.0:
            myQ = self._q
            rMask = r**2 < self._beta**2*(6.0-4.0*self._q)/(1.0-self._q)
            Kq2 = (6.0-4.0*self._q)/(1.0-self._q)*np.pi*gamma((2.0-self._q)/(1.0-self._q))/gamma((2.0-self._q)/(1.0-self._q)+1.0)
        elif self._q<3.0/2.0:
            myQ = self._q
            Kq2 = (6.0-4.0*self._q)/(self._q-1.0)*np.pi*gamma(1.0/(self._q-1.0)-1.0)/gamma(1.0/(self._q-1.0))
        retVal[rMask] = r[rMask]*(1.0-(1.0-myQ)/(6.0-4.0*myQ)*r[rMask]**2/self._beta**2)**(1.0/(1.0-myQ))/(self._beta**2*Kq2)
        if len(r) == 0.0:
            return retVal[0]
        else:
            return retVal

    def generateRingDistribution(self,nRing,minAmpl,maxAmpl,equiDistantRing,nR=100):
        if nRing > 1:
            if equiDistantRing:
                dx = (maxAmpl - minAmpl)/nRing
                boundaries = dx*np.arange(nRing+1)
                #for i in range(nRing+1):
                #    boundaries.append(i*dx)
            else:
                boundaries = np.zeros(nRing+1,dtype=float)
                for i in range(1,nRing):
                    lbd = boundaries[i-1]
                    boundaries[i]= np.sqrt(-2.0*(np.log(np.exp(-lbd**2/2)-1.0/nRing)))
                boundaries[nRing] = maxAmpl
        else:
            boundaries = np.array([0.0,maxAmpl])
        
        radius = np.zeros(nRing)
        weight = np.zeros(nRing)
        for i in range(nRing):
            rs = np.linspace(boundaries[i],boundaries[i+1],nR)
            distr = self(rs)
            weight[i] = np.sum(distr)*(rs[2]-rs[1])*2.0*np.pi
            norm = np.sum(distr[1:-1])+0.5*distr[0]+0.5*distr[-1]
            if norm != 0.0:
                radius[i] = (np.sum(rs[1:-1]*distr[1:-1])+0.5*rs[0]*distr[0]+0.5*rs[-1]*distr[-1])/(norm)
            else:
                radius[i] = (boundaries[i]+boundaries[i+1])/2

        return [radius,weight,boundaries]
        
