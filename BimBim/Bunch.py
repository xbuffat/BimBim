import numpy as np
from scipy import constants as cst

class Bunch:
    
    def __init__(self,number,energy,intensity,phys_emit_x,phys_emit_y,sigma_z,sigma_dpp,longitudinal_position=0,particleMass=0.93827231,particleCharge = 1):
        self.number = number
        self.energy = energy #GeV
        self.particleMass = particleMass
        self.phys_emit_x = phys_emit_x
        self.phys_emit_y = phys_emit_y
        self.intensity = intensity # number of charges (not number of particles)
        self.particleCharge = particleCharge
        self.sigma_z = sigma_z
        self.sigma_dpp = sigma_dpp
        self.longitudinal_position = longitudinal_position
        self.r0 = (particleCharge*cst.e)**2/(4*np.pi*cst.epsilon_0*(particleMass*1e9*cst.e))

    @property
    def gamma(self):
        return 1.0 + self.energy/self.particleMass

    @property
    def beta(self):
        return np.sqrt(1.0 - 1.0/self.gamma**2)
