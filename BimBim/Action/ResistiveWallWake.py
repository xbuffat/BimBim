import numpy as np

class ResistiveWallWake:

    def __init__(self,resistivity,quadWakeRatio=0.0):
        self.resistivity = resistivity
        self.quadWakeRatio = quadWakeRatio

    # distance in ns
    def getWake(self,distance):
        retVal = self.resistivity/np.sqrt(1E-9*distance) #TODO
        return retVal,retVal,self.quadWakeRatio*retVal,self.quadWakeRatio*retVal
