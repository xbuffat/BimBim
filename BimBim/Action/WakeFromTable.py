import numpy as np
from scipy.interpolate import interp1d

class WakeFromTable:

    def __init__(self,wakeFileName,skip_header=1):
        data = np.genfromtxt(wakeFileName,delimiter='\t',skip_header=skip_header)
        self.interp = interp1d(data[:,0],data[:,1:5]*1E15,axis=0, fill_value='extrapolate')

    # distance in ns
    def getWake(self,distance):
        wake = self.interp(distance)
        if len(np.shape(wake)) == 2:
            return wake[:,0],wake[:,1],wake[:,2],wake[:,3]
        else:
            return wake[0],wake[1],wake[2],wake[3]
