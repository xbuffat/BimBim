#ifndef XFIEDLS_BIGUASSIIAN_H
#define XFIEDLS_BIGUASSIIAN_H

#include "constants2.h"
#include "complex_error_function.h"
#include <stdio.h>

double get_transv_field_gauss_ellip_Ex(
    double sigma_x,  double sigma_y,
    double Delta_x,  double Delta_y,
    double const x,
    double const y)
{

  double sigmax = sigma_x;
  double sigmay = sigma_y;

  double abx = fabs(x - Delta_x);
  double aby = fabs(y - Delta_y);

  double S, factBE, Ex, Ey;
  double etaBE_re, etaBE_im, zetaBE_re, zetaBE_im;
  double w_etaBE_re, w_etaBE_im, w_zetaBE_re, w_zetaBE_im;
  double expBE;

  if (sigmax>sigmay){
    S = sqrt(2.*(sigmax*sigmax-sigmay*sigmay));
    factBE = 1./(2.*EPSILON_0*SQRT_PI*S);

    etaBE_re = sigmay/sigmax*abx;
    etaBE_im = sigmax/sigmay*aby;

    zetaBE_re = abx;
    zetaBE_im = aby;

    cerrf(zetaBE_re/S, zetaBE_im/S , &(w_zetaBE_re), &(w_zetaBE_im));
    cerrf(etaBE_re/S, etaBE_im/S , &(w_etaBE_re), &(w_etaBE_im));

    expBE = exp(-abx*abx/(2*sigmax*sigmax)-aby*aby/(2*sigmay*sigmay));

    Ex = factBE*(w_zetaBE_im - w_etaBE_im*expBE);
    Ey = factBE*(w_zetaBE_re - w_etaBE_re*expBE);
  }
  else if (sigmax<sigmay){
    S = sqrt(2.*(sigmay*sigmay-sigmax*sigmax));
    factBE = 1./(2.*EPSILON_0*SQRT_PI*S);

    etaBE_re = sigmax/sigmay*aby;
    etaBE_im = sigmay/sigmax*abx;

    zetaBE_re = aby;
    zetaBE_im = abx;

    cerrf(zetaBE_re/S, zetaBE_im/S , &(w_zetaBE_re), &(w_zetaBE_im));
    cerrf(etaBE_re/S, etaBE_im/S , &(w_etaBE_re), &(w_etaBE_im));

    expBE = exp(-aby*aby/(2*sigmay*sigmay)-abx*abx/(2*sigmax*sigmax));

    Ey = factBE*(w_zetaBE_im - w_etaBE_im*expBE);
    Ex = factBE*(w_zetaBE_re - w_etaBE_re*expBE);
  }
  else{
    printf("Round beam not implemented!\n");
    Ex = Ey = 0.;
  }

  if((x - Delta_x)<0) Ex=-Ex;
  if((y - Delta_y)<0) Ey=-Ey;
  return Ex;
}

double get_transv_field_gauss_ellip_Ey(
        double sigma_x,  double sigma_y,
        double Delta_x,  double Delta_y,
        double const x,
	      double const y)
{
  double sigmax = sigma_x;
  double sigmay = sigma_y;

  double abx = fabs(x - Delta_x);
  double aby = fabs(y - Delta_y);


  double S, factBE, Ex, Ey;
  double etaBE_re, etaBE_im, zetaBE_re, zetaBE_im;
  double w_etaBE_re, w_etaBE_im, w_zetaBE_re, w_zetaBE_im;
  double expBE;

  if (sigmax>sigmay){
    S = sqrt(2.*(sigmax*sigmax-sigmay*sigmay));
    factBE = 1./(2.*EPSILON_0*SQRT_PI*S);

    etaBE_re = sigmay/sigmax*abx;
    etaBE_im = sigmax/sigmay*aby;

    zetaBE_re = abx;
    zetaBE_im = aby;

    cerrf(zetaBE_re/S, zetaBE_im/S , &(w_zetaBE_re), &(w_zetaBE_im));
    cerrf(etaBE_re/S, etaBE_im/S , &(w_etaBE_re), &(w_etaBE_im));

    expBE = exp(-abx*abx/(2*sigmax*sigmax)-aby*aby/(2*sigmay*sigmay));

    Ex = factBE*(w_zetaBE_im - w_etaBE_im*expBE);
    Ey = factBE*(w_zetaBE_re - w_etaBE_re*expBE);
  }
  else if (sigmax<sigmay){
    S = sqrt(2.*(sigmay*sigmay-sigmax*sigmax));
    factBE = 1./(2.*EPSILON_0*SQRT_PI*S);

    etaBE_re = sigmax/sigmay*aby;
    etaBE_im = sigmay/sigmax*abx;

    zetaBE_re = aby;
    zetaBE_im = abx;

    cerrf(zetaBE_re/S, zetaBE_im/S , &(w_zetaBE_re), &(w_zetaBE_im));
    cerrf(etaBE_re/S, etaBE_im/S , &(w_etaBE_re), &(w_etaBE_im));

    expBE = exp(-aby*aby/(2*sigmay*sigmay)-abx*abx/(2*sigmax*sigmax));

    Ey = factBE*(w_zetaBE_im - w_etaBE_im*expBE);
    Ex = factBE*(w_zetaBE_re - w_etaBE_re*expBE);
  }
  else{
    Ex = Ey = 0.;
  }

  if((x - Delta_x)<0) Ex=-Ex;
  if((y - Delta_y)<0) Ey=-Ey;

  return Ey;
 }


double Psi(bool xxxy_plane, double x, double y, double sigma_1, double sigma_2) {
    if (xxxy_plane == true){
        return exp(-x * x / (2 * sigma_1 * sigma_1) - y * y / (2 * sigma_2 * sigma_2)) / (2 * PI * sigma_1 * sigma_2);
    }
    else {
        return exp(-y * y / (2 * sigma_2 * sigma_2) - x * x / (2 * sigma_1 * sigma_1)) / (2 * PI * sigma_2 * sigma_1);
    }
}

double Dx_prime(bool xxxy_plane, double energy, double x, double y, double sigma_1, double sigma_2, double sep1, double sep2) {
    double c = -1 * (QELEM / (energy * 1e9));
    if (xxxy_plane == true) {
        double Ex = get_transv_field_gauss_ellip_Ex(sigma_1, sigma_2, sep1, sep2, x, y);
        return c * Ex;
    }
    else {
        double Ey = get_transv_field_gauss_ellip_Ey(sigma_2, sigma_1, sep2, sep1, y, x);
        return c * Ey;
    }
}


double Dy_prime(bool xxxy_plane, double energy, double x, double y, double sigma_1, double sigma_2, double sep1, double sep2) {
    double c = -1 * (QELEM / (energy * 1e9));
    if (xxxy_plane == true) {
        double Ey = get_transv_field_gauss_ellip_Ey(sigma_1, sigma_2, sep1, sep2, x, y);
//        printf("%0.2f \n", Ey);
        return c * Ey;
    }
    else {
        double Ex = get_transv_field_gauss_ellip_Ex(sigma_2, sigma_1, sep2, sep1, y, x);
        return c * Ex;
    }
}

double product_x(bool xxxy_plane, double energy, double x, double y, double sigma_1, double sigma_2, double sep1, double sep2) {
    return Dx_prime(xxxy_plane, energy, x, y, sigma_1, sigma_2, sep1, sep2) * Psi(xxxy_plane, x, y, sigma_1, sigma_2);
}

double product_y(bool xxxy_plane, double energy, double x, double y, double sigma_1, double sigma_2, double sep1, double sep2) {
    return Dy_prime(xxxy_plane, energy, x, y, sigma_1, sigma_2, sep1, sep2) * Psi(xxxy_plane, x, y, sigma_1, sigma_2);
}

double differentiate(double f1, double f2, double eps){
    return (f2-f1)/(2*eps);
}

double trapz(int n, int m, double x[][m], double y[], int axis) {
    double sum = 0.0;
    if (axis == 0) { // integrate along the rows (axis=0)
        for (int j = 0; j < m; j++) {
            double sum = 0.0;
            for (int i = 1; i < n; i++) {
                sum += (x[i][j] + x[i-1][j]) * (y[i] - y[i-1]) / 2.0;
            }
            return sum;
        }
    }
    else if (axis == 1) { // integrate along the columns (axis=1)
        for (int i = 0; i < n; i++) {
            double sum = 0.0;
            for (int j = 1; j < m; j++) {
                sum += (x[i][j] + x[i][j-1]) * (y[j] - y[j-1])/ 2.0;
            }
            return sum;
        }
    }
}

double get_force_ellip_xx(
    double nbpoint, double sigma_1,  double sigma_2,
    double emittance1, double emittance2,
    double gamma, double sCP, double intensity,
    double sCPbeta1, double sCPbeta2,
    double sep1, double sep2,
    double energy, bool xxxy_plane)
{
    int const N = nbpoint;
    double max_1 = 5 * sigma_1;
    double min_1 = -5 * sigma_1;
    double max_2 = 5 * sigma_2;
    double min_2 = -5 * sigma_2;
    double eps_1[2] = {-1 * sigma_1 / 1000, sigma_1 / 1000};
    double eps_2[2] = {-1 * sigma_2 / 1000, sigma_2 / 1000};
    double res_x_y[4][2];
    double X_tab[N];
    double Y_tab[N];
    double tab[N][N];
    double intx[N];

    for (int i = 0; i < N; i++) {
        X_tab[i] = min_1 + (i * (max_1 - min_1) / (N - 1));
        Y_tab[i] = min_2 + (i * (max_2 - min_2) / (N - 1));
    }
    for (int k = 0; k < 2; k++) {
//        #pragma omp parallel for default(firstprivate) shared(tab)
        for (int j = 0; j < N; j++) {
            for (int i = 0; i < N; i++) {
                tab[i][j] = product_x(xxxy_plane, energy, X_tab[i], Y_tab[j], sigma_1, sigma_2, sep1 + eps_1[k], sep2);
            }
        }
//        #pragma omp parallel for default(firstprivate) shared(intx, tab)
        for (int i = 0; i < N; i++) {
            intx[i] = trapz(1, N, tab[i], Y_tab, 1);
        }
        res_x_y[0][k] = trapz(N, 1, intx, X_tab, 0);

        }
    double kbbxx = differentiate(res_x_y[0][0],res_x_y[0][1],eps_1[1]) * intensity;
    return kbbxx;
}

double get_force_ellip_yy(
    double nbpoint, double sigma_1,  double sigma_2,
    double emittance1, double emittance2,
    double gamma, double sCP, double intensity,
    double sCPbeta1, double sCPbeta2,
    double sep1, double sep2,
    double energy, bool xxxy_plane)
{
    int const N = nbpoint;
    double max_1 = 5 * sigma_1;
    double min_1 = -5 * sigma_1;
    double max_2 = 5 * sigma_2;
    double min_2 = -5 * sigma_2;
    double eps_1[2] = {-1 * sigma_1 / 1000, sigma_1 / 1000};
    double eps_2[2] = {-1 * sigma_2 / 1000, sigma_2 / 1000};
    double res_x_y[4][2];
    double X_tab[N];
    double Y_tab[N];
    double tab[N][N];
    double intx[N];

    for (int i = 0; i < N; i++) {
        X_tab[i] = min_1 + (i * (max_1 - min_1) / (N - 1));
        Y_tab[i] = min_2 + (i * (max_2 - min_2) / (N - 1));
    }
    for (int k = 0; k < 2; k++) {
//        #pragma omp parallel for default(firstprivate) shared(tab)
        for (int j = 0; j < N; j++) {
            for (int i = 0; i < N; i++) {
                tab[i][j] = product_y(xxxy_plane, energy, X_tab[i], Y_tab[j], sigma_1, sigma_2, sep1, sep2 + eps_2[k]);
            }
        }
//        #pragma omp parallel for default(firstprivate) shared(intx, tab)
        for (int i = 0; i < N; i++) {
            intx[i] = trapz(1, N, tab[i], Y_tab, 1);
        }
        res_x_y[1][k] = trapz(N, 1, intx, X_tab, 0);
        }
    double kbbyy = differentiate(res_x_y[1][0],res_x_y[1][1],eps_2[1]) * intensity;
    return kbbyy;
}

double get_force_ellip_xy(
    double nbpoint, double sigma_1,  double sigma_2,
    double emittance1, double emittance2,
    double gamma, double sCP, double intensity,
    double sCPbeta1, double sCPbeta2,
    double sep1, double sep2,
    double energy, bool xxxy_plane)
{
    int const N = nbpoint;
    double max_1 = 5 * sigma_1;
    double min_1 = -5 * sigma_1;
    double max_2 = 5 * sigma_2;
    double min_2 = -5 * sigma_2;
    double eps_2[2] = {-1 * sigma_2 / 1000, sigma_2 / 1000};
    double res_x_y[4][2];
    double X_tab[N];
    double Y_tab[N];
    double tab[N][N];
    double intx[N];

    for (int i = 0; i < N; i++) {
        X_tab[i] = min_1 + (i * (max_1 - min_1) / (N - 1));
        Y_tab[i] = min_2 + (i * (max_2 - min_2) / (N - 1));
    }
    for (int k = 0; k < 2; k++) {
//            #pragma omp parallel for default(firstprivate) shared(tab)
        for (int j = 0; j < N; j++) {
            for (int i = 0; i < N; i++) {
                tab[i][j] = product_x(xxxy_plane, energy, X_tab[i], Y_tab[j], sigma_1, sigma_2, sep1, sep2 + eps_2[k]);
            }
        }
//            #pragma omp parallel for default(firstprivate) shared(intx, tab)
        for (int i = 0; i < N; i++) {
            intx[i] = trapz(1, N, tab[i], Y_tab, 1);
        }
        res_x_y[2][k] = trapz(N, 1, intx, X_tab, 0);
    }
    double kbbxy = differentiate(res_x_y[2][0],res_x_y[2][1],eps_2[1]) * intensity;
    return kbbxy;
}

double get_force_ellip_yx(
    double nbpoint, double sigma_1,  double sigma_2,
    double emittance1, double emittance2,
    double gamma, double sCP, double intensity,
    double sCPbeta1, double sCPbeta2,
    double sep1, double sep2,
    double energy, bool xxxy_plane)
{
    int const N = nbpoint;
    double max_1 = 5 * sigma_1;
    double min_1 = -5 * sigma_1;
    double max_2 = 5 * sigma_2;
    double min_2 = -5 * sigma_2;
    double eps_1[2] = {-1 * sigma_1 / 1000, sigma_1 / 1000};
    double res_x_y[4][2]; // since separated no need to use 2d tab
    double X_tab[N];
    double Y_tab[N];
    double tab[N][N];
    double intx[N];

    for (int i = 0; i < N; i++) {
        X_tab[i] = min_1 + (i * (max_1 - min_1) / (N - 1));
        Y_tab[i] = min_2 + (i * (max_2 - min_2) / (N - 1));
    }
    for (int k = 0; k < 2; k++) {
//            #pragma omp parallel for default(firstprivate) shared(tab)
        for (int j = 0; j < N; j++) {
            for (int i = 0; i < N; i++) {
                tab[i][j] = product_y(xxxy_plane, energy, X_tab[i], Y_tab[j], sigma_1, sigma_2, sep1 + eps_1[k], sep2);
            }
        }
//        #pragma omp parallel for default(none) shared(intx, tab) firstprivate(N, Y_tab, X_tab)
        for (int i = 0; i < N; i++) {
            intx[i] = trapz(1, N, tab[i], Y_tab, 1);
        }
        res_x_y[3][k] = trapz(N, 1, intx, X_tab, 0);
    }
    double kbbyx = differentiate(res_x_y[3][0],res_x_y[3][1],eps_1[1]) * intensity;
    return kbbyx;
}
#endif
