#ifndef XFIEDLS_BIGUASSIIAN_H
#define XFIEDLS_BIGUASSIIAN_H

#include "constants.h"
#include "complex_error_function.h"
#include <stdio.h>
#include <math.h>
#include <complex.h>

void faddeeva(
    double in_re, double in_im,
    double* /*restrict*/ out_re,
    double* /*restrict*/ out_im){

    cerrf(in_re, in_im , out_re, out_im);
}

void get_transv_field_gauss_ellip_derivative(
    double sigma_x,  double sigma_y,
    double Delta_x,  double Delta_y,
    double const x,
    double const y,
    double* /*restrict*/ dfxx,
    double* /*restrict*/ dfyy,
    double* /*restrict*/ dfxy,
    double* /*restrict*/ dfyx)
{

  double sigmax = sigma_x;
  double sigmay = sigma_y;

  double abx = fabs(x - Delta_x);
  double aby = fabs(y - Delta_y);

  double complex S, factBE;
  double etaBE_re, etaBE_im, zetaBE_re, zetaBE_im;
  double w_etaBE_re, w_etaBE_im, w_zetaBE_re, w_zetaBE_im;
  double complex expBE, expBE2, expBE3;
  double complex absum, etasum;
  double complex Ax, Bx, Cx, Dx;
  double complex Ay, By, Cy, Dy;

  if (sigmax>sigmay){
    S = 2. * sqrt((sigmax*sigmax-sigmay*sigmay));
    factBE = 1./(2.*EPSILON_0*SQRT_PI*S);

    etaBE_re = sigmay/sigmax*abx;
    etaBE_im = sigmax/sigmay*aby;

    zetaBE_re = abx;
    zetaBE_im = aby;

    cerrf(zetaBE_re/S, zetaBE_im/S , &(w_zetaBE_re), &(w_zetaBE_im));
    cerrf(etaBE_re/S, etaBE_im/S , &(w_etaBE_re), &(w_etaBE_im));

    expBE = cexp(-abx*abx/(4*sigmax*sigmax)-aby*aby/(4*sigmay*sigmay));
    absum = abx + I * aby;
    expBE2 = cexp(-absum*absum/pow(S,2));
    etasum = etaBE_re + I*etaBE_im;
    expBE3 = cexp(-etasum*etasum/pow(S,2));

    Ax = 2/pow(S,2) * absum * expBE2;
    Bx = 2/pow(S,2) * absum * (w_zetaBE_re+I*w_zetaBE_im - expBE2) - 2*I/(S*SQRT_PI);
    Cx = (abx/(2*pow(sigmax,2)) + etasum * 2 * sigmay/sigmax / pow(S,2)) * expBE * expBE3;
    Dx = expBE * ((abx/(2*pow(sigmax,2)) + etasum * 2 * sigmay/sigmax / pow(S,2)) * (w_etaBE_re+I*w_etaBE_im - expBE3) - 2*I/(S*SQRT_PI) * sigmay/sigmax);

    double complex dfx = factBE * (Ax + Bx - Cx - Dx);

    Ay = 2*I/pow(S,2) * absum * expBE2;
    By = 2*I/pow(S,2) * absum * (w_zetaBE_re+I*w_zetaBE_im - expBE2) + 2/(S*SQRT_PI);
    Cy = (aby/(2*pow(sigmay,2)) + etasum * 2*I * sigmax/sigmay / pow(S,2)) * expBE * expBE3;
    Dy = expBE * ((aby/(2*pow(sigmay,2)) + etasum * 2*I * sigmax/sigmay / pow(S,2)) * (w_etaBE_re+I*w_etaBE_im - expBE3) + 2/(S*SQRT_PI) * sigmax/sigmay);

    double complex dfy = factBE * (Ay + By - Cy - Dy);

    *dfxx = cimag(dfx);
    *dfyy = creal(dfy);

    if((x - Delta_x)<0){
        *dfyx = -creal(dfx);
    }else{
        *dfyx = creal(dfx);
    }
    if((y - Delta_y)<0){
        *dfxy = -cimag(dfy);
    }else{
        *dfxy = cimag(dfy);
    }
  }

  else if (sigmax<sigmay){

    S = 2.*sqrt((sigmay*sigmay-sigmax*sigmax));
    factBE = 1./(2.*EPSILON_0*SQRT_PI*S);

    etaBE_re = sigmax/sigmay*aby;
    etaBE_im = sigmay/sigmax*abx;

    zetaBE_re = aby;
    zetaBE_im = abx;

    cerrf(zetaBE_re/S, zetaBE_im/S , &(w_zetaBE_re), &(w_zetaBE_im));
    cerrf(etaBE_re/S, etaBE_im/S , &(w_etaBE_re), &(w_etaBE_im));

    expBE = exp(-aby*aby/(4*sigmay*sigmay)-abx*abx/(4*sigmax*sigmax));
    absum = aby + I * abx;
    expBE2 = exp(-absum*absum/pow(S,2));
    etasum = etaBE_re + I*etaBE_im;
    expBE3 = exp(-etasum*etasum/pow(S,2));

    Ax = 2/pow(S,2) * absum * expBE2;
    Bx = 2/pow(S,2) * absum * (w_zetaBE_re+I*w_zetaBE_im - expBE2) - 2*I/(S*SQRT_PI);
    Cx = (aby/(2*pow(sigmay,2)) + etasum * 2 * sigmax/sigmay / pow(S,2)) * expBE * expBE3;
    Dx = expBE * ((aby/(2*pow(sigmay,2)) + etasum * 2 * sigmax/sigmay / pow(S,2)) * (w_etaBE_re+I*w_etaBE_im - expBE3) - 2*I/(S*SQRT_PI) * sigmax/sigmay);

    double complex dfy = factBE * (Ax + Bx - Cx - Dx);

    Ay = 2*I/pow(S,2) * absum * expBE2;
    By = 2*I/pow(S,2) * absum * (w_zetaBE_re+I*w_zetaBE_im - expBE2) + 2/(S*SQRT_PI);
    Cy = (abx/(2*pow(sigmax,2)) + etasum * 2*I * sigmay/sigmax / pow(S,2)) * expBE * expBE3;
    Dy = expBE * ((abx/(2*pow(sigmax,2)) + etasum * 2*I * sigmay/sigmax / pow(S,2)) * (w_etaBE_re+I*w_etaBE_im - expBE3) + 2/(S*SQRT_PI) * sigmay/sigmax);

    double complex dfx = factBE * (Ay + By - Cy - Dy);

    *dfxx = creal(dfx);
    *dfyy = cimag(dfy);

    if((x - Delta_x)<0){
        *dfyx = cimag(dfx);
    }else{
        *dfyx = -cimag(dfx);
    }
    if((y - Delta_y)<0){
        *dfxy = creal(dfy);
    }else{
        *dfxy = -creal(dfy);
    }
  }
  else{
    printf("Round beam not implemented!\n");
    *dfxx = 0.;
    *dfyx = 0.;
    *dfyy = 0.;
    *dfxy = 0.;
  }
}

void get_force_ellip(
    double sigma_1,  double sigma_2,
    double intensity,
    double sep1, double sep2,
    double energy,
    double* /*restrict*/ kbbxx,
    double* /*restrict*/ kbbyy,
    double* /*restrict*/ kbbxy,
    double* /*restrict*/ kbbyx)
{
    double dfxx, dfyx, dfyy, dfxy;
    double c = -1 * (QELEM / (energy * 1e9));

    get_transv_field_gauss_ellip_derivative(sigma_1, sigma_2, sep1, sep2, 0, 0, &dfxx, &dfyy, &dfxy, &dfyx);

    *kbbxx = c * dfxx * intensity;
    *kbbyy = c * dfyy * intensity;
    *kbbxy = c * dfxy * intensity;
    *kbbyx = c * dfyx * intensity;
}
// TESTED BOTH CASES WITH SIGX < SIGY AND SIGX > SIGY, FORCES kbbxx, kbbyy, kbbxy and kbbyx
//(Last two with offsets on x/y from 0 to 1 sigmax/y).
#endif
