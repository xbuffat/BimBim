from BimBim.Action import Action
import numpy as np
from time import time
from BimBim.Matrix import printMatrix, insert
from BimBim.Error import BimBimError

from scipy.integrate import quad
from scipy.special import ellipk
import scipy.constants as cst

class Transport(Action.Action):
    # phases, betas, chromas and fRF can be lists to specify the two beams
    def __init__(self, phase_x=0, phase_y=0, beta_x=0, beta_y=0, phase_z=0, chroma_x=0, chroma_y=0, transition=-1,
                  secondOrderchroma_x=0.0, secondOrderchroma_y=0.0, fRF=0.0, keepInMemory = True):
        self._syncBetMaps = {}
        phase_x_tmp = np.atleast_1d(phase_x)
        self.phase_x_B1 = phase_x_tmp[0]
        if len(phase_x_tmp) > 1:
            self.phase_x_B2 = phase_x_tmp[1]
        else:
            self.phase_x_B2 = self.phase_x_B1
        phase_y_tmp = np.atleast_1d(phase_y)
        self.phase_y_B1 = phase_y_tmp[0]
        if len(phase_y_tmp) > 1:
            self.phase_y_B2 = phase_y_tmp[1]
        else:
            self.phase_y_B2 = self.phase_y_B1
        phase_z_tmp = np.atleast_1d(phase_z)
        self.phase_z_B1 = phase_z_tmp[0]
        if len(phase_z_tmp) > 1:
            self.phase_z_B2 = phase_z_tmp[1]
        else:
            self.phase_z_B2 = self.phase_z_B1
        beta_x_tmp = np.atleast_1d(beta_x)
        self.beta_x_B1 = beta_x_tmp[0]
        if len(beta_x_tmp) > 1:
            self.beta_x_B2 = beta_x_tmp[1]
        else:
            self.beta_x_B2 = self.beta_x_B1
        beta_y_tmp = np.atleast_1d(beta_y)
        self.beta_y_B1 = beta_y_tmp[0]
        if len(beta_y_tmp) > 1:
            self.beta_y_B2 = beta_y_tmp[1]
        else:
            self.beta_y_B2 = self.beta_y_B1
        chroma_x_tmp = np.atleast_1d(chroma_x)
        self.chroma_x_B1 = chroma_x_tmp[0]
        if len(chroma_x_tmp) > 1:
            self.chroma_x_B2 = chroma_x_tmp[1]
        else:
            self.chroma_x_B2 = self.chroma_x_B1
        chroma_y_tmp = np.atleast_1d(chroma_y)
        self.chroma_y_B1 = chroma_y_tmp[0]
        if len(chroma_y_tmp) > 1:
            self.chroma_y_B2 = chroma_y_tmp[1]
        else:
            self.chroma_y_B2 = self.chroma_y_B1
        secondOrderchroma_x_tmp = np.atleast_1d(secondOrderchroma_x)
        self.secondOrderchroma_x_B1 = secondOrderchroma_x_tmp[0]
        if len(secondOrderchroma_x_tmp) > 1:
            self.secondOrderchroma_x_B2 = secondOrderchroma_x_tmp[1]
        else:
            self.secondOrderchroma_x_B2 = self.secondOrderchroma_x_B1
        secondOrderchroma_y_tmp = np.atleast_1d(secondOrderchroma_y)
        self.secondOrderchroma_y_B1 = secondOrderchroma_y_tmp[0]
        if len(secondOrderchroma_y_tmp) > 1:
            self.secondOrderchroma_y_B2 = secondOrderchroma_y_tmp[1]
        else:
            self.secondOrderchroma_y_B2 = self.secondOrderchroma_y_B1
        fRF_tmp = np.atleast_1d(fRF)
        self.fRF_B1 = fRF_tmp[0]
        if len(fRF_tmp) > 1:
            self.fRF_B2 = fRF_tmp[1]
        else:
            self.fRF_B2 = self.fRF_B1

        self.transition = transition
        self.keepInMemory = keepInMemory



    @property
    def phase_x_B1(self):
        return self._phase_x_B1

    @phase_x_B1.setter
    def phase_x_B1(self, phase):
        self._phase_x_B1 = phase
        self._syncBetMaps.clear()

    @property
    def phase_y_B1(self):
        return self._phase_y_B1

    @phase_y_B1.setter
    def phase_y_B1(self, phase):
        self._phase_y_B1 = phase
        self._syncBetMaps.clear()

    @property
    def phase_z_B1(self):
        return self._phase_z_B1

    @phase_z_B1.setter
    def phase_z_B1(self, phase):
        self._phase_z_B1 = phase
        self._syncBetMaps.clear()

    @property
    def beta_x_B1(self):
        return self._beta_x_B1

    @beta_x_B1.setter
    def beta_x_B1(self, beta):
        self._beta_x_B1 = beta
        self._syncBetMaps.clear()

    @property
    def beta_y_B1(self):
        return self._beta_y_B1

    @beta_y_B1.setter
    def beta_y_B1(self, beta):
        self._beta_y_B1 = beta
        self._syncBetMaps.clear()

    @property
    def chroma_x_B1(self):
        return self._chroma_x_B1

    @chroma_x_B1.setter
    def chroma_x_B1(self, chroma):
        self._chroma_x_B1 = chroma
        self._syncBetMaps.clear()

    @property
    def chroma_y_B1(self):
        return self._chroma_y_B1

    @chroma_y_B1.setter
    def chroma_y_B1(self, chroma):
        self._chroma_y_B1 = chroma
        self._syncBetMaps.clear()

    @property
    def secondOrderchroma_x_B1(self):
        return self._secondOrderchroma_x_B1

    @secondOrderchroma_x_B1.setter
    def secondOrderchroma_x_B1(self, secondOrderchroma):
        self._secondOrderchroma_x_B1 = secondOrderchroma
        self._syncBetMaps.clear()

    @property
    def secondOrderchroma_y_B1(self):
        return self._secondOrderchroma_y_B1

    @secondOrderchroma_y_B1.setter
    def secondOrderchroma_y_B1(self, secondOrderchroma):
        self._secondOrderchroma_y_B1 = secondOrderchroma
        self._syncBetMaps.clear()

    @property
    def phase_x_B2(self):
        return self._phase_x_B2

    @phase_x_B2.setter
    def phase_x_B2(self, phase):
        self._phase_x_B2 = phase
        self._syncBetMaps.clear()

    @property
    def phase_y_B2(self):
        return self._phase_y_B2

    @phase_y_B2.setter
    def phase_y_B2(self, phase):
        self._phase_y_B2 = phase
        self._syncBetMaps.clear()

    @property
    def phase_z_B2(self):
        return self._phase_z_B2

    @phase_z_B2.setter
    def phase_z_B2(self, phase):
        self._phase_z_B2 = phase
        self._syncBetMaps.clear()

    @property
    def beta_x_B2(self):
        return self._beta_x_B2

    @beta_x_B2.setter
    def beta_x_B2(self, beta):
        self._beta_x_B2 = beta
        self._syncBetMaps.clear()

    @property
    def beta_y_B2(self):
        return self._beta_y_B2

    @beta_y_B2.setter
    def beta_y_B2(self, beta):
        self._beta_y_B2 = beta
        self._syncBetMaps.clear()

    @property
    def chroma_x_B2(self):
        return self._chroma_x_B2

    @chroma_x_B2.setter
    def chroma_x_B2(self, chroma):
        self._chroma_x_B2 = chroma
        self._syncBetMaps.clear()

    @property
    def chroma_y_B2(self):
        return self._chroma_y_B2

    @chroma_y_B2.setter
    def chroma_y_B2(self, chroma):
        self._chroma_y_B2 = chroma
        self._syncBetMaps.clear()

    @property
    def secondOrderchroma_x_B2(self):
        return self._secondOrderchroma_x_B2

    @secondOrderchroma_x_B2.setter
    def secondOrderchroma_x_B2(self, secondOrderchroma):
        self._secondOrderchroma_x_B2 = secondOrderchroma
        self._syncBetMaps.clear()

    @property
    def secondOrderchroma_y_B2(self):
        return self._secondOrderchroma_y_B2

    @secondOrderchroma_y_B2.setter
    def secondOrderchroma_y_B2(self, secondOrderchroma):
        self._secondOrderchroma_y_B2 = secondOrderchroma
        self._syncBetMaps.clear()

    @property
    def fRF_B1(self):
        return self._fRF_B1

    @fRF_B1.setter
    def fRF_B1(self, fRF):
        self._fRF_B1 = fRF
        self._syncBetMaps.clear()

    @property
    def fRF_B2(self):
        return self._fRF_B2

    @fRF_B2.setter
    def fRF_B2(self, fRF):
        self._fRF_B2 = fRF
        self._syncBetMaps.clear()

    def getQs(self, qs0, fRF, phaseAmplitude=0):
        qs = qs0
        if fRF != 0.0:
            qs *= np.pi / (2 * ellipk(np.sin(phaseAmplitude / 2) ** 2))
        return qs

    def C_ij(self, i, j, nSlice, qs):
        phi = np.pi * (qs - 1.0 + (float(j) - float(i)) / float(nSlice))
        return np.sin(float(nSlice) * phi) / (float(nSlice) * np.sin(phi))

    def _getCirc(self, nSlice, qs):
        fullMat = np.array([[1.0 if j == i + 1 or (i == nSlice - 1 and j == 0) else 0.0 for j in range(nSlice)] for i in
                            range(nSlice)])
        val, vect = np.linalg.eig(fullMat)
        # for i in range(len(val)):
        #    for j in range(i+1,len(val)):
        #        print(i,j,np.abs(np.dot(vect[:,i],vect[:,j])))
        # print(np.linalg.det(vect))
        retVal = np.dot(vect, np.diag(np.power((val + 0j), (qs * nSlice))))
        # print(np.linalg.det(retVal))
        retVal = np.dot(retVal, np.linalg.inv(vect))
        # det = np.linalg.det(retVal)
        # retVal /= det
        return retVal

    # Equivalent, but does not requires diagonalisation or inversion, it uses the fact that the Fourier matrix diagonalises any circulant matrix
    def getCirc(self, nSlice, qs):
        fourierMat = np.zeros((nSlice, nSlice), dtype=complex)
        for i in range(nSlice):
            for j in range(nSlice):
                fourierMat[i, j] = np.exp(2.0j * np.pi * j * i / nSlice) / np.sqrt(nSlice)
        fourierMatH = np.conjugate(np.transpose(fourierMat))
        ### get eigenvalues from permutation matrix###
        # forwardCyclicMat = np.array([[1.0 if j==i+1 or (i == nSlice-1 and j == 0) else 0.0 for j in range(nSlice)]for i in range(nSlice)])
        # diagMat = np.dot(fourierMatH,np.dot(forwardCyclicMat,fourierMat))
        # diag = np.copy(np.diagonal(diagMat))
        # diag = diag**(qs*nSlice)
        ##############################################
        diag = np.exp(2j * np.pi * np.arange(0.0, 1.0, 1.0 / (nSlice)))
        diag = diag ** (qs * nSlice)
        # diag = np.exp(2j*np.pi*np.arange(0.0,1.0,1.0/(nSlice))*qs*nSlice) #WTF ?
        diagMat = np.diag(diag)
        circulantMat = np.dot(fourierMat, np.dot(diagMat, fourierMatH))
        return circulantMat

    # Define circulant matrix
    def getCircMap(self, basis, beamNumber, bunch):
        if beamNumber == 1:
            qs = self.phase_z_B1
            fRF = self.fRF_B1
        elif beamNumber == 2:
            qs = self.phase_z_B2
            fRF = self.fRF_B2
        if fRF == 0.0:
            retVal = self.getCirc(basis.nSlice, self.getQs(qs,fRF))
            retVal = np.kron(np.identity(basis.nRing), retVal)
        else:
            retVal = np.zeros((basis.nSlice * basis.nRing, basis.nSlice * basis.nRing),
                              dtype=complex)
            for i in range(basis.nRing):
                qs = self.getQs(qs,fRF, basis.getRingRadius(i) * bunch.sigma_z * 2.0 * np.pi * fRF / cst.c)
                circMat = self.getCirc(basis.nSlice, qs)
                retVal[i * basis.nSlice:(i + 1) * basis.nSlice,
                i * basis.nSlice:(i + 1) * basis.nSlice] = circMat
        return retVal

    def getBetMap(self, basis, beamNumber, phase_x, phase_y):
        if basis.nDim == 2:
            if beamNumber == 1 and basis.plane[0] == 'x':
                phase = phase_x
                beta = self.beta_x_B1
            elif beamNumber == 1 and basis.plane[0] == 'y':
                phase = phase_y
                beta = self.beta_y_B1
            elif beamNumber == 2 and basis.plane[0] == 'x':
                phase = phase_x
                beta = self.beta_x_B2
            elif beamNumber == 2 and basis.plane[0] == 'y':
                phase = phase_y
                beta = self.beta_y_B2
            else:
                raise BimBimError(f'Transport: {beamNumber} / {basis.plane[0]} are not valid beam number / plane')
            cos1 = np.cos(2.0 * np.pi * phase)
            sin1 = np.sin(2.0 * np.pi * phase)
            retVal = np.array(
                [[cos1, beta * sin1], [-1.0 / beta * sin1, cos1]])
        elif basis.nDim == 4:
            if beamNumber == 1:
                beta_x = self.beta_x_B1
                beta_y = self.beta_y_B1
            elif beamNumber == 2:
                beta_x = self.beta_x_B2
                beta_y = self.beta_y_B2
            else:
                raise BimBimError(f'Transport: {beamNumber} is not a valid beam number')
            cos1 = np.cos(2.0 * np.pi * phase_x)
            sin1 = np.sin(2.0 * np.pi * phase_x)
            cos2 = np.cos(2.0 * np.pi * phase_y)
            sin2 = np.sin(2.0 * np.pi * phase_y)
            retVal = np.zeros((4, 4))
            retVal[0][0] = cos1
            retVal[0][1] = beta_x * sin1
            retVal[1][1] = cos1
            retVal[1][0] = -1.0 / beta_x * sin1
            retVal[2][2] = cos2
            retVal[2][3] = beta_y * sin2
            retVal[3][3] = cos2
            retVal[3][2] = -1.0 / beta_y * sin2
        else:
            raise BimBimError('Transport is not implemented in ' + str(self.nDim) + ' dimensions')
        return retVal

    # Build incoming chromaticity phase shift matrix
    def getChromaMap(self, basis, beamNumber, sigma_dpp, direction=1.0):
        if beamNumber == 1:
            chroma_x = self.chroma_x_B1
            secondOrderchroma_x = self.secondOrderchroma_x_B1
            chroma_y = self.chroma_y_B1
            secondOrderchroma_y = self.secondOrderchroma_y_B1
        elif beamNumber == 2:
            chroma_x = self.chroma_x_B2
            secondOrderchroma_x = self.secondOrderchroma_x_B2
            chroma_y = self.chroma_y_B2
            secondOrderchroma_y = self.secondOrderchroma_y_B2
        else:
            raise BimBimError(f'Transport: {beamNumber} is not a valid beam number')

        chrmat = np.zeros((basis.bunchBlockSize, basis.bunchBlockSize))
        for iSlice in range(basis.nSlice):
            for iRing in range(basis.nRing):
                dpp = basis.getDPoverP(iSlice, iRing, sigma_dpp, self.transition)
                phase1 = direction * (chroma_x * dpp + 0.5 * secondOrderchroma_x * dpp ** 2)
                phase2 = direction * (chroma_y * dpp + 0.5 * secondOrderchroma_y * dpp ** 2)
                betMap = self.getBetMap(basis, beamNumber, phase1, phase2)
                index = basis.nDim * (iSlice + iRing * basis.nSlice)
                insert(chrmat, betMap, index)
        # myFile = open('debug_chroma.mat','w')
        # printMatrix(myFile,chrmat)
        # myFile.close()
        return chrmat

    def getSyncBetMap(self, basis, beamNumber, bunch):
        if beamNumber == 1:
            phase_x = self.phase_x_B1
            phase_y = self.phase_y_B1
        elif beamNumber == 2:
            phase_x = self.phase_x_B2
            phase_y = self.phase_y_B2
        else:
            raise BimBimError(f'Transport: {beamNumber} is not a valid beam number')
        key = (basis, beamNumber, bunch.sigma_dpp)
        if key in self._syncBetMaps.keys():
            return self._syncBetMaps[key]
        else:
            betMap = self.getBetMap(basis, beamNumber, phase_x, phase_y)
            circMap = self.getCircMap(basis, beamNumber, bunch)

            chromaIn = self.getChromaMap(basis, beamNumber, bunch.sigma_dpp, direction=1.0)
            retVal = np.kron(circMap, betMap)
            retVal = retVal.dot(chromaIn)

            if self.keepInMemory:
                self._syncBetMaps[key] = retVal
            return retVal

    def getMatrix(self, beams, pos, basis):
        bunchB1 = beams.bunchConfigB1[pos]
        bunchB2 = beams.bunchConfigB2[pos]
        if bunchB1 != None and bunchB2 == None:
            # time0 = time()
            beamNumber = 1
            syncBet = self.getSyncBetMap(basis, beamNumber, bunchB1)
            retVal = basis.getBunchProjection(beamNumber, bunchB1.number, syncBet)
            # myFile = open('debug_transportB1.mat','w')
            # printMatrix(myFile,retVal)
            # myFile.close()
            # time1 = time()
            # print 'Time to build transport matrix for B1b'+str(bunchB1.number),time1-time0
            return retVal.tocsr()
        elif bunchB1 == None and bunchB2 != None:
            # time0 = time()
            beamNumber = 2
            syncBet = self.getSyncBetMap(basis, beamNumber, bunchB2)
            retVal = basis.getBunchProjection(beamNumber, bunchB2.number, syncBet)
            # myFile = open('debug_transportB2.mat','w')
            # printMatrix(myFile,retVal)
            # myFile.close()
            # time1 = time()
            # print 'Time to build transport matrix for B2b'+str(bunchB2.number),time1-time0
            return retVal.tocsr()
        else:
            # time0 = time()
            beamNumber = 1
            syncBet = self.getSyncBetMap(basis, beamNumber, bunchB1)
            projB1 = basis.getBunchProjection(beamNumber, bunchB1.number, syncBet)
            beamNumber = 2
            syncBet = self.getSyncBetMap(basis, beamNumber, bunchB2)
            projB2 = basis.getBunchProjection(beamNumber, bunchB2.number, syncBet)
            retVal = projB1.tocsr().dot(projB2.tocsr())
            # myFile = open('debug_transport.mat','w')
            # printMatrix(myFile,retVal)
            # myFile.close()
            # time1 = time()
            # print 'Time to build transport matrix for B1b'+str(bunchB1.number)+' and B2b'+str(bunchB2.number),time1-time0
            return retVal
