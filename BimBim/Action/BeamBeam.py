import numpy as np
from BimBim.Action import Action
from BimBim.Error import BimBimError
from BimBim.Matrix import dotProd
import os, ctypes
from time import time
import scipy.constants as cst
import scipy.sparse as spm


# Note : The virtual drift is done neglecting the Xing angle and the linearized coherent kick takes into account
#        the beam position and size at the location of the interaction (including hour glass and Xing angle)

class BeamBeam(Action.Action):
    # betaStars can be lists to specify the two beams. By default the beta's are symmetric.
    # Sep and xing can be lists to specify the two planes. By default sep and Xing are 0 in the other plane.
    def __init__(self, betaStar_x=None, betaStar_y = None, sepStar=0.0, Xing=0.0, hourglass=True,
                  phaseAdvance = False, XYCoupling=True, virtualDrift = True, n_integration_points=20):
        if betaStar_x is None and betaStar_y is None:
            raise BimBimError('BeamBeam: Need to specify at least one betastar')
        elif betaStar_y is None:
            betaStar_y = betaStar_x
        elif betaStar_x is None:
            betaStar_x = betaStar_y

        betaStar_x_tmp = np.atleast_1d(betaStar_x)
        self.betaStar_x_B1 = betaStar_x_tmp[0]
        if len(betaStar_x_tmp) > 1:
            self.betaStar_x_B2 = betaStar_x_tmp[1]
        else:
            self.betaStar_x_B2 = self.betaStar_x_B1
        betaStar_y_tmp = np.atleast_1d(betaStar_y)
        self.betaStar_y_B1 = betaStar_y_tmp[0]
        if len(betaStar_y_tmp) > 1:
            self.betaStar_y_B2 = betaStar_y_tmp[1]
        else:
            self.betaStar_y_B2 = self.betaStar_y_B1

        sepStar_tmp = np.atleast_1d(sepStar)
        self.sepStar_x = sepStar_tmp[0]
        if len(sepStar_tmp) > 1:
            self.sepStar_y = sepStar_tmp[1]
        else:
            self.sepStar_y = 0.0
        xing_tmp = np.atleast_1d(Xing)
        self.xing_x = xing_tmp[0]
        if len(xing_tmp) > 1:
            self.xing_y = xing_tmp[1]
        else:
            self.xing_y = 0.0
        
        self.phaseAdvance = phaseAdvance
        self.hourglass = hourglass
        self.XYCoupling = XYCoupling
        self.virtualDrift = virtualDrift

        self._bb_lib = None
        self.n_integration_points = n_integration_points

    def generateCollisionSchedule(self,bunchB1,bunchB2,basis):
        size = (basis.nSlice*basis.nRing)**2
        scheds = np.zeros((size,4),dtype=int)
        taus = np.zeros(size,dtype=float)
        i = 0
        for iSliceB1 in range(basis.nSlice):
            for iRingB1 in range(basis.nRing):
                for iSliceB2 in range(basis.nSlice):
                    for iRingB2 in range(basis.nRing):
                        taus[i] = basis.getSPosition(iSliceB2,iRingB2,bunchB2.sigma_z)+basis.getSPosition(iSliceB1,iRingB1,bunchB1.sigma_z)
                        scheds[i,0] = iSliceB1
                        scheds[i,1] = iRingB1
                        scheds[i,2] = iSliceB2
                        scheds[i,3] = iRingB2
                        i += 1
        sortedIndices = np.argsort(taus)
        return scheds[sortedIndices,:]

    def _init(self, basis):
        if basis.nDim == 2:
            self.drift = np.identity(4)
            self.invDrift = np.identity(4)
            self.beamBeamMatrix = np.identity(4)
            self._4D = False
        elif basis.nDim == 4:
            self.drift = np.identity(8)
            self.invDrift = np.identity(8)
            self.beamBeamMatrix = np.identity(8)
            self._4D = True

    def _load_bb_lib(self):
        bblib_dir = os.path.join(os.path.dirname(__file__),"bigaussian_src")
        bblib_path = os.path.join(bblib_dir,'bblib.so')
        if not os.path.exists(bblib_path):
            makefile_path = os.path.join(bblib_dir,'Makefile')
            if not os.path.exists(makefile_path):
                raise BimBimError('BeamBeam: Missing library for coherent kick with flat beams')
            else:
                print('BeamBeam: The library for kick coherent with flat beams was not found. Attempting to build it')
                currentDir = os.getcwd()
                os.chdir(bblib_dir)
                os.system('make')
                os.chdir(currentDir)
                if not os.path.exists(bblib_path):
                    raise BimBimError('BeamBeam: Could not build library in',bblib_dir)
                else:
                    print('BeamBeam: lib built at',bblib_path)
        self._bb_lib = ctypes.CDLL(bblib_path)

        self._bb_lib.get_force_ellip.argtypes = [ctypes.c_double, ctypes.c_double, ctypes.c_double,
                                                 ctypes.c_double, ctypes.c_double, ctypes.c_double,
                                                 ctypes.POINTER(ctypes.c_double), ctypes.POINTER(ctypes.c_double),
                                                 ctypes.POINTER(ctypes.c_double), ctypes.POINTER(ctypes.c_double)]   

    def updateVirtualDrifts(self,sCP):
        if self._4D:
            self.drift[0,1] = sCP
            self.drift[2,3] = sCP
            self.drift[4,5] = -sCP
            self.drift[6,7] = -sCP
            self.invDrift[0,1] = -sCP
            self.invDrift[2,3] = -sCP
            self.invDrift[4,5] = sCP
            self.invDrift[6,7] = sCP
        else:
            self.drift[0,1] = sCP
            self.drift[2,3] = -sCP
            self.invDrift[0,1] = -sCP
            self.invDrift[2,3] = sCP

    def getBetaWithHourglass(self,beta,s=0):
        if self.hourglass:
            return beta*(1.0+s**2/beta**2)
        else:
            return beta

    def bb_force_round(self,bunch_target,bunch_source,sep_x,sep_y,beta):
        if abs(sep_x) < 1E-10 and abs(sep_y) < 1E-10:
            kbbxx = -0.5*bunch_source.intensity*bunch_source.particleCharge/np.abs(bunch_source.particleCharge)*bunch_target.particleCharge*cst.e/(4*np.pi*cst.epsilon_0*bunch_target.particleMass*1e9*bunch_source.phys_emit_x*bunch_target.gamma*beta)
            kbbyy = kbbxx
            kbbxy = 0.0
        else:
            #TODO re-check against Joanna's and adapt for aymetric beams
            sigma = np.sqrt(beta*bunch_source.phys_emit_x)
            x = sep_x
            y = sep_y
            r = np.sqrt(x**2+y**2)
            E = np.exp(-r**2/(4.0*sigma**2))
            A1 = 2.0*bunch_source.intensity*bunch_source.r0/(bunch_source.gamma)
            B1 = (1.0/r**2-2.0*x**2/r**4)*(1.0-E)
            B2 = x**2*E/(2.0*r**2*sigma**2)
            kbbxx = -A1*(B1+B2)
            B1 = (1.0/r**2-2.0*y**2/r**4)*(1.0-E)
            B2 = y**2*E/(2.0*r**2*sigma**2)
            kbbyy = -A1*(B1+B2)
            if self.XYCoupling:
                B1 = -2/r**4*(1.0-E)
                B2 = E/(2.0*r**2*sigma**2)
                kbbxy = -A1*x*y*(B1+B2)
            else:
                kbbxy = 0
        return kbbxx,kbbyy,kbbxy

    def bb_force_ellip(self,bunch_target,bunch_source,sep_x,sep_y,sigma_x,sigma_y):

        kbbxx = ctypes.c_double(0.0)
        kbbyy = ctypes.c_double(0.0)
        kbbxy = ctypes.c_double(0.0)
        kbbyx = ctypes.c_double(0.0)

        self._bb_lib.get_force_ellip(sigma_x, sigma_y, bunch_source.intensity,
                             sep_x, sep_y, bunch_target.energy, kbbxx, kbbyy, kbbxy, kbbyx)

        if self.XYCoupling == False:
            kbbxy.value = 0
            kbbyx.value = 0

        return kbbxx.value, kbbyy.value, kbbxy.value, kbbyx.value

    def updateBeamBeamKickRoundSym(self,basis,bunchB1,weightB1,bunchB2,weightB2,sCP):
        sep_x = 2.0*sCP*np.tan(self.xing_x/2.0)
        sep_y = 2.0*sCP*np.tan(self.xing_y/2.0)
        beta = self.getBetaWithHourglass(self.betaStar_x_B1,sCP)
        sigma = np.sqrt(self.betaStar_x_B1*bunchB1.phys_emit_x)
        kxB2,kyB2,kxyB2 = self.bb_force_round(bunchB2,bunchB1,self.sepStar_x*sigma+sep_x,self.sepStar_y*sigma+sep_y,beta)
        kxB1,kyB1,kxyB1 = self.bb_force_round(bunchB1,bunchB2,self.sepStar_x*sigma+sep_x,self.sepStar_y*sigma+sep_y,beta)
        if self._4D:
            self.beamBeamMatrix[1,0] = -kxB1*weightB2
            self.beamBeamMatrix[1,2] = -kxyB1*weightB2
            self.beamBeamMatrix[1,4] = kxB1*weightB2
            self.beamBeamMatrix[1,6] = kxyB1*weightB2
            
            self.beamBeamMatrix[3,0] = -kxyB1*weightB2
            self.beamBeamMatrix[3,2] = -kyB1*weightB2
            self.beamBeamMatrix[3,4] = kxyB1*weightB2
            self.beamBeamMatrix[3,6] = kyB1*weightB2
            
            self.beamBeamMatrix[5,0] = kxB2*weightB1
            self.beamBeamMatrix[5,2] = kxyB2*weightB1
            self.beamBeamMatrix[5,4] = -kxB2*weightB1
            self.beamBeamMatrix[5,6] = -kxyB2*weightB1
            
            self.beamBeamMatrix[7,0] = kxyB2*weightB1
            self.beamBeamMatrix[7,2] = kyB2*weightB1
            self.beamBeamMatrix[7,4] = -kxyB2*weightB1
            self.beamBeamMatrix[7,6] = -kyB2*weightB1
        else:
            if basis.plane[0] == 'x':
                self.beamBeamMatrix[1,0] = -kxB1*weightB2
                self.beamBeamMatrix[1,2] = kxB1*weightB2
            
                self.beamBeamMatrix[3,0] = kxB2*weightB1
                self.beamBeamMatrix[3,2] = -kxB2*weightB1
            elif basis.plane[0] == 'y':
                self.beamBeamMatrix[1,0] = -kyB1*weightB2
                self.beamBeamMatrix[1,2] = kyB1*weightB2
            
                self.beamBeamMatrix[3,0] = kyB2*weightB1
                self.beamBeamMatrix[3,2] = -kyB2*weightB1
            else:
                raise BimBimError(f'BeamBeam: {basis.plane[0]} is not a valid plane')

    def updateBeamBeamKickSym(self,basis,bunchB1,weightB1,bunchB2,weightB2,sCP):
        if self._bb_lib is None:
            self._load_bb_lib()
        sep_x = 2.0*sCP*np.tan(self.xing_x/2.0)
        sep_y = 2.0*sCP*np.tan(self.xing_y/2.0)
        sigmaStar_x = np.sqrt(self.betaStar_x_B1*bunchB1.phys_emit_x)
        sigmaStar_y = np.sqrt(self.betaStar_y_B1*bunchB1.phys_emit_y)
        sigma_x = np.sqrt(self.getBetaWithHourglass(self.betaStar_x_B1,sCP) * bunchB1.phys_emit_x)
        sigma_y = np.sqrt(self.getBetaWithHourglass(self.betaStar_y_B1,sCP) * bunchB1.phys_emit_y)
        kxB2,kyB2,kxyB2,kyxB2 = self.bb_force_ellip(bunchB2,bunchB1,self.sepStar_x*sigmaStar_x+sep_x,self.sepStar_y*sigmaStar_y+sep_y,sigma_x,sigma_y)
        kxB1,kyB1,kxyB1,kyxB1 = self.bb_force_ellip(bunchB1,bunchB2,self.sepStar_x*sigmaStar_x+sep_x,self.sepStar_y*sigmaStar_y+sep_y,sigma_x,sigma_y)
        if self._4D:
            self.beamBeamMatrix[1,0] = -kxB1*weightB2
            self.beamBeamMatrix[1,2] = -kxyB1*weightB2
            self.beamBeamMatrix[1,4] = kxB1*weightB2
            self.beamBeamMatrix[1,6] = kxyB1*weightB2
            
            self.beamBeamMatrix[3,0] = -kyxB1*weightB2
            self.beamBeamMatrix[3,2] = -kyB1*weightB2
            self.beamBeamMatrix[3,4] = kyxB1*weightB2
            self.beamBeamMatrix[3,6] = kyB1*weightB2
            
            self.beamBeamMatrix[5,0] = kxB2*weightB1
            self.beamBeamMatrix[5,2] = kxyB2*weightB1
            self.beamBeamMatrix[5,4] = -kxB2*weightB1
            self.beamBeamMatrix[5,6] = -kxyB2*weightB1
            
            self.beamBeamMatrix[7,0] = kyxB2*weightB1
            self.beamBeamMatrix[7,2] = kyB2*weightB1
            self.beamBeamMatrix[7,4] = -kyxB2*weightB1
            self.beamBeamMatrix[7,6] = -kyB2*weightB1
        else:
            if basis.plane[0] == 'x':
                self.beamBeamMatrix[1,0] = -kxB1*weightB2
                self.beamBeamMatrix[1,2] = kxB1*weightB2
                self.beamBeamMatrix[3,0] = kxB2*weightB1
                self.beamBeamMatrix[3,2] = -kxB2*weightB1
            elif basis.plane[0] == 'y':
                self.beamBeamMatrix[1,0] = -kyB1*weightB2
                self.beamBeamMatrix[1,2] = kyB1*weightB2
                self.beamBeamMatrix[3,0] = kyB2*weightB1
                self.beamBeamMatrix[3,2] = -kyB2*weightB1
            else:
                raise BimBimError(f'BeamBeam: {basis.plane[0]} is not a valid plane')


    def updateBeamBeamKick(self,basis,bunchB1,weightB1,bunchB2,weightB2,sCP):
        if (self.betaStar_x_B1 == self.betaStar_y_B1 and
            self.betaStar_x_B1 == self.betaStar_x_B2 and
            self.betaStar_x_B1 == self.betaStar_y_B2 and
            bunchB1.phys_emit_x == bunchB1.phys_emit_y and
            bunchB1.phys_emit_x == bunchB2.phys_emit_x and
            bunchB1.phys_emit_x == bunchB2.phys_emit_y):
            self.updateBeamBeamKickRoundSym(basis,bunchB1,weightB1,bunchB2,weightB2,sCP)
        elif (self.betaStar_x_B1 == self.betaStar_x_B2 and
            self.betaStar_y_B1 == self.betaStar_y_B2 and
            bunchB1.phys_emit_x == bunchB2.phys_emit_x and 
            bunchB1.phys_emit_y == bunchB2.phys_emit_y):
            self.updateBeamBeamKickSym(basis,bunchB1,weightB1,bunchB2,weightB2,sCP)
        else:
            raise BimBimError('BeamBeam: Beam-beam kick with unequal beta or physical emittance in the two beams is not implemented')
        
    def beamBeam(self,bunchB1,bunchB2,basis):
        fullBBMat = np.identity(2*basis.bunchBlockSize)
        collSched = self.generateCollisionSchedule(bunchB1,bunchB2,basis)
        for collision in collSched:
            iSliceB1 = collision[0]
            iRingB1 = collision[1]
            iSliceB2 = collision[2]
            iRingB2 = collision[3]
            sB1 = basis.getSPosition(iSliceB1,iRingB1,bunchB1.sigma_z)
            sB2 = basis.getSPosition(iSliceB2,iRingB2,bunchB2.sigma_z)
            sCP = (sB1-sB2)/2.0
            if self.virtualDrift:
                self.updateVirtualDrifts(sCP)
            self.updateBeamBeamKick(basis,bunchB1,basis.getWeight(iSliceB1,iRingB1),bunchB2,basis.getWeight(iSliceB2,iRingB2),sCP)
            tmpBBMat = dotProd([self.invDrift,self.beamBeamMatrix,self.drift])
            basis.binaryInteractionOptimisedDotProduct(tmpBBMat,fullBBMat,iSliceB1,iRingB1,1,0,iSliceB2,iRingB2,1,1)
        return fullBBMat

    def getMatrix(self,beams,pos,basis):
        self._init(basis)
        bunchB1 = beams.bunchConfigB1[pos]
        bunchB2 = beams.bunchConfigB2[pos]
        if self.phaseAdvance:
            raise BimBimError('Phase advance is not yet implemented in BeamBeam.')
        if bunchB1 != None and bunchB2 != None:
            beamBeamMat = self.beamBeam(bunchB1,bunchB2,basis)
            retVal = basis.getTwoBunchProjection(bunchB1.number,bunchB2.number,beamBeamMat)
            return retVal.tocsr()
            # return retVal
        return spm.identity(basis.size,format='csr')

