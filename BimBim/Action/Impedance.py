import scipy.sparse as spm
import time
import numpy as np
import os
import pickle
from scipy import constants as cst

from BimBim.Action import Action
from BimBim.Error import BimBimError
from BimBim.Matrix import printMatrix,insert


class Impedance(Action.Action):

    # betas correspond to the ratio with respect to the tracking beta
    def __init__(self,wakeDefinition,beta,quadWake=False,pickleFileName=None,intensityScaling = 1.0, keepInMemory = True,
                    mimicbunch_n_bunch=0,mimicbunch_distance=0.0,
                    mimicbunch_phase_x=0.0,mimicbunch_phase_y=0.0,mimicbunch_beta_x=0.0,mimicbunch_beta_y=0.0):
        self._GeVToKg = cst.e/cst.c**2*1E9
        beta_tmp = np.atleast_1d(beta)
        self._beta_x = beta_tmp[0]
        if len(beta_tmp)>1:
            self._beta_y = beta_tmp[1]
        self._quadWake = quadWake
        self._wakeDefinition = wakeDefinition
        self._intensityScaling = intensityScaling
        self._mimicbunch_n_bunch = mimicbunch_n_bunch
        self._mimicbunch_distance = mimicbunch_distance
        self._mimicbunch_phase_x = mimicbunch_phase_x
        self._mimicbunch_phase_y = mimicbunch_phase_y
        self._mimicbunch_beta_x = mimicbunch_beta_x
        self._mimicbunch_beta_y = mimicbunch_beta_y
        self.keepInMemory = keepInMemory
        if pickleFileName == None:
            self._singleBunchMatrices = {}
        else:
            self.loadMatrices(pickleFileName)

    @property
    def beta_x(self):
        return self._beta_x

    @beta_x.setter
    def beta_x(self, value):
        self._beta_x = value
        self._singleBunchMatrices.clear()

    @property
    def beta_y(self):
        return self._beta_y

    @beta_y.setter
    def beta_y(self, value):
        self._beta_y = value
        self._singleBunchMatrices.clear()

    @property
    def quadWake(self):
        return self._quadWake

    @quadWake.setter
    def quadWake(self, value):
        self._quadWake = value
        self._singleBunchMatrices.clear()

    @property
    def wakeDefinition(self):
        return self._wakeDefinition

    @wakeDefinition.setter
    def wakeDefinition(self, value):
        self._wakeDefinition = value
        self._singleBunchMatrices.clear()

    @property
    def intensityScaling(self):
        return self._intensityScaling

    @intensityScaling.setter
    def intensityScaling(self, value):
        self._intensityScaling = value
        self._singleBunchMatrices.clear()

    @property
    def mimicbunch_n_bunch(self):
        return self._mimicbunch_n_bunch

    @mimicbunch_n_bunch.setter
    def mimicbunch_n_bunch(self, value):
        self._mimicbunch_n_bunch = value
        self._singleBunchMatrices.clear()

    @property
    def mimicbunch_distance(self):
        return self._mimicbunch_distance

    @mimicbunch_distance.setter
    def mimicbunch_distance(self, value):
        self._mimicbunch_distance = value
        self._singleBunchMatrices.clear()
        
    @property
    def mimicbunch_phase_x(self):
        return self._mimicbunch_phase_x

    @mimicbunch_phase_x.setter
    def mimicbunch_phase_x(self, value):
        self._mimicbunch_phase_x = value
        self._singleBunchMatrices.clear()

    @property
    def mimicbunch_phase_y(self):
        return self._mimicbunch_phase_y

    @mimicbunch_phase_y.setter
    def mimicbunch_phase_y(self, value):
        self._mimicbunch_phase_y = value
        self._singleBunchMatrices.clear()

    @property
    def mimicbunch_beta_x(self):
        return self._mimicbunch_beta_x

    @mimicbunch_beta_x.setter
    def mimicbunch_beta_x(self, value):
        self._mimicbunch_beta_x = value
        self._singleBunchMatrices.clear()

    @property
    def mimicbunch_beta_y(self):
        return self._mimicbunch_beta_y

    @mimicbunch_beta_y.setter
    def mimicbunch_beta_y(self, value):
        self._mimicbunch_beta_y = value
        self._singleBunchMatrices.clear()
        
    def pickleMatrices(self,fileName):
        print('Pickling impedance matrix')
        pickleFile = open(fileName,'wb')
        pickle.dump(self._singleBunchMatrices,pickleFile)
        pickleFile.close()

    def loadMatrices(self,pickleFileName):
        if not os.path.exists(pickleFileName):
            print(pickleFileName,'does not exist')
            self._singleBunchMatrices = {}
        else:
            print('Loading impedance matrix',pickleFileName)
            pickleFile = open(pickleFileName,'rb')
            self._singleBunchMatrices = pickle.load(pickleFile)
            pickleFile.close()
    
    def getSingleBunchImpedanceMatrix(self,basis,bunch):
        cst0 = cst.e**2/(bunch.particleMass*self._GeVToKg*bunch.gamma*bunch.beta**2*cst.c**2)*bunch.intensity*self.intensityScaling
        sfact = cst.c/(bunch.beta*1.0e9)
        key = (basis.nSlice,basis.nRing,basis.nDim,cst0,sfact,bunch.sigma_z)
        if key in self._singleBunchMatrices.keys():
            print('Re-using impedance matrix',key)
            return self._singleBunchMatrices[key]
        else:
            #print('Impedance matrix not found, computing it',key)
            zMatrix = np.identity(basis.bunchBlockSize)
            for iSliceSource in range(basis.nSlice):
                for iSliceTest in range(basis.nSlice):
                    for iRingSource in range(basis.nRing):
                        for iRingTest in range(basis.nRing):
                            distance = basis.getSDiff(iSliceTest,iRingTest,iSliceSource,iRingSource,bunch.sigma_z)/sfact
                            dipx = dipy = quadx = quady = 0.0
                            dipx_p = dipy_p = quadx_p = quady_p = 0.0
                            if distance > 0.0:
                                dipx,dipy,quadx,quady = self.wakeDefinition.getWake(distance)
                            if self.mimicbunch_n_bunch > 0:
                                mimic_bunches = np.arange(1,self.mimicbunch_n_bunch+1)
                                mimic_distances = distance + mimic_bunches*self.mimicbunch_distance
                                mimic_dipx,mimic_dipy,mimic_quadx,mimic_quady = self.wakeDefinition.getWake(mimic_distances)
                                mimic_phases_x = mimic_bunches*self.mimicbunch_phase_x
                                mimic_phases_y = mimic_bunches*self.mimicbunch_phase_y
                                cosphis_x = np.cos(mimic_phases_x)
                                betasinphis_x = self.mimicbunch_beta_x*np.sin(mimic_phases_x)
                                cosphis_y = np.cos(mimic_phases_y)
                                betasinphis_y = self.mimicbunch_beta_x*np.sin(mimic_phases_y)
                                dipx += np.sum(mimic_dipx*cosphis_x)
                                dipx_p += np.sum(mimic_dipx*betasinphis_x)
                                quadx += np.sum(mimic_quadx*cosphis_x)
                                quadx_p += np.sum(mimic_quadx*betasinphis_x)
                                dipy += np.sum(mimic_dipy*cosphis_y)
                                dipy_p += np.sum(mimic_dipy*betasinphis_y)
                                quady += np.sum(mimic_quady*cosphis_y)
                                quady_p += np.sum(mimic_quady*betasinphis_y)
                            if dipx != 0.0 or dipy != 0.0 or quadx != 0.0 or quady != 0.0 or dipx_p != 0.0 or dipy_p != 0.0 or quadx_p != 0.0 or quady_p != 0.0:
                                weight = basis.getWeight(iSliceSource,iRingSource)
                                if basis.nDim == 2:
                                    basisIndexForTest = basis.getIndexForSlice(iSliceTest,iRingTest)
                                    if basis.plane[0] == 'y':
                                        tmpDip = dipy
                                        tmQuad = quady
                                        tmpDip_p = dipy_p
                                        tmQuad_p = quady_p
                                        beta = self.beta_y
                                    elif basis.plane[0] == 'x':
                                        tmpDip = dipx
                                        tmQuad = quadx
                                        tmpDip_p = dipx_p
                                        tmQuad_p = quadx_p
                                        beta = self.beta_x
                                    else:
                                        print(f'Impedance: {basis.plane[0]} is not a valid plane')
                                    zMatrix[basisIndexForTest+1,basis.getIndexForSlice(iSliceSource,iRingSource)] = tmpDip*cst0*beta*weight
                                    zMatrix[basisIndexForTest+1,basis.getIndexForSlice(iSliceSource,iRingSource)+1] += tmpDip_p*cst0*beta*weight
                                    if self.quadWake:
                                        zMatrix[basisIndexForTest+1,basisIndexForTest] += tmQuad*cst0*beta*weight
                                        zMatrix[basisIndexForTest+1,basisIndexForTest+1] += tmQuad_p*cst0*beta*weight
                                elif basis.nDim == 4:
                                    basisIndexForTest = basis.getIndexForSlice(iSliceTest,iRingTest)
                                    zMatrix[basisIndexForTest+1,basis.getIndexForSlice(iSliceSource,iRingSource)] = dipx*cst0*self.beta_x*weight
                                    zMatrix[basisIndexForTest+3,basis.getIndexForSlice(iSliceSource,iRingSource)+2] = dipy*cst0*self.beta_y*weight
                                    zMatrix[basisIndexForTest+1,basis.getIndexForSlice(iSliceSource,iRingSource)+1] += dipx_p*cst0*self.beta_x*weight
                                    zMatrix[basisIndexForTest+3,basis.getIndexForSlice(iSliceSource,iRingSource)+3] += dipy_p*cst0*self.beta_y*weight
                                    if self.quadWake:
                                        zMatrix[basisIndexForTest+1,basisIndexForTest] += quadx*cst0*self.beta_x*weight
                                        zMatrix[basisIndexForTest+3,basisIndexForTest+2] += quady*cst0*self.beta_y*weight
                                        zMatrix[basisIndexForTest+1,basisIndexForTest+1] += quadx_p*cst0*self.beta_x*weight
                                        zMatrix[basisIndexForTest+3,basisIndexForTest+3] += quady_p*cst0*self.beta_y*weight
                                else:
                                    raise BimBimError('Impedance is not implemented in '+str(basis.nDim)+' dimensions')
            if self.keepInMemory:
               self._singleBunchMatrices[key] = zMatrix
            return zMatrix
    
    def getCouplingMatrix(self,basis,beam,sourceBunch,testBunch):
        retVal = np.zeros((basis.bunchBlockSize,basis.getBeamBlockSize(beam)))
        sourceBunchIndex = basis.getIndexForBunch(sourceBunch.number)
        testBunchIndex = basis.getIndexForBunch(testBunch.number)
        distance = testBunch.longitudinal_position-sourceBunch.longitudinal_position
        dipx,dipy,quadx,quady = self.wakeDefinition.getWake(distance)
        cst0 = self._qe**2/(sourceBunch.particleMass*self._GeVToKg*sourceBunch.gamma*sourceBunch.beta**2*cst.c**2)*sourceBunch.intensity*self.intensityScaling
        for iSliceSource in range(basis.nSlice):
            for iSliceTest in range(basis.nSlice):
                for iRingSource in range(basis.nRing):
                    for iRingTest in range(basis.nRing):
                        weight = basis.getWeight(iSliceSource,iRingSource)
                        if basis.nDim == 2:
                            basisIndexForTest = basis.getIndexForSlice(iSliceTest,iRingTest)
                            if basis.plane[0] == 'y':
                                tmpDip = dipy
                                tmQuad = quady
                            elif basis.plane[0] == 'x':
                                tmpDip = dipx
                                tmQuad = quadx
                            else:
                                print(f'Impedance: {basis.plane[0]} is not a valid plane')
                            retVal[basisIndexForTest+1,sourceBunchIndex+basis.getIndexForSlice(iSliceSource,iRingSource)] = tmpDip*cst0*self.beta_x*weight
                            if self.quadWake:
                                retVal[basisIndexForTest+1,testBunchIndex + basisIndexForTest] = tmQuad*cst0*self.beta_x*weight
                        elif basis.nDim == 4:
                            basisIndexForTest = basis.getIndexForSlice(iSliceTest,iRingTest)
                            retVal[basisIndexForTest+1,sourceBunchIndex + basis.getIndexForSlice(iSliceSource,iRingSource)] = dipx*cst0*self.beta_x*weight
                            retVal[basisIndexForTest+3,sourceBunchIndex + basis.getIndexForSlice(iSliceSource,iRingSource)+2] = dipy*cst0*self.beta_y*weight
                            if self.quadWake:
                                retVal[basisIndexForTest+1,testBunchIndex + basis.getIndexForSlice(iSliceSource,iRingSource)] = quadx*cst0*self.beta_x*weight
                                retVal[basisIndexForTest+3,testBunchIndex + basis.getIndexForSlice(iSliceSource,iRingSource)+2] = quady*cst0*self.beta_y*weight
                        else:
                            raise BimBimError('Multibunch impedance is not implemented in '+str(basis.nDim)+' dimensions')
        return retVal

    #Build impedance matrix from wake table
    def getImpedanceMatrix(self,basis,beams,beam,bunch):
        singleMatrix = self.getSingleBunchImpedanceMatrix(basis,bunch)

        #myFile = open('impedanceMat.pkl','wb')
        #pickle.dump(singleMatrix,myFile)
        #myFile.close()

        #singleMatrix = spm.identity(basis.bunchBlockSize,format='dok')
        zMatrix = np.zeros((basis.bunchBlockSize,basis.getBeamBlockSize(beam)))
        insert(zMatrix,singleMatrix,0,basis.getIndexForBunch(bunch.number))
        for sourceBunch in beams.getBunchConfig(beam):
            if sourceBunch != None:
                if bunch.longitudinal_position > sourceBunch.longitudinal_position:
                    cMatrix = self.getCouplingMatrix(basis,beam,sourceBunch,bunch)
                    zMatrix += cMatrix
        return zMatrix

    def getMatrix(self,beams,pos,basis):
        bunchB1 = beams.getBunch(pos,beam=1)
        bunchB2 = beams.getBunch(pos,beam=2)
        if bunchB1 != None and bunchB2 == None:
            beam = 1
            #print('Building impedance matrix for B1b'+str(bunchB1.number))
            zMatrix = self.getImpedanceMatrix(basis,beams,beam,bunchB1)
            zMatrix = basis.getFullBunchProjection(beam,bunchB1.number,zMatrix)
            #myFile = open('debug_impedance_B'+str(beam)+'b'+str(bunchNb)+'.mat','w')
            #printMatrix(myFile,zMatrix)
            #myFile.close()
            return zMatrix.tocsr()
        elif bunchB2 != None and bunchB1 == None:
            beam = 2
            #print('Building impedance matrix for B2b'+str(bunchNb))
            zMatrix = self.getImpedanceMatrix(basis,beams,beam,bunchB2)
            zMatrix = basis.getFullBunchProjection(beam,bunchB2.number,zMatrix)
            #myFile = open('debug_impedance_B'+str(beam)+'b'+str(bunchNb)+'.mat','w')
            #printMatrix(myFile,zMatrix)
            #myFile.close()
            return zMatrix.tocsr()
        elif bunchB1 != None and bunchB2 != None:
            #print('Building impedance matrix for B2b'+str(bunchNb))
            zMatrix = self.getImpedanceMatrix(basis,beams,1,bunchB1)
            zMatrix1 = basis.getFullBunchProjection(1,bunchB1.number,zMatrix)
            zMatrix = self.getImpedanceMatrix(basis,beams,2,bunchB2)
            zMatrix2 = basis.getFullBunchProjection(2,bunchB2.number,zMatrix)
            zMatrix = zMatrix1.dot(zMatrix2)
            #myFile = open('debug_impedance_B'+str(beam)+'b'+str(bunchNb)+'.mat','w')
            #printMatrix(myFile,zMatrix)
            #myFile.close()
            return zMatrix.tocsr()
        else:
            return spm.identity(basis.size,format='csr')
