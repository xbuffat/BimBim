import time, os, pickle
import scipy.sparse as spm
import numpy as np
import scipy.constants as cst
from BimBim.Action import Action
from BimBim.Matrix import printMatrix
from BimBim.Error import BimBimError

class SpaceCharge(Action.Action):

    def __init__(self, nZ=100, nTheta=40, nR=40, circum = 26700, opticalBeta=1.0,pickleInteractionMatrixDir=None,dipolar=True,quadrupolar=True):
        self._GeVToKg = cst.e/cst.c**2*1E9
        self.nZ = nZ
        self.nTheta = nTheta
        self.nR = nR
        self.circum = circum
        self.opticalBeta = opticalBeta
        if isinstance(self.opticalBeta,list):
            raise BimBimError('SpaceCharge cannot take a list of optical betas (unequal beam sized are not implemented)')
        self._dipolar = dipolar
        self._quadrupolar = quadrupolar
        self.convergenceCriterion = 5E-2
        self.convergenceStep = 1.2
        self.convergenceMaxN = 100
        self.usePickledInteractionMatrix = False

        if pickleInteractionMatrixDir!= None:
            self.usePickledInteractionMatrix = True
            self.pickleInteractionMatrixDir = pickleInteractionMatrixDir

    def getPickleFileName(self,basis):
        return os.path.join(self.pickleInteractionMatrixDir,'BimBimSpaceChargeInteractionMatrix_nSlice{:04d}_nRing{:04d}_{:s}.pkl'.format(basis.nSlice,basis.nRing, basis.distribution.getDescription()))
     
    def getC(self, bunch):    #constant A, equation (7) overleaf, now aded sig s and optical beta
        if bunch.phys_emit_x != bunch.phys_emit_y:
            raise BimBimError('SpaceCharge is not implemented for unequal emittances')
        C1 = - cst.e**2 * bunch.intensity / (2 * np.pi * cst.epsilon_0 * bunch.particleMass*self._GeVToKg * cst.c**2 * bunch.beta**2 * bunch.gamma**3*bunch.sigma_z)
        C2 = 0.5 * self.circum /(bunch.phys_emit_x*self.opticalBeta)
        C = C1 * C2
        return C

    def getInteractionMatrix(self, basis):
        ok = False
        if self.usePickledInteractionMatrix: 
            pickleFileName = self.getPickleFileName(basis)
            if os.path.exists(pickleFileName):
                print('Space charge : loading',pickleFileName)
                myFile = open(pickleFileName,'rb')
                interactionMatrix = pickle.load(myFile)
                myFile.close()
                ok = True
        if not ok:
            interactionMatrix = self.computeInteractionMatrix(basis)
            if self.usePickledInteractionMatrix:
                pickleFileName = self.getPickleFileName(basis)
                myFile = open(pickleFileName,'wb')
                pickle.dump(interactionMatrix,myFile)
                myFile.close()
        return interactionMatrix

    def computeInteractionMatrix(self,basis):
        bmat = np.zeros((basis.bunchBlockSize,basis.bunchBlockSize))
        for iSliceB1 in range(basis.nSlice):
            for iRingB1 in range(basis.nRing):
                for jSliceB1 in range(basis.nSlice):
                    for jRingB1 in range(basis.nRing):
                        if iSliceB1!=jSliceB1 or iRingB1!=jRingB1:
                            I=basis.getIndexForSlice(iSliceB1,iRingB1)
                            J=basis.getIndexForSlice(jSliceB1,jRingB1)
                            if basis.cellsOverlapLongitudinaly(iSliceB1,iRingB1,jSliceB1,jRingB1):
                                interaction_strength = self.computeInteractionMatrixElement(basis, iSliceB1, iRingB1, jSliceB1, jRingB1)
                                if self._dipolar:
                                    bmat[I+1,J] = interaction_strength
                                if self._quadrupolar:
                                    bmat[I+1,I] -= interaction_strength
                            #else:
                            #    print('Cells',iSliceB1,iRingB1,jSliceB1,jRingB1,'do not overlap')

        #myFile = open('matrix.mat','w');
        #printMatrix(myFile,bmat);
        #myFile.close();
        return bmat

    # test for analytical airbag
    def _computeInteractionMatrixElement(self,basis,iSlice,iRing,jSlice,jRing):
        if iSlice+1 == basis.nSlice-jSlice:
            x1 = np.cos(basis.getSliceAngle(iSlice)-np.pi/basis.nSlice)
            x2 = np.cos(basis.getSliceAngle(iSlice)+np.pi/basis.nSlice)
            if np.abs(x1) == 1 or np.abs(x2) == 1:
                return 1.0
            else:
                R = 1.0
                interaction = np.abs(basis.nSlice/(8*np.pi**2*R)*np.log((1+x2)*(1-x1)/((1-x2)*(1+x1))))
                return interaction
        else:
            return 0.0

    def computeInteractionMatrixElement(self,basis,iSlice,iRing,jSlice,jRing):
        time0 = time.time()
        n1 = self.nZ
        converged = False
        nStep = 0
        while not converged:
            n0 = n1
            n1 = n0*self.convergenceStep
            A0 = self.computeInteractionMatrixElement_nRConvergence(basis,iSlice,iRing,jSlice,jRing,n0)
            A1 = self.computeInteractionMatrixElement_nRConvergence(basis,iSlice,iRing,jSlice,jRing,n1)
            if A0 == 0.0 and A1 == 0.0:
                converged = True
            elif A0 == 0.0 and A1 != 0.0:
                converged = False
            else:
                converged = np.abs((A0-A1)/A0)<self.convergenceCriterion
                if not converged:
                   if n1 > self.convergenceMaxN:
                       converged = True
                       print('Convegence was not reached in nZ')
                #else:
                #    print('Converged nZ',n0,'in',nStep,'step',n0,n1,np.abs((A0-A1)/A0))
            nStep += 1
        print(iSlice,iRing,jSlice,jRing,'converged in',time.time()-time0,A1)
        return A0

    def computeInteractionMatrixElement_nRConvergence(self,basis,iSlice,iRing,jSlice,jRing,nZ):
        n1 = self.nR
        converged = False
        nStep = 0
        while not converged:
            n0 = n1
            n1 = n0*self.convergenceStep
            A0 = self.computeInteractionMatrixElement_nThetaConvergence(basis,iSlice,iRing,jSlice,jRing,nZ,n0)
            A1 = self.computeInteractionMatrixElement_nThetaConvergence(basis,iSlice,iRing,jSlice,jRing,nZ,n1)
            if A0 == 0.0 and A1 == 0.0:
                converged = True
            elif A0 == 0.0 and A1 != 0.0:
                converged = False
            else:
                converged = np.abs((A0-A1)/A0)<self.convergenceCriterion
                if not converged:
                   if n1 > self.convergenceMaxN:
                       converged = True
                       print('Convegence was not reached in nR')
                #else:
                #    print('Converged nR',n0,'in',nStep,'step',n0,n1,np.abs((A0-A1)/A0))
            nStep += 1
        return A0

    def computeInteractionMatrixElement_nThetaConvergence(self,basis,iSlice,iRing,jSlice,jRing,nZ,nR):
        n1 = self.nTheta
        converged = False
        nStep = 0
        while not converged:
            n0 = n1
            n1 = n0*self.convergenceStep
            A0 = self.computeInteractionMatrixElement_step(basis,iSlice,iRing,jSlice,jRing,nZ,nR,n0)
            A1 = self.computeInteractionMatrixElement_step(basis,iSlice,iRing,jSlice,jRing,nZ,nR,n1)
            if A0 == 0.0 and A1 == 0.0:
                converged = True
            elif A0 == 0.0 and A1 != 0.0:
                converged = False
            else:
                converged = np.abs((A0-A1)/A0)<self.convergenceCriterion
                if not converged:
                   if n1 > self.convergenceMaxN:
                       converged = True
                       print('Convegence was not reached in nTheta')
                #else:
                #    print('Converged nTheta',n0,'in',nStep,'step',n0,n1,np.abs((A0-A1)/A0))

            nStep += 1
        return A0

    def computeInteractionMatrixElement_step(self,basis,iSlice,iRing,jSlice,jRing,nZ,nR,nT):
        #time0 = time.time()
        dTheta = 2.0*np.pi/basis.nSlice/nT
        cosThetais = np.cos(np.arange(basis.getSliceAngle(iSlice)-np.pi/basis.nSlice,basis.getSliceAngle(iSlice)+np.pi/basis.nSlice,dTheta))
        cosThetajs = np.cos(np.arange(basis.getSliceAngle(jSlice)-np.pi/basis.nSlice,basis.getSliceAngle(jSlice)+np.pi/basis.nSlice,dTheta))
        dRi = (basis.ringDistribution[2][iRing+1]-basis.ringDistribution[2][iRing])/nR
        dRj = (basis.ringDistribution[2][jRing+1]-basis.ringDistribution[2][jRing])/nR
        Ris = np.arange(basis.ringDistribution[2][iRing],basis.ringDistribution[2][iRing+1],dRi)
        Rjs = np.arange(basis.ringDistribution[2][jRing],basis.ringDistribution[2][jRing+1],dRj)
        RiMesh,cosThetais = np.meshgrid(Ris,cosThetais)
        RjMesh,cosThetajs = np.meshgrid(Rjs,cosThetajs)
        ri,rj = np.meshgrid(RiMesh,RjMesh)
        zi,zj = np.meshgrid(Ris*cosThetais,Rjs*cosThetajs)
        mask = np.abs(zi-zj) < 1 / (2*nZ)
        disti = basis.getDistributionFunction(ri[mask])
        distj = basis.getDistributionFunction(rj[mask])
        A = np.sum(disti*distj)*dRj*dRi*dTheta**2*nZ
        B = (np.sum(basis.getDistributionFunction(Ris[1:-1]))+0.5* basis.getDistributionFunction(Ris[0]) +0.5* basis.getDistributionFunction(Ris[-1])) *dRi*2.0*np.pi/basis.nSlice
        #print(nZ,nR,nT,time.time()-time0)
        return A/B


    def computeInteractionMatrixElement_fixed(self,basis,iSlice,iRing,jSlice,jRing):
        dTheta = 2.0*np.pi/basis.nSlice/self.nTheta
        time0 = time.time()
        cosThetais = np.cos(np.arange(basis.getSliceAngle(iSlice)-np.pi/basis.nSlice,basis.getSliceAngle(iSlice)+np.pi/basis.nSlice,dTheta))
        cosThetajs = np.cos(np.arange(basis.getSliceAngle(jSlice)-np.pi/basis.nSlice,basis.getSliceAngle(jSlice)+np.pi/basis.nSlice,dTheta))
        dRi = (basis.ringDistribution[2][iRing+1]-basis.ringDistribution[2][iRing])/self.nR
        dRj = (basis.ringDistribution[2][jRing+1]-basis.ringDistribution[2][jRing])/self.nR
        Ris = np.arange(basis.ringDistribution[2][iRing],basis.ringDistribution[2][iRing+1],dRi)
        Rjs = np.arange(basis.ringDistribution[2][jRing],basis.ringDistribution[2][jRing+1],dRj)
        RiMesh,cosThetais = np.meshgrid(Ris,cosThetais)
        RjMesh,cosThetajs = np.meshgrid(Rjs,cosThetajs)
        ri,rj = np.meshgrid(RiMesh,RjMesh)
        zi,zj = np.meshgrid(Ris*cosThetais,Rjs*cosThetajs)
        mask = np.abs(zi-zj) < 1 / (2*self.nZ)
        disti = basis.getDistributionFunction(ri[mask])
        distj = basis.getDistributionFunction(rj[mask])
        prodDist = disti*distj
        print(np.shape(prodDist))
        A = np.sum(disti*distj)*dRj*dRi*dTheta**2*self.nZ
        #A = (np.sum(basis.getDistributionFunction(Ris[1:-1]))+0.5* basis.getDistributionFunction(Ris[0]) +0.5* basis.getDistributionFunction(Ris[-1]))*dRj*dRi*dTheta**2*self.nZ
        B = (np.sum(basis.getDistributionFunction(Ris[1:-1]))+0.5* basis.getDistributionFunction(Ris[0]) +0.5* basis.getDistributionFunction(Ris[-1]))*dRi*2.0*np.pi/basis.nSlice
        return A/B

    def getMatrix(self,beams,pos,basis):
        bunchB1 = beams.getBunch(pos,beam=1)
        bunchB2 = beams.getBunch(pos,beam=2)
        if bunchB1 != None and bunchB2 == None:
            singleBunchSCMap = self.getInteractionMatrix(basis)
            singleBunchSCMap = np.identity(basis.bunchBlockSize)+singleBunchSCMap *self.getC(bunchB1)
            retVal = basis.getFullBunchProjection(1,bunchB1.number,singleBunchSCMap);
            return retVal.tocsr()
        elif bunchB2 != None and bunchB1 == None:
            singleBunchSCMap = self.getInteractionMatrix(basis)
            singleBunchSCMap = np.identity(basis.bunchBlockSize)+singleBunchSCMap *self.getC(bunchB2)
            retVal = basis.getFullBunchProjection(2,bunchB2.number,singleBunchSCMap);
            return retVal.tocsr()
        elif bunchB1 != None and bunchB2 != None:
            singleBunchSCMap = self.getInteractionMatrix(basis)
            singleBunchSCMap1 = np.identity(basis.bunchBlockSize)+singleBunchSCMap *self.getC(bunchB1)
            zMatrix1 = basis.getFullBunchProjection(1,bunchB1.number,zMatrix)
            singleBunchSCMap2 = np.identity(basis.bunchBlockSize)+singleBunchSCMap *self.getC(bunchB2)
            zMatrix2 = basis.getFullBunchProjection(2,bunchB2.number,zMatrix);
            retVal = singleBunchSCMap1.dot(singleBunchSCMap2);
            return retVal.tocsr()
        else:
            retVal = spm.identity(basis.size,format='csr')
        return retVal


