import numpy as np

class ResonatorWake:

    def __init__(self,shuntImpedance,resonanceFrequency=1E9,qualityFactor=1.0,quadWakeRatio=0.0):
        self.omegar = 2.0*np.pi*resonanceFrequency
        self.R = shuntImpedance
        self.Qr = qualityFactor
        self.QPrime = np.sqrt(qualityFactor**2-0.25)
        self.omegaPrime = self.QPrime*self.omegar/self.Qr

        self._quadWakeRatio = quadWakeRatio

    # distance in ns
    def getWake(self,distance):
        retVal = self.omegar*self.R*np.exp(-1E-9*self.omegar*distance/(2.0*self.Qr))*np.sin(1E-9*self.omegaPrime*distance)/self.QPrime
        return retVal,retVal,self._quadWakeRatio*retVal,self._quadWakeRatio*retVal
