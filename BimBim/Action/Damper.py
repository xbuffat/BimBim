import scipy.sparse as spm
import numpy as np
from BimBim.Action import Action
from BimBim.Error import BimBimError
from BimBim.Matrix import printMatrix

class Damper(Action.Action):
    
    # All arguments can be lists to specify the two beams (by default the two beams have identical parameters)
    def __init__(self,gain_x=0.0,gain_y=0.0,phase_x=0.0,phase_y=0.0):
        gain_x_tmp = np.atleast_1d(gain_x)
        self.gain_x = [gain_x_tmp[0],gain_x_tmp[0]]
        if len(gain_x_tmp) > 1:
            self.gain_x[1] = gain_x_tmp[1]
        gain_y_tmp = np.atleast_1d(gain_y)
        self.gain_y = [gain_y_tmp[0],gain_y_tmp[0]]
        if len(gain_y_tmp) > 1:
            self.gain_y[1] = gain_y_tmp[1]
        phase_x_tmp = np.atleast_1d(phase_x)
        self.phase_x = [phase_x_tmp[0],phase_x_tmp[0]]
        if len(phase_x_tmp) > 1:
            self.phase_x[1] = phase_x_tmp[1]
        phase_y_tmp = np.atleast_1d(phase_y)
        self.phase_y = [phase_y_tmp[0],phase_y_tmp[0]]
        if len(phase_y_tmp) > 1:
            self.phase_y[1] = phase_y_tmp[1]

    #Build perfect damper matrix
    def getDamperMatrix(self,basis,beam_index=0):
        retVal = np.identity(basis.bunchBlockSize)
        if basis.nDim == 2:
            if basis.plane[0] == 'x':
                gain = self.gain_x[beam_index]
                cosphase = np.cos(self.phase_x[beam_index])
                sinphase = np.sin(self.phase_x[beam_index])
            elif basis.plane[0] == 'y':
                gain = self.gain_y[beam_index]
                cosphase = np.cos(self.phase_y[beam_index])
                sinphase = np.sin(self.phase_y[beam_index])
            else:
                raise BimBimError(f'Damper: {basis.plane[0]} is not a valid plane')
            for iSlice in range(basis.nSlice):
                for iRing in range(basis.nRing):
                    j = basis.getIndexForSlice(iSlice,iRing)
                    for i in range(0,basis.bunchBlockSize,2):
                        retVal[i+1,j+1] -= 2.0*gain*cosphase*basis.getWeight(iSlice,iRing)
                        retVal[i+1,j] -= 2.0*gain*sinphase*basis.getWeight(iSlice,iRing)
        elif basis.nDim == 4:
            gainX = self.gain_x[beam_index]
            cosphaseX = np.cos(self.phase_x[beam_index])
            sinphaseX = np.sin(self.phase_x[beam_index])
            gainY = self.gain_y[beam_index]
            cosphaseY = np.cos(self.phase_y[beam_index])
            sinphaseY = np.sin(self.phase_y[beam_index])
            for iSlice in range(basis.nSlice):
                for iRing in range(basis.nRing):
                    j = basis.getIndexForSlice(iSlice,iRing)
                    for i in range(0,basis.bunchBlockSize,2):
                        retVal[i+1,j+1] -= 2.0*gainX*cosphaseX*basis.getWeight(iSlice,iRing)
                        retVal[i+1,j] -= 2.0*gainX*sinphaseX*basis.getWeight(iSlice,iRing)
                        retVal[i+3,j+3] -= 2.0*gainY*cosphaseY*basis.getWeight(iSlice,iRing)
                        retVal[i+3,j+2] -= 2.0*gainY*sinphaseY*basis.getWeight(iSlice,iRing)
        else:
            raise BimBimError('Damper is not implemented in '+str(basis.nDim)+' dimensions')
        return retVal
      
    def getMatrix(self,beams,pos,basis):
        if beams.getBunch(pos,beam=1) != None and beams.getBunch(pos,beam=2) == None:
            dMatrix = self.getDamperMatrix(basis,beam_index=0)
            retVal = basis.getBunchProjection(1,beams.getBunch(pos,beam=1).number,dMatrix)
        elif beams.getBunch(pos,beam=2) != None and beams.getBunch(pos,beam=1) == None:
            dMatrix = self.getDamperMatrix(basis,beam_index=1)
            retVal = basis.getBunchProjection(2,beams.getBunch(pos,beam=2).number,dMatrix)
        elif beams.getBunch(pos,beam=1) != None and beams.getBunch(pos,beam=2) != None:
            dMatrix_B1 = self.getDamperMatrix(basis,beam_index=0)
            projB1 = basis.getBunchProjection(1,beams.getBunch(pos,beam=1).number,dMatrix_B1)
            dMatrix_B2 = self.getDamperMatrix(basis,beam_index=1)
            projB2 = basis.getBunchProjection(2,beams.getBunch(pos,beam=1).number,dMatrix_B2)
            retVal = proB1.dot(projB2)
        else:
            retVal = spm.identity(basis.size,format='csr')
        return retVal
            
            
class SingleTurnADT(Action.Action):    
    # omega needs to be multiplied by sigma_z : omega = 2*pi*frec*sigma_z/c (TODO give bunch as an argument for getDamperMatrix instead)
    # soffset is an offset with in units of the bunch length (same issue as above)
    # gains and phases can be lists to specify the two beams (by default the two beams have identical parameters)
    def __init__(self,gain_x=0.0,gain_y=0.0,phase_x=0.0,phase_y=0.0,gain_HT_x=0.0,gain_HT_y=0.0,phase_HT_x=0.0,phase_HT_y=0.0,omega=0,soffset=0):
        gain_x_tmp = np.atleast_1d(gain_x)
        self.gain_x = [gain_x_tmp[0],gain_x_tmp[0]]
        if len(gain_x_tmp) > 1:
            self.gain_x[1] = gain_x_tmp[1]

        gain_y_tmp = np.atleast_1d(gain_y)
        self.gain_y = [gain_y_tmp[0],gain_y_tmp[0]]
        if len(gain_y_tmp) > 1:
            self.gain_y[1] = gain_y_tmp[1]

        phase_x_tmp = np.atleast_1d(phase_x)
        self.phase_x = [phase_x_tmp[0],phase_x_tmp[0]]
        if len(phase_x_tmp) > 1:
            self.phase_x[1] = phase_x_tmp[1]

        phase_y_tmp = np.atleast_1d(phase_y)
        self.phase_y = [phase_y_tmp[0],phase_y_tmp[0]]
        if len(phase_y_tmp) > 1:
            self.phase_y[1] = phase_y_tmp[1]

        gain_HT_x_tmp = np.atleast_1d(gain_HT_x)
        self.gain_HT_x = [gain_HT_x_tmp[0],gain_HT_x_tmp[0]]
        if len(gain_HT_x_tmp) > 1:
            self.gain_HT_x[1] = gain_HT_x_tmp[1]

        gain_HT_y_tmp = np.atleast_1d(gain_HT_y)
        self.gain_HT_y = [gain_HT_y_tmp[0],gain_HT_y_tmp[0]]
        if len(gain_HT_y_tmp) > 1:
            self.gain_HT_y[1] = gain_HT_y_tmp[1]

        phase_HT_x_tmp = np.atleast_1d(phase_HT_x)
        self.phase_HT_x = [phase_HT_x_tmp[0],phase_HT_x_tmp[1]]
        if len(phase_HT_x_tmp) > 1:
            self.phase_HT_x[1] = phase_HT_x_tmp[1]

        phase_HT_y_tmp = np.atleast_1d(phase_HT_y)
        self.phase_HT_y = [phase_HT_y_tmp[0],phase_HT_y_tmp[0]]
        if len(phase_HT_y_tmp) > 1:
            self.phase_HT_y[1] = phase_HT_y_tmp[1]

        self.omega = omega
        self.soffset = soffset

    #Build ADT matrix
    def getDamperMatrix(self,basis,beam_index):
        retVal = np.identity(basis.bunchBlockSize)
        if basis.nDim == 2:
            Isum = 0
            Qsum = 0
            for iSlice in range(basis.nSlice):
                for iRing in range(basis.nRing):
                    Isum += basis.getWeight(iSlice,iRing)*np.cos(self.omega*(basis.getSPosition(iSlice,iRing,1.0)+self.soffset))
                    Qsum += basis.getWeight(iSlice,iRing)*np.sin(self.omega*(basis.getSPosition(iSlice,iRing,1.0)+self.soffset))
            norm = Isum*Isum+Qsum*Qsum
            if basis.plane[0] == 'x':
                gain = self.gain_x[beam_index]
                cosphase = np.cos(self.phase_x[beam_index])
                sinphase = np.sin(self.phase_x[beam_index])
                gain_HT = self.gain_HT_x[beam_index]
                cosphase_HT = np.cos(self.phase_HT_x[beam_index])
                sinphase_HT = np.sin(self.phase_HT_x[beam_index])
            elif basis.plane[0] == 'y':
                gain = self.gain_y[beam_index]
                cosphase = np.cos(self.phase_y[beam_index])
                sinphase = np.sin(self.phase_y[beam_index])
                gain_HT = self.gain_HT_y[beam_index]
                cosphase_HT = np.cos(self.phase_HT_y[beam_index])
                sinphase_HT = np.sin(self.phase_HT_y[beam_index])
            else:
                raise BimBimError(f'Damper: {basis.plane[0]} is not a valid plane')
            for iSlice in range(basis.nSlice):
                for iRing in range(basis.nRing):
                    j = basis.getIndexForSlice(iSlice,iRing)
                    
                    Idiff = basis.getWeight(iSlice,iRing)*np.cos(self.omega*(basis.getSPosition(iSlice,iRing,1.0)+self.soffset))
                    Qdiff = basis.getWeight(iSlice,iRing)*np.sin(self.omega*(basis.getSPosition(iSlice,iRing,1.0)+self.soffset))
                    
                    kickRes = -2.0*gain*cosphase*(Idiff*Isum+Qdiff*Qsum)/norm
                    kickRea = -2.0*gain*sinphase*(Idiff*Isum+Qdiff*Qsum)/norm
                    kickHTRes = -2.0*gain_HT*cosphase_HT*(Qdiff*Isum-Idiff*Qsum)/norm
                    kickHTRea = -2.0*gain_HT*sinphase_HT*(Qdiff*Isum-Idiff*Qsum)/norm
                    for i in range(0,basis.bunchBlockSize,2):
                        retVal[i+1,j+1] = kickRes+kickHTRes
                        retVal[i+1,j] = kickRea+kickHTRea
                        if i==j:
                            retVal[i+1,j+1] += 1.0
        else:
            raise BimBimError('Damper is not implemented in '+str(basis.nDim)+' dimensions')
        return retVal
      
    def getMatrix(self,beams,pos,basis):
        if beams.getBunch(pos,beam=1) != None and beams.getBunch(pos,beam=2) == None:
            dMatrix = self.getDamperMatrix(basis,0)
            retVal = basis.getBunchProjection(1,beams.getBunch(pos,beam=1).number,dMatrix)
        elif beams.getBunch(pos,beam=2) != None and beams.getBunch(pos,beam=1) == None:
            dMatrix = self.getDamperMatrix(basis,1)
            retVal = basis.getBunchProjection(2,beams.getBunch(pos,beam=2).number,dMatrix)
        elif beams.getBunch(pos,beam=1) != None and beams.getBunch(pos,beam=2) != None:
            dMatrix_B1 = self.getDamperMatrix(basis,0)
            projB1 = basis.getBunchProjection(1,beams.getBunch(pos,beam=1).number,dMatrix_B1)
            dMatrix_B2 = self.getDamperMatrix(basis,1)
            projB2 = basis.getBunchProjection(2,beams.getBunch(pos,beam=1).number,dMatrix_B2)
            retVal = proB1.dot(projB2)
        else:
            retVal = spm.identity(basis.size,format='csr')
        return retVal

