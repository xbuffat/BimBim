import time, os, pickle
import scipy.sparse as spm
import numpy as np
import scipy.constants as cst
from BimBim.Action import Action
from BimBim.Matrix import printMatrix
from BimBim.Error import BimBimError


class SpaceCharge(Action.Action):

    def __init__(self, circum = 26700, opticalBeta=1.0,radius=None,dipolar=True,quadrupolar=True):
        self._GeVToKg = cst.e/cst.c**2*1E9
        self._circum = circum
        self._opticalBeta = opticalBeta
        if isinstance(self.opticalBeta,list):
            raise BimBimError('SpaceChargeAB cannot take a list of optical betas (unequal beam sized are not implemented)')
        self._dipolar = dipolar
        self._quadrupolar = quadrupolar
        if radius == None:
            self._radius = np.sqrt(2) # by default this matches the airbag radius to get an rms length of 1
        else:
            self._radius = radius

    def getC(self, bunch):    #constant A, equation (7) overleaf, now aded sig s and optical beta
        if bunch.emit_phys_x != bunch.emit_phys_y:
            raise BimBimError('SpaceChargeAB is not implemented for unequal emittances')
        C1 = - cst.e**2 * bunch.intensity / (2 * np.pi * cst.epsilon_0 * bunch.particleMass*self._GeVToKg * cst.c**2 * bunch.beta**2 * bunch.gamma**3*bunch.sigma_z)
        C2 = 0.5 * self.circum /(bunch.emit_phys_x*self.opticalBeta)
        C = C1 * C2
        return C

    def getInteractionMatrix(self, basis):
        interactionMatrix = self.computeInteractionMatrix(basis)
        return interactionMatrix

    def computeInteractionMatrix(self,basis):
        bmat = np.zeros((basis.bunchBlockSize,basis.bunchBlockSize))
        for iSliceB1 in range(basis.nSlice):
            for iRingB1 in range(basis.nRing):
                for jSliceB1 in range(basis.nSlice):
                    for jRingB1 in range(basis.nRing):
                        if iSliceB1!=jSliceB1 or iRingB1!=jRingB1:
                            I=basis.getIndexForSlice(iSliceB1,iRingB1)
                            J=basis.getIndexForSlice(jSliceB1,jRingB1)
                            interaction_strength = self.computeInteractionMatrixElement(basis, iSliceB1, iRingB1, jSliceB1, jRingB1)
                            if self._dipolar:
                                bmat[I+1,J] = interaction_strength
                            if self._quadrupolar:
                                bmat[I+1,I] -= interaction_strength

        #myFile = open('matrix.mat','w');
        #printMatrix(myFile,bmat);
        #myFile.close();
        return bmat

    def computeInteractionMatrixElement(self,basis,iSlice,iRing,jSlice,jRing):
        if np.abs(basis.getSPosition(iSlice,iRing,1.0)-basis.getSPosition(jSlice,jRing,1.0))<1E-6:
            x1 = np.cos(basis.getSliceAngle(iSlice)-np.pi/basis._nSlice)
            x2 = np.cos(basis.getSliceAngle(iSlice)+np.pi/basis._nSlice)
            interaction = np.abs(basis._nSlice/(8*np.pi**2*self._radius)*np.log((1+x2)*(1-x1)/((1-x2)*(1+x1))))
            return interaction
        else:
            return 0.0

    def getMatrix(self,beams,pos,basis):
        bunchB1 = beams.getBunch(pos,beam=1)
        bunchB2 = beams.getBunch(pos,beam=2)
        if bunchB1 != None and bunchB2 == None:
            singleBunchSCMap = self.getInteractionMatrix(basis)
            singleBunchSCMap = np.identity(basis.bunchBlockSize)+singleBunchSCMap *self.getC(bunchB1)
            retVal = basis.getFullBunchProjection(1,bunchB1.getNumber(),singleBunchSCMap);
            return retVal.tocsr()
        elif bunchB2 != None and bunchB1 == None:
            singleBunchSCMap = self.getInteractionMatrix(basis)
            singleBunchSCMap = np.identity(basis.bunchBlockSize)+singleBunchSCMap *self.getC(bunchB2)
            retVal = basis.getFullBunchProjection(2,bunchB2.getNumber(),singleBunchSCMap);
            return retVal.tocsr()
        elif bunchB1 != None and bunchB2 != None:
            singleBunchSCMap = self.getInteractionMatrix(basis)
            singleBunchSCMap1 = np.identity(basis.bunchBlockSize)+singleBunchSCMap *self.getC(bunchB1)
            zMatrix1 = basis.getFullBunchProjection(1,bunchB1.getNumber(),zMatrix)
            singleBunchSCMap2 = np.identity(basis.bunchBlockSize)+singleBunchSCMap *self.getC(bunchB2)
            zMatrix2 = basis.getFullBunchProjection(2,bunchB2.getNumber(),zMatrix);
            retVal = singleBunchSCMap1.dot(singleBunchSCMap2);
            return retVal.tocsr()
        else:
            retVal = spm.identity(basis.size,format='csr')
        return retVal


