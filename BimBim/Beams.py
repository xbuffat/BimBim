import numpy as np
from BimBim.Bunch import Bunch
from BimBim.Error import BimBimError

class Beams:
    def _parseStringRep(self,rep,energy = 0,phys_emit_x=0,phys_emit_y=0,sigma_z=0,sigma_dpp=0,intensity=0,particleMass=0,particleCharge=0,bunchSpacing=0):
        tokens = rep.split(' ')
        if len(tokens)%2 > 0:
            print("ERROR parsing filling scheme",rep)
            return []
        retVal = []
        bunchNumber = 0
        longitudinal_positions = 0.0
        for i in range(0,len(tokens),2):
             if tokens[i+1] == '0':
                bunch = False
             else:
                bunch = True
             for j in range(int(tokens[i])):
                if bunch:
                    retVal.append(Bunch(bunchNumber,energy,intensity,phys_emit_x,phys_emit_y,sigma_z,sigma_dpp,longitudinal_positions,particleMass,particleCharge))
                    retVal.append(None)
                    bunchNumber += 1
                else:
                    retVal.append(None)
                    retVal.append(None)
                longitudinal_positions += bunchSpacing
        return retVal, bunchNumber
    
    # all parameters can be lists to specify the two beams
    def __init__(self, stringRep=None, phys_emit_x = None, phys_emit_y = None, energy=4000.0, particleMass=0.93827231, particleCharge=1, sigma_z=0, sigma_dpp=0, intensity=0, bunchSpacing=0):
        if stringRep is None:
            stringRepB1 = '1 1 9 0'
            stringRepB2 = None
        else:
            stringRep_tmp = np.atleast_1d(stringRep)
            stringRepB1 = stringRep_tmp[0]
            if len(stringRep_tmp) > 1:
                stringRepB2 = stringRep_tmp[0]
            else:
                stringRepB2 = None
        if phys_emit_x is None:
            phys_emit_x = phys_emit_y
        elif phys_emit_y is None:
            phys_emit_y = phys_emit_x
        phys_emit_x = np.atleast_1d(phys_emit_x)
        phys_emit_x_B1 = phys_emit_x[0]
        phys_emit_y = np.atleast_1d(phys_emit_y)
        phys_emit_y_B1 = phys_emit_y[0]
        energy = np.atleast_1d(energy)
        energy_B1 = energy[0]
        particleMass = np.atleast_1d(particleMass)
        particleMass_B1 = particleMass[0]
        particleCharge = np.atleast_1d(particleCharge)
        particleCharge_B1 = particleCharge[0]
        sigma_z = np.atleast_1d(sigma_z)
        sigma_z_B1 = sigma_z[0]
        sigma_dpp = np.atleast_1d(sigma_dpp)
        sigma_dpp_B1 = sigma_dpp[0]
        intensity = np.atleast_1d(intensity)
        intensity_B1 = intensity[0]
        bunchSpacing = np.atleast_1d(bunchSpacing)
        bunchSpacing_B1 = bunchSpacing[0]
            
        self.bunchConfigB1, self.nBunchB1 = self._parseStringRep(stringRepB1,energy = energy_B1,
                phys_emit_x=phys_emit_x_B1,phys_emit_y=phys_emit_y_B1,
                sigma_z=sigma_z_B1,sigma_dpp=sigma_dpp_B1,intensity=intensity_B1,
                particleMass=particleMass_B1,particleCharge=particleCharge_B1,
                bunchSpacing=bunchSpacing_B1)
        #reverse beam 1
        self.bunchConfigB1 = self.bunchConfigB1[::-1]
        self.bunchConfigB1.insert(0,self.bunchConfigB1[-1])
        self.bunchConfigB1.pop()

        if stringRepB2 != None:
            if len(phys_emit_x) > 1:
                phys_emit_x_B2 = phys_emit_x[1]
            else:
                phys_emit_x_B2 = phys_emit_x[0]
            if len(phys_emit_x) > 1:
                phys_emit_y_B2 = phys_emit_y[1]
            else:
                phys_emit_y_B2 = phys_emit_y[0]
            if len(energy) > 1:
                energy_B2 = energy[1]
            else:
                energy_B2 = energy[0]
            if len(particleMass) > 1:
                particleMass_B2 = particleMass[1]
            else:
                particleMass_B2 = particleMass[0]
            if len(particleCharge) > 1:
                particleCharge_B2 = particleCharge[1]
            else:
                particleCharge_B2 = particleCharge[0]
            if len(sigma_z) > 1:
                sigma_z_B2 = sigma_z[1]
            else:
                sigma_z_B2 = sigma_z[0]
            if len(sigma_dpp) > 1:
                sigma_dpp_B2 = sigma_dpp[1]
            else:
                sigma_dpp_B2 = sigma_dpp[0]
            if len(intensity) > 1:
                intensity_B2 = intensity[1]
            else:
                intensity_B2 = intensity[0]
            if len(bunchSpacing) > 1:
                bunchSpacing_B2 = bunchSpacing[1]
            else:
                bunchSpacing_B2 = bunchSpacing[0]

            self.bunchConfigB2, self.nBunchB2 = self._parseStringRep(stringRepB2,energy = energy_B2,
                    phys_emit_x=phys_emit_x_B2,phys_emit_y=phys_emit_y_B2,
                    sigma_z=sigma_z_B2,sigma_dpp=sigma_dpp_B2,intensity=intensity_B2,
                    particleMass=particleMass_B2,particleCharge=particleCharge_B2,
                    bunchSpacing=bunchSpacing_B1)
        else:
            self.bunchConfigB2 = []
            self.nBunchB2 = 0
        if len(self.bunchConfigB1) > len(self.bunchConfigB2):
            while len(self.bunchConfigB1) > len(self.bunchConfigB2):
                self.bunchConfigB2.append(None)
        else:
            while len(self.bunchConfigB2) > len(self.bunchConfigB1):
                self.bunchConfigB1.append(None)

        if self.nBunchB2 > 0:
            self.nBeam = 2
        else:
            self.nBeam = 1

    def getBunchConfig(self,beam=1):
        if beam==1:
            return self.bunchConfigB1
        else:
            return self.bunchConfigB2

    def getBunch(self,pos,beam=1):
        if beam==1:
            return self.bunchConfigB1[pos]
        else:
            return self.bunchConfigB2[pos]

    def getNBunch(self,beam=1):
        if beam==1:
            return self.nBunchB1
        else:
            return self.nBunchB2

    def getEmptyActionSequence(self):
        return [None for k in range(len(self.bunchConfigB1))]
        
    def step(self):
        self.bunchConfigB1.insert(0,self.bunchConfigB1[-1])
        self.bunchConfigB1.pop()
        if self.nBeam == 2:
            self.bunchConfigB2.append(self.bunchConfigB2[0])
            self.bunchConfigB2.remove(self.bunchConfigB2[0])
            
