import time,os,psutil
from BimBim.Error import BimBimError
import scipy.sparse as spm

class System:
    def __init__(self, beams = None, actionSequence = None, basis = None, returnSparse=False, monitorPerf=False):
        self.beams = beams
        self.actionSequence = actionSequence
        self.basis = basis
        self.npos = len(actionSequence)
        self.returnSparse = returnSparse
        self.checkValidity()
        self.monitorPerf = monitorPerf
        
    def checkValidity(self):
        if len(self.actionSequence) != self.npos:
            raise BimBimError('System: action sequence length should be equal to the number of position '+str(len(self.actionSequence))+', '+str(self.npos))
        if len(self.beams.bunchConfigB1) != len(self.beams.bunchConfigB2):
            raise BimBimError('System error : Bunch configurations must have equal size ('+str(len(self.beams.bunchConfigB1))+', '+str(len(self.beams.bunchConfigB2)))
        if len(self.beams.bunchConfigB1) != self.npos:
            raise BimBimError('System: Bunch configuration length should be identical to action sequence length ('+str(len(self.beams.bunchConfigB1))+'/'+str(self.npos)+')')

    def step(self):
        self.beams.step()
    
    def getAction(self,pos):
        return self.actionSequence[pos]
    
    def getMatrix(self):
        #print 'Computing step matrix for',nSlice,'slices and',nRing,'rings'
        stepMatrix = spm.identity(self.basis.size,format="csr")
        for pos in range(self.npos):
            #print 'position,',pos,',bunch1',self.beams.getBunchesB1()[pos],',bunch2',self.beams.getBunchesB2()[pos]
            if self.actionSequence[pos] != None:
                if self.beams.bunchConfigB1[pos] != None or self.beams.bunchConfigB2[pos] != None:
                    #print(self.actionSequence[pos])
                    time0 = time.time()
                    if isinstance(self.actionSequence[pos],list):
                        posMatrix = spm.identity(self.basis.size,format="csr")
                        for action in self.actionSequence[pos]:
                            posMatrix = action.getMatrix(self.beams,pos,self.basis).dot(posMatrix)
                    else:
                        posMatrix = self.actionSequence[pos].getMatrix(self.beams,pos,self.basis)
                    time1 = time.time()
                    stepMatrix = posMatrix.dot(stepMatrix)
                    if self.monitorPerf:
                        process = psutil.Process(os.getpid())
                        print('System: {:.3f}+{:.3f}s for action {:s} ({:.3f}M)'.format(time1-time0,time.time()-time1,str(type(self.actionSequence[pos])),process.memory_info()[0]/2**20))
        return stepMatrix
        
    def buildOneTurnMap(self):
        time0 = time.time()
        oneTurn = spm.identity(self.basis.size,format="csr")
        for step in range(len(self.actionSequence)):
            #print(step,'/',len(self.actionSequence))
            stepMatrix = self.getMatrix()
            time1 = time.time()
            oneTurn = stepMatrix.dot(oneTurn)
            self.step()
            if self.monitorPerf:
                process = psutil.Process(os.getpid())
                print('System: {:.3f}s for step {:d} dot ({:.3f}M)'.format(time.time()-time1,step,process.memory_info()[0]/2**20))
        if self.monitorPerf:
            process = psutil.Process(os.getpid())
            print('System: {:.3f}s for one turn matrix ({:.3f}M)'.format(time.time()-time0,process.memory_info()[0]/2**20))
        if self.returnSparse:
            return oneTurn
        else:
            return oneTurn.todense()
